import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sp
from scipy.stats import poisson
from numba import njit, jit, prange
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import math
from scipy.optimize import curve_fit

#Parameters
core=8              #-      #Number of cores for parallel processing
c=299792458         #m/s    #Speed of light
lambda_red=637E-9   #m      #Wavelength laser
f_red=c/lambda_red  #1/s    #Frequency laser
sigma=50E6          #1/s    #Variance laser
LL_linewidth=13E6   #1/s    #Lifetime limited linewidth Lorentzian shape
N_tot=0.3E14        #-      #Scaling factor
N_thr_success=30    #-      #Treshold for passing the check 
N_thr_repump=5      #-      #Treshold for repump
dt=50E-6            #s      #Duration red laser
dt_gauss=400E-6     #s      #Duration green laser
psi=0.2             #-      #Memory parameter


def generalized_normal(x, mu=0, beta=2, alpha=1):
    return beta/2/alpha/math.gamma(1/beta)*np.exp(-((abs(x-mu)/alpha)**beta))


@jit(nopython=True)
def get_f_res(sigma=sigma,f_red=f_red):
    return np.random.normal(f_red,sigma)
    

@jit(nopython=True)
def lorentzian(x,x_0=0.,gamma=1.):
    return 1./(1.+((x-x_0)*(x-x_0)/gamma/gamma))/np.pi/gamma


@jit(nopython=True)
def get_counts(f_resonance,dt=dt):
    return dt*N_tot*lorentzian(f_red,f_resonance,LL_linewidth)


@jit(nopython=True)
def measure_counts(N_fotons): 
    return np.random.poisson(N_fotons)


@jit(nopython=True)
def CRcheck(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),N_thr_red=20):
    N_green=0 
    N_red=0
    If=get_counts(f_res,dt)
    N=measure_counts(If)
    N_red+=1
    while not N>N_thr_success:
        if N<N_thr_repump or N_red%N_thr_red==0:
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_green+=1
        N=measure_counts(If)
        N_red+=1
    Delta=f_res-f_red
    return Delta, N_green, N_red, N, f_res


@jit(nopython=True,parallel=True)
def get_results(function, N_thr_success, N_thr_repump, sigma=sigma, psi=psi, dt=dt, N_thr_red=20, amount=20000):
    d=np.zeros(amount*core)
    N_green=np.zeros(amount*core)
    N_red=np.zeros(amount*core)
    N_on_success=np.zeros(amount*core)
    for j in prange(core):
        f_res=get_f_res()
        for i in range(amount):
            d[i+j*amount], N_green[i+j*amount], N_red[i+j*amount], N_on_success[i+j*amount],f_res_old=function(N_thr_success,N_thr_repump,sigma,dt,f_res,N_thr_red)
            f_res=get_f_res(psi*sigma,f_res_old)
    N_avg=np.mean(N_on_success)
    var=np.var(d)
    sd=np.sqrt(var)
    mu_g=np.mean(N_green)
    mu_r=np.mean(N_red)
    meantime=dt_gauss*mu_g+dt*mu_r
    return sd, meantime, N_avg


#@jit(nopython=True)
def find_opt(constraint,tSD_arr, tMuT_arr, tN_arr, X, Y,sigma,dt=dt):
        #finding optimal values
    #Constraint 13 MHZ
    c1=tSD_arr<constraint*1E6
    #finding min average time
    c2=np.min(tMuT_arr[c1])
    #print(np.where(tMuT_arr==c2)[1][0])
    place=np.where(tMuT_arr==c2)
    loc=(place[0][0],place[1][0])
    opt_N_thr_succes=X[loc]
    opt_N_thr_repump=Y[loc]
    opt_SD=tSD_arr[loc]
    opt_MuT=tMuT_arr[loc]
    opt_N=tN_arr[loc]
    print()
    print("At sigma = "+str(sigma))
    print("At dt = "+str(dt))
    print("Optimal Location at N_thr_succes = "+str(opt_N_thr_succes)+" and N_thr_repump = "+str(opt_N_thr_repump))
    print("Spectral diffusion = "+str(opt_SD))
    print("Mean time untill succes = "+str(opt_MuT))
    print("Mean number of photons= "+str(opt_N))
    return opt_N_thr_succes, opt_N_thr_repump, opt_SD, opt_MuT, opt_N


#@jit(nopython=True)
def optimal_results(function,g_num,r_num, sigma=sigma, start_N_thr_succes=0 , start_N_thr_repump=0, psi=0.2, dt=dt):
    N_thr_succes=start_N_thr_succes
    N_thr_repump=start_N_thr_repump
    N_thr_succes_arr=np.arange(N_thr_succes,N_thr_succes+g_num,1)
    N_thr_repump_arr=np.arange(N_thr_repump,N_thr_repump+r_num,1)
    SD_arr= np.zeros((g_num,r_num))
    MuT_arr=np.zeros((g_num,r_num))
    N_arr=np.zeros((g_num,r_num))
    X,Y=np.meshgrid(N_thr_succes_arr,N_thr_repump_arr)
    for i in range(g_num):
        for j in range(r_num):
            if N_thr_repump_arr[j]<N_thr_succes_arr[i]:
                SD_arr[i,j], MuT_arr[i,j], N_arr[i,j]=get_results(function,N_thr_succes_arr[i],N_thr_repump_arr[j],sigma,psi )
            else:
                SD_arr[i,j], MuT_arr[i,j], N_arr[i,j]=np.nan ,np.nan, np.nan
    tSD_arr=SD_arr.T
    tMuT_arr=MuT_arr.T
    tN_arr=N_arr.T
    opt_N_thr_succes, opt_N_thr_repump, opt_SD, opt_MuT, opt_N=find_opt(13,tSD_arr, tMuT_arr, tN_arr, X, Y,sigma,dt)
    return opt_N_thr_succes, opt_N_thr_repump, opt_SD,  opt_MuT, opt_N
    

#@jit(nopython=True)
def variation_psi(psi_length=1,psi_start=0,psi_step=0.1,sigma_length=100,sigma_start=0,sigma_step=10):
    sigma_arr=1E6*np.arange(sigma_start,sigma_start+sigma_length+sigma_step,step=sigma_step)
    psi_arr=np.arange(psi_start,psi_start+psi_length+psi_step,step=psi_step)
    sigma_size=len(sigma_arr)
    psi_size=len(psi_arr)
    opt_N_thr_succes_arr=np.zeros((sigma_size,psi_size))
    opt_N_thr_repump_arr=np.zeros((sigma_size,psi_size))
    opt_SD_arr=np.zeros((sigma_size,psi_size))
    opt_MuT_arr=np.zeros((sigma_size,psi_size))
    opt_N_arr=np.zeros((sigma_size,psi_size))      
    X,Y=np.meshgrid(sigma_arr,psi_arr)
    for i in range(sigma_size):
        for j in range(psi_size):
            opt_N_thr_succes_arr[i,j], opt_N_thr_repump_arr[i,j], opt_SD_arr[i,j],  opt_MuT_arr[i,j], opt_N_arr[i,j]=optimal_results(CRcheck,30,30,sigma_arr[i],0,0,psi_arr[j])


    fig = plt.figure()
    ax = fig.add_subplot(1, 3, 1, projection='3d')
    plt.title("Optimal N_thresholds for varying sigma")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("psi (-)")
    ax.set_zlabel("N_thr (-)")
    ax.plot_surface(X/1E6, Y, opt_N_thr_succes_arr.T, cmap=cm.coolwarm, linewidth=0.1, antialiased=False,label="N_thr_succes")
    ax.plot_surface(X/1E6, Y, opt_N_thr_repump_arr.T, cmap=cm.bwr, linewidth=0.1, antialiased=False, label="N_thr_repump")

    ax = fig.add_subplot(1, 3, 2, projection='3d')
    plt.title("Optimal values of rate and fequency with varying sigma")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("psi (-)")
    ax.set_zlabel("Rate (ms)")#, color='b')
    ax.plot_surface(X/1E6, Y, opt_MuT_arr.T*1E3, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Mean (time till succes)")

    ax = fig.add_subplot(1, 3, 3, projection='3d')
    plt.title("Optimal photon counts") 
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("psi (-)")
    ax.set_zlabel("Photons (-)")
    ax.plot_surface(X/1E6, Y, opt_N_arr.T, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Average lambda")
    plt.show()

    fig=plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    plt.title("Optimal values of mean time till succes with varying sigma and psi")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("psi (-)")
    ax.set_zlabel("Time (ms)")#, color='b')
    ax.plot_surface(X/1E6, Y, opt_MuT_arr.T*1E3, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Mean (time till succes)")
    plt.show()

    fig=plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    plt.title("Optimal Photon counts with varying sigma and psi")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("psi (-)")
    ax.set_zlabel("Photons (-)")
    ax.plot_surface(X/1E6, Y, opt_N_arr.T, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Average N on succes")
    plt.show()

    fig=plt.figure()
    ax = fig.add_subplot(1, 2, 1, projection='3d')
    plt.title("Optimal values of mean time till succes")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("psi (-)")
    ax.set_zlabel("Time (ms)")
    ax.plot_surface(X/1E6, Y, opt_MuT_arr.T*1E3, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Mean (time till succes)")

    ax = fig.add_subplot(1, 2, 2, projection='3d')
    plt.title("Optimal Photon counts with varying sigma and psi")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("psi (-)")
    ax.set_zlabel("Photons (-)")
    ax.plot_surface(X/1E6, Y, opt_N_arr.T, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Average N on succes")
    plt.show()

        
variation_psi(psi_length=1,psi_start=0,psi_step=0.1,sigma_length=100,sigma_start=0,sigma_step=10)