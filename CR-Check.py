import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sp
from scipy.stats import poisson
from numba import njit, jit, prange
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import math
from scipy.optimize import curve_fit

#Parameters
core=8              #-      #Number of cores for parallel processing
c=299792458         #m/s    #Speed of light
lambda_red=637E-9   #m      #Wavelength laser
f_red=c/lambda_red  #1/s    #Frequency laser
sigma=50E6          #1/s    #Variance laser
LL_linewidth=13E6   #1/s    #Lifetime limited linewidth Lorentzian shape
N_tot=0.3E14        #-      #Scaling factor
N_thr_success=30    #-      #Treshold for passing the check 
N_thr_repump=5      #-      #Treshold for repump
dt=50E-6            #s      #Duration red laser
dt_gauss=400E-6     #s      #Duration green laser
psi=0.2             #-      #Memory parameter


def generalized_normal(x, mu=0, beta=2, alpha=1):
    return beta/2/alpha/math.gamma(1/beta)*np.exp(-((abs(x-mu)/alpha)**beta))


@jit(nopython=True)
def get_f_res(sigma=sigma,f_red=f_red):
    return np.random.normal(f_red,sigma)
    

@jit(nopython=True)
def lorentzian(x,x_0=0.,gamma=1.):
    return 1./(1.+((x-x_0)*(x-x_0)/gamma/gamma))/np.pi/gamma


@jit(nopython=True)
def get_counts(f_resonance,dt=dt):
    return dt*N_tot*lorentzian(f_red,f_resonance,LL_linewidth)


@jit(nopython=True)
def measure_counts(N_fotons): 
    return np.random.poisson(N_fotons)


@jit(nopython=True)
def CRcheck(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),N_thr_red=20):
    N_green=0 
    N_red=0
    If=get_counts(f_res,dt)
    N=measure_counts(If)
    N_red+=1
    while not N>N_thr_success:
        if N<N_thr_repump or N_red%N_thr_red==0:
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_green+=1
        N=measure_counts(If)
        N_red+=1
    Delta=f_res-f_red
    return Delta, N_green, N_red, N, f_res

@jit(nopython=True)
def CRcheck_prim(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),N_thr_red=20):
    N_green=0 
    N_red=0
    If=get_counts(f_res,dt)
    N=measure_counts(If)
    N_red+=1
    while not N>N_thr_success:
        f_res=get_f_res(sigma)
        If=get_counts(f_res,dt)
        N_green+=1
        N=measure_counts(If)
        N_red+=1
    Delta=f_res-f_red
    return Delta, N_green, N_red, N, f_res


@jit(nopython=True,parallel=True)
def get_resultsv2(function, N_thr_success, N_thr_repump, sigma=sigma, dt=dt, N_thr_red=20, amount=200000):
    d=np.zeros(amount*core)
    N_green=np.zeros(amount*core)
    N_red=np.zeros(amount*core)
    N_on_success=np.zeros(amount*core)
    for j in prange(core):
        f_res=get_f_res()
        for i in range(amount):
            d[i+j*amount], N_green[i+j*amount], N_red[i+j*amount], N_on_success[i+j*amount],f_res_old=function(N_thr_success,N_thr_repump,sigma,dt,f_res,N_thr_red)
            f_res=get_f_res(psi*sigma,f_res_old)
    return d, N_green, N_red, N_on_success


def plot_distribution(function=CRcheck, N_thr_success=N_thr_success, N_thr_repump=N_thr_repump, sigma=sigma,dt=dt, amount=200000):
    d, N_green, N_red, N=get_resultsv2(function, N_thr_success, N_thr_repump, sigma, dt=dt, N_thr_red=20, amount=amount)
    plt.figure()
    plt.title("Distance on success N_thr_success= "+str(N_thr_success)+" and N_thr_repump= "+str(N_thr_repump)) 
    plt.ylabel("Normalized occurence (-)")
    plt.xlabel("Frequency (MHz)")
    number_of_bins=50
    d=d/1E6 #make it MHz
    count, bins, ignored = plt.hist(d, bins=number_of_bins ,density=True)
    mu=np.mean(d)
    var=np.var(d)
    x_arr=np.linspace(np.min(d),np.max(d),1000)
    guess=np.zeros(3)
    guess[0]=mu
    guess[1]=2#10
    guess[2]=1.28*np.sqrt(2*var)
    popt,pcov=curve_fit(generalized_normal,(bins[:-1]+bins[1:])/2,count,p0=guess)
    mu_fit=popt[0]
    print("mu_fit="+str(mu_fit))
    beta_fit=popt[1]
    print("beta_fit="+str(beta_fit))
    alpha_fit=popt[2]
    print("alpha_fit="+str(alpha_fit))   
    plt.plot(x_arr,generalized_normal(x_arr, mu_fit, beta_fit, alpha_fit),linewidth=2, label=("Generalized normal fit"))
    plt.legend()
    plt.show()
    print("Delta = "+str(mu)+" +/- "+str(np.sqrt(var)))
    mu_green=np.mean(N_green)
    mu_red=np.mean(N_red)
    print("Mean (time untill success) in seconds = "+str(dt_gauss*mu_green+dt*mu_red))


# plot_distribution(function=CRcheck, N_thr_success=16, N_thr_repump=12, sigma=sigma,dt=dt, amount=200000)


@jit(nopython=True,parallel=True)
def get_results(function, N_thr_success, N_thr_repump, sigma=sigma, dt=dt, N_thr_red=20, amount=20000):
    d=np.zeros(amount*core)
    N_green=np.zeros(amount*core)
    N_red=np.zeros(amount*core)
    N_on_success=np.zeros(amount*core)
    for j in prange(core):
        f_res=get_f_res()
        for i in range(amount):
            d[i+j*amount], N_green[i+j*amount], N_red[i+j*amount], N_on_success[i+j*amount],f_res_old=function(N_thr_success,N_thr_repump,sigma,dt,f_res,N_thr_red)
            f_res=get_f_res(psi*sigma,f_res_old)
    N_avg=np.mean(N_on_success)
    var=np.var(d)
    sd=np.sqrt(var)
    mu_g=np.mean(N_green)
    mu_r=np.mean(N_red)
    meantime=dt_gauss*mu_g+dt*mu_r
    return sd, meantime, N_avg

#@jit(nopython=True)
def find_opt(constraint,tSD_arr, tMuT_arr, tN_arr, X, Y,sigma,dt=dt):
    #finding optimal values
    #Constraint 11 MHZ
    c1=tSD_arr<constraint*1E6
    #finding min average time
    c2=np.min(tMuT_arr[c1])
    place=np.where(tMuT_arr==c2)
    loc=(place[0][0],place[1][0])
    opt_N_thr_success=X[loc]
    opt_N_thr_repump=Y[loc]
    opt_SD=tSD_arr[loc]
    opt_MuT=tMuT_arr[loc]
    opt_N=tN_arr[loc]
    print()
    print("At sigma = "+str(sigma))
    print("At dt = "+str(dt))
    print("Optimal Location at N_thr_success = "+str(opt_N_thr_success)+" and N_thr_repump = "+str(opt_N_thr_repump))
    print("Spectral diffusion = "+str(opt_SD))
    print("Mean time untill success = "+str(opt_MuT))
    print("Mean number of photons= "+str(opt_N))
    return opt_N_thr_success, opt_N_thr_repump, opt_SD, opt_MuT, opt_N


#@jit(nopython=True)
def optimal_results(function,g_num,r_num, sigma=sigma, start_N_thr_success=0 , start_N_thr_repump=0,dt=dt, N_thr_red=20):
    N_thr_success_arr=np.arange(start_N_thr_success,start_N_thr_success+g_num,1)
    N_thr_repump_arr=np.arange(start_N_thr_repump,start_N_thr_repump+r_num,1)
    SD_arr= np.zeros((g_num,r_num))
    MuT_arr=np.zeros((g_num,r_num))
    N_arr=np.zeros((g_num,r_num))
    X,Y=np.meshgrid(N_thr_success_arr,N_thr_repump_arr)
    for i in range(g_num):
        for j in range(r_num):
            if N_thr_repump_arr[j]<=N_thr_success_arr[i]:
                SD_arr[i,j], MuT_arr[i,j] , N_arr[i,j]=get_results(function,N_thr_success_arr[i],N_thr_repump_arr[j],sigma,dt, N_thr_red ) #, P_arr[i,j]
            else:
                SD_arr[i,j], MuT_arr[i,j] , N_arr[i,j]=np.nan, np.nan, np.nan

    tSD_arr=SD_arr.T
    tMuT_arr=MuT_arr.T
    tN_arr=N_arr.T

    opt_N_thr_success, opt_N_thr_repump, opt_SD, opt_MuT, opt_N=find_opt(11,tSD_arr, tMuT_arr, tN_arr, X, Y,sigma,dt)
    
    # fig = plt.figure()
    # ax = fig.add_subplot(1, 1, 1, projection='3d')      
    # plt.title("Spectral diffusion on success, optimum at N_thr_success = "+str(opt_N_thr_success)+" and N_thr_repump = "+str(opt_N_thr_repump))
    # ax.set_xlabel("N_thr_success (-)")
    # ax.set_ylabel("N_thr_repump (-)")
    # ax.set_zlabel("Frequency (MHz)")
    # ax.plot_surface(X, Y, tSD_arr/1E6, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False,vmin=np.nanmin(tSD_arr/1E6), vmax=np.nanmax(tSD_arr/1E6))
    # ax.scatter(opt_N_thr_success, opt_N_thr_repump, zs=(opt_SD/1E6), zdir='z', s=20, c="red")
    # plt.show()

    # fig=plt.figure()
    # ax = fig.add_subplot(1, 1, 1, projection='3d')
    # plt.title("Mean time till success, optimum at N_thr_success = "+str(opt_N_thr_success)+" and N_thr_repump = "+str(opt_N_thr_repump))
    # ax.set_xlabel("N_thr_success (-)")
    # ax.set_ylabel("N_thr_repump (-)")
    # ax.set_zlabel("Time (ms)")
    # ax.plot_surface(X, Y, tMuT_arr/1E-3, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False,vmin=np.nanmin(tMuT_arr*1E3), vmax=np.nanmax(tMuT_arr*1E3))
    # ax.scatter(opt_N_thr_success, opt_N_thr_repump, zs=(opt_MuT*1E3), zdir='z', s=20, c="red")
    # plt.show()

    # # ##COMBINED PLOT
    # fig = plt.figure()
    # ax = fig.add_subplot(1, 2, 1, projection='3d')      
    # plt.title("Spectral diffusion on success")
    # ax.set_xlabel("N_thr_success (-)")
    # ax.set_ylabel("N_thr_repump (-)")
    # ax.set_zlabel("Frequency (MHz)")
    # ax.plot_surface(X, Y, tSD_arr/1E6, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False,vmin=np.nanmin(tSD_arr/1E6), vmax=np.nanmax(tSD_arr/1E6))
    # ax.scatter(opt_N_thr_success, opt_N_thr_repump, zs=(opt_SD/1E6), zdir='z', s=20, c="red")

    # ax = fig.add_subplot(1, 2, 2, projection='3d')
    # plt.title("Mean time till succes, optimum at N_thr_success = "+str(opt_N_thr_success)+" and N_thr_repump = "+str(opt_N_thr_repump))
    # ax.set_xlabel("N_thr_success (-)")
    # ax.set_ylabel("N_thr_repump (-)")
    # ax.set_zlabel("Time (ms)")
    # ax.plot_surface(X, Y, tMuT_arr*1E3, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False,vmin=np.nanmin(tMuT_arr*1E3), vmax=np.nanmax(tMuT_arr*1E3))
    # ax.scatter(opt_N_thr_success, opt_N_thr_repump, zs=(opt_MuT*1E3), zdir='z', s=20, c="red")
    # plt.show()

    return opt_N_thr_success, opt_N_thr_repump, opt_SD,  opt_MuT, opt_N

#optimal_results(CRcheck,5,5, sigma=sigma, start_N_thr_success=10 , start_N_thr_repump=5,dt=dt, N_thr_red=20)
    

def variation_of_sigma_results(function=CRcheck,length=500,start=0,step=10, dt=dt):
    sigma_arr=1E6*np.arange(start,start+length+step,step=step)
    size=len(sigma_arr)
    opt_N_thr_success_arr=np.zeros(size)
    opt_N_thr_repump_arr=np.zeros(size)
    opt_SD_arr=np.zeros(size)
    opt_MuT_arr=np.zeros(size)
    opt_N_arr=np.zeros(size)
    for i in range(size):
        opt_N_thr_success_arr[i], opt_N_thr_repump_arr[i], opt_SD_arr[i],  opt_MuT_arr[i], opt_N_arr[i]=optimal_results(function,15,13,sigma_arr[i],0,0,dt) #(20,18)
    
    plt.figure()
    plt.title("Optimal N_thresholds for varying sigma, dt = "+str(dt*1E3)+"ms")
    plt.plot(sigma_arr/1E6,opt_N_thr_success_arr,label="N_thr_success") 
    plt.plot(sigma_arr/1E6, opt_N_thr_repump_arr,label="N_thr_repump") 
    plt.legend()
    plt.xlabel("sigma (MHz)")
    plt.ylabel("N_thr (-)")
    plt.show()

    plt.figure()
    plt.title("Optimal time till success and fequency with varying sigma, dt = "+str(dt*1E3)+"ms")
    plt.xlabel("sigma (MHz)")
    plt.ylabel("Time (ms)",color='b')
    plt.plot(sigma_arr/1E6,opt_MuT_arr*1E3,color='b',label="Mean (time till success)")
    plt.tick_params(axis="y", labelcolor="b", pad=8)
    plt.legend(loc='upper left')
    plt.twinx()
    plt.plot(sigma_arr/1E6,opt_SD_arr/1E6,color='r',label="F_resonance")
    plt.tick_params(axis="y", labelcolor="r", pad=8)
    plt.ylabel("Frequency (MHZ)", color='r')  
    plt.legend(loc='lower right')
    plt.show()
    
    plt.figure()
    plt.title("Optimal Photon counts with varying sigma, dt = "+str(dt*1E3)+"ms")
    plt.plot(sigma_arr/1E6,opt_N_arr,label="Average N")  
    plt.legend()
    plt.xlabel("sigma (MHz)")
    plt.ylabel("Photons (-)")
    plt.show()

    
variation_of_sigma_results(function=CRcheck,length=100, start=0,step=10, dt=dt)
#variation_of_sigma_results(function=CRcheck_prim,length=200, start=0,step=10, dt=dt)


def variation_of_dt_results(function=CRcheck,percentage=0.85,length=1,start=0.5,step=0.1,sigma=sigma):
    dt_arr=dt*np.arange(start,start+length+step,step=step)
    size=len(dt_arr)
    opt_N_thr_success_arr=np.zeros(size)
    opt_N_thr_repump_arr=np.zeros(size)
    opt_SD_arr=np.zeros(size)
    opt_MuT_arr=np.zeros(size)
    opt_N_arr=np.zeros(size)
    for i in range(size):
        Np=min(int((np.ceil(get_counts(f_red,dt=dt_arr[i])))),30)
        opt_N_thr_success_arr[i], opt_N_thr_repump_arr[i], opt_SD_arr[i],  opt_MuT_arr[i], opt_N_arr[i]=optimal_results(function,Np,Np,sigma,0,0,dt_arr[i])
    
    plt.figure()
    plt.title("Optimal N_thresholds for varying dt, sigma = "+str(sigma/1E6)+"MHz")
    plt.plot(dt_arr*1E3,opt_N_thr_success_arr,label="N_thr_success") 
    plt.plot(dt_arr*1E3, opt_N_thr_repump_arr,label="N_thr_repump") 
    plt.legend()
    plt.xlabel("dt (ms)")
    plt.ylabel("N_thr (-)")
    plt.show()

    plt.figure
    plt.title("Optimal time till success and fequency with varying dt, sigma = "+str(sigma/1E6)+"MHz")
    plt.xlabel("dt (ms)")
    plt.ylabel("Time (ms)",color='b')
    plt.plot(dt_arr*1E3,opt_MuT_arr*1E3,color='b',label="Mean (time till success)")
    plt.tick_params(axis="y", labelcolor="b", pad=8)
    plt.legend(loc='lower right')
    plt.twinx()
    plt.plot(dt_arr*1E3,opt_SD_arr/1E6,color='r',label="F_resonance")
    plt.tick_params(axis="y", labelcolor="r", pad=8)
    plt.ylabel("Frequency (MHz)", color='r')  
    plt.legend(loc='upper left')
    plt.show()

    plt.figure()
    plt.title("Optimal Photon counts with varying dt, sigma = "+str(sigma/1E6)+"MHz")
    plt.plot(dt_arr*1E3,opt_N_arr,label="Average N")  
    plt.legend()
    plt.xlabel("dt (ms)")
    plt.ylabel("Photons (-)")
    plt.show()

    plt.figure()
    plt.title("Optimal N_thresholds and Photon counts for varying dt, sigma = "+str(sigma/1E6)+"MHz")
    plt.plot(dt_arr*1E3,opt_N_thr_success_arr,label="N_thr_success") 
    plt.plot(dt_arr*1E3, opt_N_thr_repump_arr,label="N_thr_repump")
    plt.plot(dt_arr*1E3,opt_N_arr,label="Average N on succes")  
    plt.legend()
    plt.xlabel("dt (ms)")
    plt.ylabel("N (-)")
    plt.show()

#variation_of_dt_results(function=CRcheck,length=2,start=0.1,step=0.1,sigma=sigma)
#variation_of_dt_results(function=CRcheck_prim,length=2,start=0.5,step=0.1)

#@jit(nopython=True)
def variation_2D(function=CRcheck,dt_length=1,dt_start=0.5,dt_step=0.1,sigma_length=100,sigma_start=0,sigma_step=10):
    sigma_arr=1E6*np.arange(sigma_start,sigma_start+sigma_length+sigma_step,step=sigma_step)
    dt_arr=dt*np.arange(dt_start,dt_start+dt_length+dt_step,step=dt_step)
    sigma_size=len(sigma_arr)
    dt_size=len(dt_arr)
    opt_N_thr_success_arr=np.zeros((sigma_size,dt_size))
    opt_N_thr_repump_arr=np.zeros((sigma_size,dt_size))
    opt_SD_arr=np.zeros((sigma_size,dt_size))
    opt_MuT_arr=np.zeros((sigma_size,dt_size))
    norm_arr=np.zeros((sigma_size,dt_size))
    opt_N_arr=np.zeros((sigma_size,dt_size))      
    X,Y=np.meshgrid(sigma_arr,dt_arr)
    for i in range(sigma_size):
        for j in range(dt_size):
            norm_arr[i,j]=get_counts(f_red,dt=dt_arr[j])
            Np=int(min(np.ceil(get_counts(f_red,dt=dt_arr[j])),35))
            opt_N_thr_success_arr[i,j], opt_N_thr_repump_arr[i,j], opt_SD_arr[i,j],  opt_MuT_arr[i,j], opt_N_arr[i,j]=optimal_results(function,Np,1,sigma_arr[i],0,0,dt_arr[j])
        
    fig = plt.figure()
    ax = fig.add_subplot(1, 2, 1, projection='3d')
    plt.title("Optimal N_threshold_success for varying sigma and dt")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("dt (ms)")
    ax.set_zlabel("N_thr (-)")
    ax.plot_surface(X/1E6, Y*1E3, opt_N_thr_success_arr.T, cmap=cm.coolwarm, linewidth=0.1, antialiased=False,label="N_thr_success")
    ax = fig.add_subplot(1, 2, 2, projection='3d')
    plt.title("Optimal N_threshold_repump for varying sigma and dt")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("dt (ms)")
    ax.set_zlabel("N_thr (-)")
    ax.plot_surface(X/1E6, Y*1E3, opt_N_thr_repump_arr.T, cmap=cm.bwr, linewidth=0.1, antialiased=False, label="N_thr_repump")
    plt.show()

    fig = plt.figure()
    ax = fig.add_subplot(1, 2, 1, projection='3d')
    plt.title("Optimal values of time till success")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("dt (ms)")
    ax.set_zlabel("Time (ms)")
    ax.plot_surface(X/1E6, Y*1E3, opt_MuT_arr.T*1E3, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Mean (time till success)")
    ax = fig.add_subplot(1, 2, 2, projection='3d')
    plt.title("Optimal photon counts") 
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("dt (ms)")
    ax.set_zlabel("Photons (-)")
    ax.plot_surface(X/1E6, Y*1E3, opt_N_arr.T, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Average N on succes")
    plt.show()

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    plt.title("Optimal values of time till success")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("dt (ms)")
    ax.set_zlabel("Time (ms)")#, color='b')
    ax.plot_surface(X/1E6, Y*1E3, opt_MuT_arr.T*1E3, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Mean (time till success)")
    plt.show()

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    plt.title("Optimal photon counts")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("dt (ms)")
    ax.set_zlabel("Photons (-)")
    ax.plot_surface(X/1E6, Y*1E3, opt_N_arr.T, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Average N on succes")
    plt.show()

    
#variation_2D(function=CRcheck_prim,dt_length=2,dt_start=0.5,dt_step=0.1,sigma_length=100,sigma_start=0,sigma_step=10)
#variation_2D(function=CRcheck_new5,dt_length=1,dt_start=0.1,dt_step=0.1,sigma_length=100,sigma_start=0,sigma_step=10)

def variation_of_N_thr_red(function=CRcheck,length=20,start=1,step=1): #start is at least 1
    N_thr_red_arr=np.arange(start,start+length+step,step=step)
    size=len(N_thr_red_arr)
    opt_N_thr_success_arr=np.zeros(size)
    opt_N_thr_repump_arr=np.zeros(size)
    opt_SD_arr=np.zeros(size)
    opt_MuT_arr=np.zeros(size)
    opt_N_arr=np.zeros(size)
    for i in range(size):
        opt_N_thr_success_arr[i], opt_N_thr_repump_arr[i], opt_SD_arr[i],  opt_MuT_arr[i], opt_N_arr[i]=optimal_results(function,18,18,sigma,0,0,dt,N_thr_red_arr[i])
    
    plt.subplot(1,3,1)
    plt.title("Optimal N_thresholds for varying sigma")
    plt.plot(N_thr_red_arr,opt_N_thr_success_arr,label="N_thr_success") 
    plt.plot(N_thr_red_arr, opt_N_thr_repump_arr,label="N_thr_repump") 
    plt.legend()
    plt.xlabel("N_thr_red (#)")
    plt.ylabel("N_thr (#)")

    plt.subplot(1,3,2)
    plt.title("Optimal time till success and fequency with varying sigma")
    plt.xlabel("N_thr_red (#)")
    plt.ylabel("Time (ms)",color='b')
    plt.plot(N_thr_red_arr,opt_MuT_arr*1E3,color='b',label="Mean (time till success)")
    plt.tick_params(axis="y", labelcolor="b", pad=8)
    plt.legend()
    plt.twinx()
    plt.plot(N_thr_red_arr, opt_SD_arr/1E6,color='r',label="F_resonance")
    plt.tick_params(axis="y", labelcolor="r", pad=8)
    plt.ylabel("Frequency (MHZ)", color='r')  
    plt.legend()
    
    plt.subplot(1,3,3)
    plt.title("Optimal photon counts")
    plt.plot(N_thr_red_arr,opt_N_arr,label="Average lambda")  
    plt.legend()
    plt.xlabel("N_thr_red (#)")
    plt.ylabel("Photons (#)")
    plt.show()
    
#variation_of_N_thr_red(function=CRcheck,length=20,start=1,step=1)

#@jit(nopython=True)
def variation_N_red_2D(function=CRcheck,sigma_length=1,sigma_start=0.5,sigma_step=0.1,length=20,start=1,step=1): #start is at least 1
    sigma_arr=1E6*np.arange(sigma_start,sigma_start+sigma_length+sigma_step,step=sigma_step)
    N_thr_red_arr=np.arange(start,start+length+step,step=step)
    size=len(N_thr_red_arr)
    sigma_size=len(sigma_arr)
    opt_N_thr_success_arr=np.zeros((sigma_size,size))
    opt_N_thr_repump_arr=np.zeros((sigma_size,size))
    opt_SD_arr=np.zeros((sigma_size,size))
    opt_MuT_arr=np.zeros((sigma_size,size))
    opt_N_arr=np.zeros((sigma_size,size))      
    X,Y=np.meshgrid(sigma_arr,N_thr_red_arr)
    for i in range(sigma_size):
        for j in range(size):
            opt_N_thr_success_arr[i,j], opt_N_thr_repump_arr[i,j], opt_SD_arr[i,j],  opt_MuT_arr[i,j], opt_N_arr[i,j]=optimal_results(function,20,15,sigma_arr[i],0,0,dt, N_thr_red_arr[j])#np.max(Np-40,0)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    plt.title("Optimal N_thresholds for varying sigma")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("N_thr_red (#)")
    ax.set_zlabel("N_thr (#)")
    ax.plot_surface(X/1E6, Y, opt_N_thr_success_arr.T, cmap=cm.coolwarm, linewidth=0.1, antialiased=False,label="N_thr_success")
    ax.plot_surface(X/1E6, Y, opt_N_thr_repump_arr.T, cmap=cm.bwr, linewidth=0.1, antialiased=False, label="N_thr_repump")
    plt.show()

    fig=plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    plt.title("Optimal values of time till success")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("N_thr_red (#)")
    ax.set_zlabel("Time (ms)")
    ax.plot_surface(X/1E6, Y, opt_MuT_arr.T*1E3, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Mean (time till success)")
    plt.show()

    fig=plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    plt.title("Optimal photon counts")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("N_thr_red (#)")
    ax.set_zlabel("Photons (#)")
    ax.plot_surface(X/1E6, Y, opt_N_arr.T, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Average lambda")
    plt.show()

#variation_N_red_2D(function=CRcheck,sigma_length=100,sigma_start=0,sigma_step=10,length=30,start=1,step=1)
#variation_N_red_2D(function=CRcheck_new5,sigma_length=100,sigma_start=0,sigma_step=10,length=20,start=1,step=1)


def comparison_of_sigma_results(function=CRcheck, function2=CRcheck,length=100,start=0,step=10):
    sigma_arr=1E6*np.arange(start,start+length+step,step=step)
    size=len(sigma_arr)
    opt_N_thr_success_arr=np.zeros((2,size))
    opt_N_thr_repump_arr=np.zeros((2,size))
    opt_SD_arr=np.zeros((2,size))
    opt_MuT_arr=np.zeros((2,size))
    opt_N_arr=np.zeros((2,size))
    for i in range(size):
        opt_N_thr_success_arr[0,i], opt_N_thr_repump_arr[0,i], opt_SD_arr[0,i],  opt_MuT_arr[0,i], opt_N_arr[0,i]=optimal_results(function,16,16,sigma_arr[i],0,0,dt)
        opt_N_thr_success_arr[1,i], opt_N_thr_repump_arr[1,i], opt_SD_arr[1,i],  opt_MuT_arr[1,i], opt_N_arr[1,i]=optimal_results(function2,14,12,sigma_arr[i],0,0,dt/2)
    plt.figure()
    plt.title("Optimal N_thresholds for varying sigma")
    plt.plot(sigma_arr/1E6,opt_N_thr_success_arr[0],color='b',label="N_thr_success") 
    plt.plot(sigma_arr/1E6, opt_N_thr_repump_arr[0],color='r',label="N_thr_repump")
    plt.plot(sigma_arr/1E6,opt_N_thr_success_arr[1],color='b',linestyle='dashed',label="N_thr_success func2") 
    plt.plot(sigma_arr/1E6, opt_N_thr_repump_arr[1],color='r',linestyle='dashed',label="N_thr_repump func2") 
    plt.legend()
    plt.xlabel("sigma (MHz)")
    plt.ylabel("N_thr (-)")
    plt.show()

    plt.figure()
    plt.title("Optimal time till success and fequency with varying sigma")
    plt.xlabel("sigma (MHz)")
    plt.ylabel("Time (ms)",color='b')
    plt.plot(sigma_arr/1E6,opt_MuT_arr[0]*1E3,color='b',label="Mean (time till success)")
    plt.plot(sigma_arr/1E6,opt_MuT_arr[1]*1E3,color='b',linestyle='dashed', label="Mean (time till success) func2")
    plt.tick_params(axis="y", labelcolor="b", pad=8)
    plt.legend()
    plt.twinx()
    plt.plot(sigma_arr/1E6,opt_SD_arr[0]/1E6,color='r',label="F_resonance")
    plt.plot(sigma_arr/1E6,opt_SD_arr[1]/1E6,color='r',linestyle='dashed',label="F_resonance func2")
    plt.tick_params(axis="y", labelcolor="r", pad=8)
    plt.ylabel("Frequency (MHz)", color='r')  
    plt.legend()
    plt.show()

    plt.figure()
    plt.title("Optimal Photon counts with varying sigma")
    plt.plot(sigma_arr/1E6,opt_N_arr[0],color='b',label="Average N")  
    plt.plot(sigma_arr/1E6,opt_N_arr[1],color='b', linestyle='dashed',label="Average N func 2") 
    plt.legend()
    plt.xlabel("sigma (MHz)")
    plt.ylabel("Photons (-)")
    plt.show()


#comparison_of_sigma_results(function=CRcheck, function2=CRcheck,length=120,start=0,step=10)   