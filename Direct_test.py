import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sp
from scipy.stats import poisson
from numba import njit, jit, prange
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import math
from scipy.optimize import curve_fit

#Parameters
core=8              #-      #Number of cores for parallel processing
c=299792458         #m/s    #Speed of light
lambda_red=637E-9   #m      #Wavelength laser
f_red=c/lambda_red  #1/s    #Frequency laser
sigma=50E6          #1/s    #Variance laser
LL_linewidth=13E6   #1/s    #Lifetime limited linewidth Lorentzian shape
N_tot=0.3E14        #-      #Scaling factor
N_thr_success=30     #-      #Treshold for passing the check 
N_thr_repump=5      #-      #Treshold for repump
dt=50E-6            #s      #Duration red laser
dt_gauss=400E-6     #s      #Duration green laser
psi=0.2             #-      #Memory parameter


def generalized_normal(x, mu=0, beta=2, alpha=1):
    return beta/2/alpha/math.gamma(1/beta)*np.exp(-((abs(x-mu)/alpha)**beta))


@jit(nopython=True)
def get_f_res(sigma=sigma,f_red=f_red):
    return np.random.normal(f_red,sigma)
    

@jit(nopython=True)
def lorentzian(x,x_0=0.,gamma=1.):
    return 1./(1.+((x-x_0)*(x-x_0)/gamma/gamma))/np.pi/gamma


@jit(nopython=True)
def get_counts(f_resonance,dt=dt):
    return dt*N_tot*lorentzian(f_red,f_resonance,LL_linewidth)


@jit(nopython=True)
def measure_counts(N_fotons): 
    return np.random.poisson(N_fotons)


@jit(nopython=True)
def factorial(x):
    fac=1.
    for i in range(x):
        fac=fac*(i+1)
    return fac


@jit(nopython=True)
def hyp_test(x,lam):
    p=0
    for i in range(x):
        p+=np.exp(-lam)/factorial(i)*((float(lam))**i)
    return (1-p)


@jit(nopython=True,parallel=True)
def create_list(minval,rlen=60):
    Range=np.zeros(rlen)
    for j in prange(rlen):
        Range[j]=hyp_test(j,minval)
    return Range

minval=get_counts(f_red-LL_linewidth)
print(minval)
Range=create_list(minval,rlen=60)


# Routine using direct test
@jit(nopython=True)
def CRcheck_routine_d(N_thr_succes=0.01,N_thr_repump=0.5,sigma=sigma,dt=dt,f_res=get_f_res(),Range=Range): 
    N_thr_red=10
    N_green=0
    N_red=0
    If=get_counts(f_res,dt)
    N=measure_counts(If)
    p=Range[N]
    N_red+=1
    i=1
    while not p<N_thr_succes:
        if p>N_thr_repump or N_red%N_thr_red==0:
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_green+=1
        N=measure_counts(If)
        N_red+=1
        p=Range[N]
    Delta=f_res-f_red
    return Delta, N_green, N_red, N, f_res


@jit(nopython=True,parallel=True)
def get_results(function, N_thr_success, N_thr_repump, sigma=sigma, dt=dt, Range=Range, amount=20000):
    d=np.zeros(amount*core)
    N_green=np.zeros(amount*core)
    N_red=np.zeros(amount*core)
    N_on_success=np.zeros(amount*core)
    for j in prange(core):
        f_res=get_f_res()
        for i in range(amount):
            d[i+j*amount], N_green[i+j*amount], N_red[i+j*amount], N_on_success[i+j*amount],f_res_old=function(N_thr_success,N_thr_repump,sigma,dt,f_res,Range)
            f_res=get_f_res(psi*sigma,f_res_old)
    N_avg=np.mean(N_on_success)
    var=np.var(d)
    sd=np.sqrt(var)
    mu_g=np.mean(N_green)
    mu_r=np.mean(N_red)
    meantime=dt_gauss*mu_g+dt*mu_r
    return sd, meantime, N_avg

#@jit(nopython=True)
def find_opt(constraint,tSD_arr, tMuT_arr, tN_arr, X, Y,sigma,dt=dt):
    #finding optimal values
    #Constraint 11 MHZ
    c1=tSD_arr<constraint*1E6
    #finding min average time
    c2=np.min(tMuT_arr[c1])
    place=np.where(tMuT_arr==c2)
    loc=(place[0][0],place[1][0])
    opt_N_thr_success=X[loc]
    opt_N_thr_repump=Y[loc]
    opt_SD=tSD_arr[loc]
    opt_MuT=tMuT_arr[loc]
    opt_N=tN_arr[loc]
    print()
    print("At sigma = "+str(sigma))
    print("At dt = "+str(dt))
    print("Optimal Location at N_thr_success = "+str(opt_N_thr_success)+" and N_thr_repump = "+str(opt_N_thr_repump))
    print("Spectral diffusion = "+str(opt_SD))
    print("Mean time untill success = "+str(opt_MuT))
    print("Mean number of photons= "+str(opt_N))
    return opt_N_thr_success, opt_N_thr_repump, opt_SD, opt_MuT, opt_N


#@jit(nopython=True)
def optimal_results(function,g_num,r_num, sigma=sigma, max_N_thr_succes=0.2 , min_N_thr_repump=0.5,dt=dt, Range=Range):
    N_thr_succes_arr=np.linspace(0.80,max_N_thr_succes,num=g_num)
    N_thr_repump_arr=np.linspace(min_N_thr_repump,1,num=r_num)
    SD_arr= np.zeros((g_num,r_num))
    MuT_arr=np.zeros((g_num,r_num))
    N_arr=np.zeros((g_num,r_num))
    X,Y=np.meshgrid(N_thr_succes_arr,N_thr_repump_arr)
    for i in range(g_num):
        for j in range(r_num):
            if  N_thr_succes_arr[i]<=N_thr_repump_arr[j]:
                SD_arr[i,j], MuT_arr[i,j] , N_arr[i,j]=get_results(function,N_thr_succes_arr[i],N_thr_repump_arr[j],sigma,dt, Range )
            else:
                SD_arr[i,j], MuT_arr[i,j] , N_arr[i,j]=np.nan ,np.nan, np.nan

    tSD_arr=SD_arr.T
    tMuT_arr=MuT_arr.T
    tN_arr=N_arr.T
    opt_N_thr_succes, opt_N_thr_repump, opt_SD, opt_MuT, opt_N=find_opt(11,tSD_arr, tMuT_arr, tN_arr, X, Y,sigma,dt)
    return opt_N_thr_succes, opt_N_thr_repump, opt_SD,  opt_MuT, opt_N
    
#optimal_results(CRcheck_routine,10,4, sigma=2*sigma, max_N_thr_succes=0.84 , min_N_thr_repump=0.70 ,dt=dt, N_thr_red=20)

def variation_of_sigma(function=CRcheck_routine_d,sigma_length=100,sigma_start=0,sigma_step=10):
    sigma_arr=1E6*np.arange(sigma_start,sigma_start+sigma_length+sigma_step,step=sigma_step)
    #sigma_arr[0]=1 
    #dt_arr=dt*np.arange(dt_start,dt_start+dt_length+dt_step,step=dt_step)
    sigma_size=len(sigma_arr)
    #dt_size=len(dt_arr)
    opt_N_thr_succes_arr=np.zeros(sigma_size)
    opt_N_thr_repump_arr=np.zeros(sigma_size)
    opt_SD_arr=np.zeros(sigma_size)
    opt_MuT_arr=np.zeros(sigma_size)
    opt_N_arr=np.zeros(sigma_size)
    minval=get_counts(f_red-LL_linewidth)
    Range=create_list(minval,60)
    #print(Range)
    for i in range(sigma_size):
        opt_N_thr_succes_arr[i], opt_N_thr_repump_arr[i], opt_SD_arr[i],  opt_MuT_arr[i], opt_N_arr[i]=optimal_results(function,20,10,sigma_arr[i],0.97,0.90,dt,Range)
    
    plt.figure()
    plt.title("Optimal N_thresholds for varying sigma, dt = "+str(dt*1E3)+"ms")
    plt.plot(sigma_arr/1E6,opt_N_thr_succes_arr,label="N_thr_succes") 
    plt.plot(sigma_arr/1E6, opt_N_thr_repump_arr,label="N_thr_repump") 
    plt.legend()
    plt.xlabel("sigma (MHz)")
    plt.ylabel("N_thr (/)")
    plt.show()

    plt.figure()
    plt.title("Optimal time till succes and fequency with varying sigma, dt = "+str(dt*1E3)+"ms")
    plt.xlabel("sigma (MHz)")
    plt.ylabel("Time (ms)",color='b')
    plt.plot(sigma_arr/1E6,opt_MuT_arr*1E3,color='b',label="Mean (time till succes)")
    plt.tick_params(axis="y", labelcolor="b", pad=8)
    plt.legend(loc='upper left')
    plt.twinx()
    plt.plot(sigma_arr/1E6,opt_SD_arr/1E6,color='r',label="F_resonance")
    plt.tick_params(axis="y", labelcolor="r", pad=8)
    plt.ylabel("Frequency (MHz)", color='r')  
    plt.legend(loc='lower right')
    plt.show()


    plt.figure()
    plt.title("Optimal Photon counts with varying sigma, dt = "+str(dt*1E3)+"ms")
    plt.plot(sigma_arr/1E6,opt_N_arr,label="Average N")  
    plt.legend()
    plt.xlabel("sigma (MHz)")
    plt.ylabel("Photons (-)")
    plt.show()
    
variation_of_sigma(function=CRcheck_routine_d,sigma_length=200,sigma_start=0,sigma_step=10)