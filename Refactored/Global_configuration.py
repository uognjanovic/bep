#Parameters
core=8              #-      #Number of cores for parallel processing
c=299792458         #m/s    #Speed of light
lambda_red=637E-9   #m      #Wavelength laser
f_red=c/lambda_red  #1/s    #Frequency laser
sigma=50E6          #1/s    #Variance laser
LL_linewidth=13E6   #1/s    #Lifetime limited linewidth Lorentzian shape
N_tot=0.3E14        #-      #Scaling factor
N_thr_success=30    #-      #Treshold for passing the check 
N_thr_repump=5      #-      #Treshold for repump
dt=50E-6            #s      #Duration red laser
dt_gauss=400E-6     #s      #Duration green laser
psi=0.2             #-      #Memory parameter



