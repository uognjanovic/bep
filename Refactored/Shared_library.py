import numpy as np
from scipy.stats import poisson
from numba import njit, jit, prange
import math

import Global_configuration as gc

def generalized_normal(x, mu=0, beta=2, alpha=1):
    return beta/2/alpha/math.gamma(1/beta)*np.exp(-((abs(x-mu)/alpha)**beta))


@jit(nopython=True)
def get_f_res(sigma=gc.sigma,f_red=gc.f_red):
    return np.random.normal(f_red,sigma)
    

@jit(nopython=True)
def lorentzian(x,x_0=0.,gamma=1.):
    return 1./(1.+((x-x_0)*(x-x_0)/gamma/gamma))/np.pi/gamma


@jit(nopython=True)
def get_counts(f_resonance,dt=gc.dt):
    return dt*gc.N_tot*lorentzian(gc.f_red,f_resonance,gc.LL_linewidth)


@jit(nopython=True)
def measure_counts(N_fotons): 
    return np.random.poisson(N_fotons)


