import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sp
from scipy.stats import poisson
import random as rd
from numba import njit, jit, prange
from time import time
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import math
from scipy.optimize import curve_fit

# import cProfile
# import re
# cProfile.run('re.compile("foo|bar")')
core=8

#Parameters
c=299792458         #m/s    #speed of light
lambda_red=637E-9   #m      #golflengte laser
f_red=c/lambda_red  #1/s    #frequentie laser
sigma=50E6          #1/s    #variantie laser
LL_linewidth=13E6   #1/s    #Lifetime limited linewidth lorentzian shape

#Choose:
N_tot=0.3E14#/1.2          #-      #Total number of electrons (werken:1E18 met v2 )
N_thr_succes=30            #-      #Treshold number (werken: 1E6 )
N_thr_repump=5            #-      #Treshold number (werken: 1E6 )
dt=50E-6            #s
dt_gauss=400E-6     #s

def generalized_normal(x, mu=0, beta=2, alpha=1):
    return beta/2/alpha/math.gamma(1/beta)*np.exp(-((abs(x)-mu)**beta))


x=np.linspace(-5,5,num=1000)

beta_arr=np.zeros(5)
beta_arr[0]=0.5
beta_arr[1]=1
beta_arr[2]=2
beta_arr[3]=5
beta_arr[4]=10


plt.figure()
plt.title("Effect of shape parameter, mu=0 and alpha = 1")
for i in range(5):
    betas=beta_arr[i]
    y=generalized_normal(x,mu=0,beta=betas,alpha=1)
    plt.plot(x,y, label=("beta = "+str(betas)))
plt.legend()
plt.xlabel("x")
plt.ylabel("f (x)")
plt.show()

