
@jit(nopython=True)
def CRcheck_new(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,N_thr_red=20): #experiment=False #using while loop
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    #N=0
    If=get_counts(f_res,dt/3)
    Nt=(measure_counts(If)+measure_counts(If)+measure_counts(If))
    N_red+=1
    while not Nt>N_thr_success:
        if  Nt<N_thr_repump or N_red%N_thr_red==0:#N_red%100==0:
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt/3)
            N_green+=1
        #N[iz%3]=measure_counts(If)
        #Nt=int(round(np.mean(N)))
        Nt=(measure_counts(If)+measure_counts(If)+measure_counts(If))
        N_red+=1
    Delta=f_res-f_red
    return Delta, N_green, N_red, Nt, f_res#Nt

@jit(nopython=True)
def CRcheck_new2(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,N_thr_red=20): #experiment=False #using while loop
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    #N=0
    If=get_counts(f_res,dt)
    Nt=measure_counts(If)
    N_red+=1
    while not Nt>N_thr_success:
        if  Nt<N_thr_repump or N_red%20==0:
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_green+=1
            Nt=measure_counts(If)
            N_red+=1
        else:
            Nt+=measure_counts(If)
            Nt=Nt/2
            N_red+=1
    Delta=f_res-f_red
    return Delta, N_green, N_red, Nt, f_res

@jit(nopython=True)
def CRcheck_new3(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,ro=20): #experiment=False #using while loop
    N_thr_red=10
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    #N=0
    # eps=3
    # if not Nold > (N_thr_success+eps):
    #     f_res=get_f_res(sigma)
    #     N_green+=1
    If=get_counts(f_res,dt)
    N=np.zeros(N_thr_red)
    N[0]=measure_counts(If)
    N_red+=1
    Nt=zmean(N)
    iz=1
    while not Nt>(N_thr_success+ro/iz**0.8):#/np.sqrt(iz)):#N_thr_success
        if  Nt<(N_thr_repump) or N_red%N_thr_red==0:#-np.sqrt(N_thr_repump/iz)
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_green+=1
            N=np.zeros(N_thr_red)
            iz=0
        
        N[iz]=measure_counts(If)
        iz+=1
        N_red+=1
        Nt=zmean(N)
    Delta=f_res-f_red
    return Delta, N_green, N_red, Nt, f_res

@jit(nopython=True)#,parallel=True)
def CRcheck_mem(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,N_thr_red=15): #experiment=False #using while loop
    N_thr_red=15
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    n=50
    length=25
    If=get_counts(f_res,dt/n*length)
    #N=0
    #for i in range(length):
    #    N+=measure_counts(If)
    totallen=length
    N=measure_counts(If)
    N=n/totallen*N
    N_red+=1
    while not N>N_thr_success:#N_thr_success,min(iz,2)
        if  N<N_thr_repump or N_red%N_thr_red==0:#-np.sqrt(N_thr_repump/iz)
            f_res=get_f_res(sigma)
            #If=get_counts(f_res,dt/n)
            If=get_counts(f_res,dt/n*length)
            totallen=0
            N_green+=1
            N=0
        #for j in range(length):
            #N+=measure_counts(If)
        N+=measure_counts(If)
        totallen+=length
        totallen=min(2*length,totallen)
        N=n/totallen*N
        N_red+=1
    If=get_counts(f_res,dt)    
    Delta=f_res-f_red
    return Delta, N_green, N_red*length/n, If, f_res


@jit(nopython=True)#,parallel=True)
def CRcheck_radical(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,N_thr_red=15): #experiment=False #using while loop
    N_thr_red=15
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    n=50
    length=25
    If=get_counts(f_res,dt/n*length)
    #N=0
    #for i in range(length):
    #    N+=measure_counts(If)
    N=measure_counts(If)
    N=n/length*N
    N_red+=1
    while not N>N_thr_success:#N_thr_success,min(iz,2)
        if  N<N_thr_repump or N_red%N_thr_red==0:#-np.sqrt(N_thr_repump/iz)
            f_res=get_f_res(sigma)
            #If=get_counts(f_res,dt/n)
            If=get_counts(f_res,dt/n*length)
            N_green+=1
        #N=0
        #for j in range(length):
            #N+=measure_counts(If)
        N=measure_counts(If)
        N=n/length*N
        N_red+=1
    #If=get_counts(f_res,dt)    
    Delta=f_res-f_red
    return Delta, N_green, N_red*length/n, N, f_res

@jit(nopython=True)#,parallel=True)
def CRcheck_radical2(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,N_thr_red=15): #experiment=False #using while loop
    N_thr_red=15
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    chi=1./5
    #length=25
    Ifi=get_counts(f_res,dt*chi)
    #N=0
    #for i in range(length):
    #    N+=measure_counts(If)
    Ni=measure_counts(Ifi)
    N_red+=chi
    N_thr_success_init=3# 4 works
    i=0
    while i<3:
        while not Ni>N_thr_repump:
                f_res=get_f_res(sigma)
                N_green+=1
                Ifi=get_counts(f_res,dt*chi)
                Ni=measure_counts(Ifi)
                N_red+=1*chi
        Ni=measure_counts(Ifi)
        N_red+=1*chi
        i+=1
    If=get_counts(f_res,dt)
    N=measure_counts(If)
    while not N>N_thr_success:#N_thr_success,min(iz,2)
        if  N<N_thr_repump or N_red%N_thr_red==0:#-np.sqrt(N_thr_repump/iz)
            
            f_res=get_f_res(sigma)
            #If=get_counts(f_res,dt/n)
            If=get_counts(f_res,dt)
            N_green+=1
        #N=0
        # for j in range(length):
            #N+=measure_counts(If)
        N=measure_counts(If)
        #N=n/length*N
        N_red+=1
    If=get_counts(f_res,dt)    
    Delta=f_res-f_red
    return Delta, N_green, N_red, N, f_res

@jit(nopython=True)#,parallel=True)
def CRcheck_step(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,N_thr_red=15): #experiment=False #using while loop
    N_thr_red=15
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    If=get_counts(f_res,dt/3)
    N=measure_counts(If)
    N_red+=1./3
    #N_thr_success_init=3# 4 works
    while not N>N_thr_success:#,min(iz,2)
        if  N>=N_thr_repump:# or N_red%N_thr_red==0:#-np.sqrt(N_thr_repump/iz)
            If=get_counts(f_res,dt/4)
            N=measure_counts(If)
            N_red+=1./4
            if N>=1:
                If=get_counts(f_res,dt/6)
                N=measure_counts(If)
                N_red+=1./6
                if N>=1:
                    If=get_counts(f_res,dt/6)
                    N=measure_counts(If)
                    N_red+=1./12
                    if N>=1:
                        If=get_counts(f_res,dt)
                        N=measure_counts(If)
                        N_red+=1
                
        # for j in range(length):
            #N+=measure_counts(If)
            #N=measure_counts(If)
        #N=n/length*N
        else:
            f_res=get_f_res(sigma)
            N_green+=1
            If=get_counts(f_res,dt/3)
            N=measure_counts(If)
            N_red+=1./3
        N_red+=1
    If=get_counts(f_res,dt)    
    Delta=f_res-f_red
    return Delta, N_green, N_red, N, f_res



@jit(nopython=True)
def CRcheck_new5(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,ro=10): #experiment=False #using while loop
    N_thr_red=5
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    N_thr_success_arr=np.zeros(N_thr_red)
    N_thr_success_arr[0]=N_thr_success
    N_thr_success_arr[1]=N_thr_success-0.6
    N_thr_success_arr[2:]=N_thr_success-1
    #N_thr_success_arr[3]=N_thr_success-1.3
    #N_thr_success_arr[4:]=N_thr_success-1.5
    #N_thr_success_arr[3:]=9.5#:]=ro
    N_thr_repump_arr=np.zeros(N_thr_red)
    N_thr_repump_arr[0]=N_thr_repump
    N_thr_repump_arr[1]=N_thr_repump+0.5
    N_thr_repump_arr[2:]=N_thr_repump+1
    #N_thr_repump_arr[3]=N_thr_repump+1.3
    #N_thr_repump_arr[4:]=N_thr_repump+1.5
    #N=0
    # eps=3
    # if not Nold > (N_thr_success+eps):
    #     f_res=get_f_res(sigma)
    #     N_green+=1
    If=get_counts(f_res,dt)
    N=np.zeros(5)#N_thr_red)
    N[0]=measure_counts(If)
    N_red+=1
    Nt=zmean(N)
    iz=0
    while not Nt>N_thr_success_arr[iz]:#N_thr_success,min(iz,2)
        iz+=1 
        if  Nt<(N_thr_repump_arr[iz-1]) or N_red%N_thr_red==0:#-np.sqrt(N_thr_repump/iz)
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_green+=1
            N=np.zeros(N_thr_red)
            iz=0
        
        N[iz%3]=measure_counts(If)
        N_red+=1
        Nt=zmean(N)
    Delta=f_res-f_red
    return Delta, N_green, N_red, Nt, f_res

@jit(nopython=True)
def CRcheck_r2(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,ro=10): #experiment=False #using while loop
    N_thr_red=15
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    N_thr_success_arr=np.zeros(N_thr_red)
    N_thr_success_arr[0:]=N_thr_success
    N_thr_success_arr[1]=N_thr_success-0.6
    N_thr_success_arr[2:]=N_thr_success-1
    # N_thr_success_arr[3]=N_thr_success-1.3
    # N_thr_success_arr[4:]=N_thr_success-1.5
    #N_thr_success_arr[3:]=9.5#:]=ro
    N_thr_repump_arr=np.zeros(N_thr_red)
    N_thr_repump_arr[0:]=N_thr_repump
    N_thr_repump_arr[1]=N_thr_repump+0.5
    # N_thr_repump_arr[2]=N_thr_repump+1
    # N_thr_repump_arr[3]=N_thr_repump+1.3
    # N_thr_repump_arr[4:]=N_thr_repump+1.5
    #N=0
    # eps=3
    # if not Nold > (N_thr_success+eps):
    #     f_res=get_f_res(sigma)
    #     N_green+=1
    If=get_counts(f_res,dt)
    N=np.zeros(N_thr_red)
    N[0]=measure_counts(If)
    N_red+=1
    Nt=N[0]+0.5*zvar(N)#zmean(N)+0.5*zvar(N)
    iz=0
    while not Nt>N_thr_success_arr[iz]:#N_thr_success,min(iz,2)
        iz+=1 
        if  Nt<(N_thr_repump_arr[iz-1]) or N_red%N_thr_red==0:#-np.sqrt(N_thr_repump/iz)
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_green+=1
            N=np.zeros(N_thr_red)
            iz=0
        
        N[iz]=measure_counts(If)#%15
        N_red+=1
        Nt=N[iz]+0.5*zvar(N)
    Delta=f_res-f_red
    return Delta, N_green, N_red, zmean(N), f_res


@jit(nopython=True)
def CRcheck_new4(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,N_thr_red=20): #experiment=False #using while loop
    #N_thr_red=20
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    eps=5
    #N=0
    if not Nold > (N_thr_success+eps):
        f_res=get_f_res(sigma)
        N_green+=1
    If=get_counts(f_res,dt)
    Nt=measure_counts(If)
    N_red+=1
    while not Nt>N_thr_success:
        if  Nt<N_thr_repump or N_red%20==0:
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_green+=1
            Nt=measure_counts(If)
            N_red+=1
        else:
            Nt=measure_counts(If)
            #Nt=Nt/2
            N_red+=1
    Delta=f_res-f_red
    return Delta, N_green, N_red, Nt, f_res



# for i in range(30):
#     print(CRcheck_new())


def set_Nthr(percentage, dt=dt,num=100000): #percentage of states that succeeds (counted from the highest)
    maxI=get_counts(f_red,dt)
    counts=np.zeros(num)
    for i in range(num):
        counts[i]=measure_counts(maxI)
    #print(counts)
    counts.sort()
    Nt=int(counts[-int(round(percentage*num))])
    print("maximal intensity"+str(maxI))
    print("N_threshold_success"+str(Nt))
    return Nt


@jit(nopython=True)
def zmean(x):
    if not int(x[len(x)-1])==0:
        return np.mean(x)
    for i in range(len(x)):
        if int(x[i])==0:
            if i==0:
                return 0
            else:
                #print( np.mean(x[:i]))
                return np.mean(x[:i])

@jit(nopython=True)
def zvar(x):
    if not int(x[len(x)-1])==0:
        return np.var(x)
    for i in range(len(x)):
        if int(x[i])==0:
            if i==0:
                return 0
            else:
                #print( np.mean(x[:i]))
                return np.var(x[:i])                



@jit(nopython=True, parallel=True)
def get_results2(function, amount=5000,a=30): 
    d=np.zeros(amount)
    N_green=np.zeros(amount)
    N_red=np.zeros(amount)
    #TIME1=time()
    for i in prange(amount):
        Delta, N_g, N_r, N_a,f=function(N_thr_success=a)#1/(j+1))
        d[i]=Delta
        N_green[i]=N_g
        N_red[i]=N_r#*(dt_gauss+dt)+dt)
    #TIME2=time()
    #print("Duration of the loop")
    #print(TIME2-TIME1)
    ##    print()
    ##    print("Resultaten")
    ##    print("Delta = "+str(Delta))
    ##    print("iteration = "+str(iteration))
    ##    print()
    return d, N_green, N_red



#y_arr=1./np.sqrt(2*np.pi*var)*np.exp(-0.5*(x_arr-mu)**2/var)
    #plt.plot(x_arr,y_arr,label="Normal Fit")
    
    # #Code for adding a generalized normal fit
    # #sorted_d=d.sort()
    # x_fit=np.linspace(np.min(d),np.max(d),number_of_bins)
    # y_fit=np.zeros(number_of_bins)
    # for k in range(number_of_bins):
    #     for l in range(amount):
    #         if d[l]>x_fit[k] and d[l]<x_fit[k+1]:
    #             y_fit[k]+=1
    # y_fit=y_fit/np.linalg.norm(y_fit)/(x_fit[1]-x_fit[0])
    # print(y_fit)
    # popt,pcov=curve_fit(generalized_normal,x_fit,y_fit)
    # mu_fit=popt[0]
    # print("mu_fit="+str(mu_fit))
    # beta_fit=popt[1]
    # print("mu_fit="+str(mu_fit))
    # alpha_fit=popt[2]
    # plt.plot(x_arr,generalized_normal(x_arr, mu_fit, beta_fit, alpha_fit), label="Generalized normal fit")
    #plt.plot(label="with mu="+str(mu))
    

# plt.figure()
    # #plt.title("Iterations untill success for "+str(len(d))+" iterations")
    # #plt.ylabel("Occurence (normalized)") #surpessed to have a better overview
    # #plt.xlabel("Time (s)")
    # plt.xlabel("Iteration")
    # x_lst1=np.linspace(np.min(N_green),np.max(N_green),20)
    # #print(x_lst1)
    # p=1./mu_green
    # print("Chance on success p = "+str(p) )
    # print()
    # y_lst1=p*((1-p)**(x_lst1))#-1
    # #print(y_lst1)
    # # plt.hist(itera,bins=20,density=True)#bin_loc)
    # plt.hist(N_green,bins=bin_loc,density=True)#bin_loc)
    # plt.plot(x_lst1,y_lst1, linewidth=2, label="Geometric fit")
    # #y_lst2=p*((1-p)**bin_loc)
    # #plt.plot(bin_loc,y_lst2, label="Geometric fit 2", linestyle="--")
    # plt.legend()
    # plt.show()


# #mask=Y<X
    # #X[mask]=np.nan
    # #Y[mask]=np.nan
    # # print(mask)
    # # print("xs shape")
    # # print(Xs.shape)
    # # print(Xs)
    # #print (X.shape)
    # # thr_s_max=max(N_thr_success_arr)
    # # thr_s_min=min(N_thr_success_arr)
    # # thr_r_max=max(N_thr_repump_arr)
    # # thr_r_min=min(N_thr_repump_arr)
    # # thrmin=max(thr_s_min,thr_r_min)
    # # thrmax=min(thr_s_max,thr_r_max)
    # # x_line = np.arange(thrmin,thrmax,step=1)
    # # y_line = np.arange(thrmin,thrmax,step=1)
    # # z_0=np.zeros(len(x_line))
    # # z_1=np.ones(len(x_line))
    # # z_try=np.arange(min(SD_arr),max(SD_arr),step=1)
    # #print (SD_arr.shape)
    # #print (Y.shape)


    # # tPlane_arr=Plane_arr.T
    
    #ax.plot_surface(X, Y, tPlane_arr/1E6 ,color="orange")#label="13 MHZ", )
    #current_cmap = cm.coolwarm
    #current_cmap.set_bad(color='red')
    #print(np.nanmin(tSD_arr))
    #print(np.nanmax(tSD_arr))



    # # ax = fig.add_subplot(1, 3, 3, projection='3d')
    # # plt.title("Probability to succeed") #Calculated with the geometric formula and normalization
    # # ax.set_xlabel("N_thr_success (#)")
    # # ax.set_ylabel("N_thr_repump (#)")
    # # ax.set_zlabel("Probability (/)")
    # # tP_arr=P_arr.T
    # # ax.plot_surface(X, Y, tP_arr, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    # # plt.show()

     # # tPlane_arr=Plane_arr.T
    
    # # ax.plot_surface(X, Y, tPlane_arr/1E6 ,color="orange")#label="13 MHZ", )
    # #current_cmap = cm.coolwarm
    # #current_cmap.set_bad(color='red')
    # print(np.nanmin(tSD_arr))
    # print(np.nanmax(tSD_arr))



    # #finding optimal values
    # #Constraint 13 MHZ
    # c1=tSD_arr<13E6
    # #finding min average time
    # c2=min(tMuT_arr[c1])
    # #print(np.where(tMuT_arr==c2)[1][0])
    # place=np.where(tMuT_arr==c2)
    # loc=(place[0][0],place[1][0])
    # opt_N_thr_success=X[loc]
    # opt_N_thr_repump=Y[loc]
    # opt_SD=tSD_arr[loc]
    # opt_MuT=tMuT_arr[loc]
    # print()
    # print("At sigma = "+str(sigma))
    # print("Optimal Location at N_thr_success = "+str(opt_N_thr_success)+" and N_thr_repump = "+str(opt_N_thr_repump))
    # print("Spectral diffusion = "+str(opt_SD))
    # print("Mean time untill success = "+str(opt_MuT))



    #ax.tick_params(axis="z", labelcolor="b", pad=8)
    #plt.legend()
    # plt.twinx()
    # ax.plot_surface(X/1E6, Y*1E3, opt_SD_arr.T/1E63, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="label="F_resonance"")
    # ax.tick_params(axis="z", labelcolor="r", pad=8)
    # ax.set_zlabel("Frequency (MHZ)", color='r')  
    # plt.legend()