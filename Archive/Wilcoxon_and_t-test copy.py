import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sp
from scipy.stats import poisson
import random as rd
from numba import njit, jit, prange
from time import time
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import math
from scipy.optimize import curve_fit

# import cProfile
# import re
# cProfile.run('re.compile("foo|bar")')
core=8

#Parameters
c=299792458         #m/s    #speed of light
lambda_red=637E-9   #m      #golflengte laser
f_red=c/lambda_red  #1/s    #frequentie laser
sigma=50E6          #1/s    #variantie laser
LL_linewidth=13E6   #1/s    #Lifetime limited linewidth lorentzian shape

#Choose:
N_tot=0.3E14        #-      #Total number of electrons (werken:1E18 met v2 )
N_thr_succes=30     #-      #Treshold number (werken: 1E6 )
N_thr_repump=5      #-      #Treshold number (werken: 1E6 )
dt=50E-6            #s
dt_gauss=400E-6     #s

def generalized_normal(x, mu=0, beta=2, alpha=1):
    return beta/2/alpha/math.gamma(1/beta)*np.exp(-((abs(x-mu)/alpha)**beta))


@jit(nopython=True)
def get_f_res(sigma=sigma,f_red=f_red):
    #return np.random.normal(f_red,sigma)
    return rd.gauss(f_red,sigma)

##print("f_red")
##print(str(f_red))
##print()
##for i in range(20):
##    print(get_f_res())
##print()

@jit(nopython=True)
def lorentzian(x,x_0=0.,gamma=1.):
    return 1./(1.+((x-x_0)*(x-x_0)/gamma/gamma))/np.pi/gamma

@jit(nopython=True)
def get_counts(f_resonance,dt=dt): #nog om aan te passen
    #f=sp.cauchy.pdf(f_red,f_resonance,LL_linewidth)
    #return dt*N_tot*sp.cauchy.pdf(f_red,f_resonance,LL_linewidth)
    return dt*N_tot*lorentzian(f_red,f_resonance,LL_linewidth)

print("max value lamda")
print(get_counts(f_red))
print()
print("minimal value lamda needed to pass")
minval=int(np.floor(get_counts(f_red-LL_linewidth)))
print(get_counts(f_red-LL_linewidth))
print(get_counts(f_red+LL_linewidth))
print()
# print("Counts per cr")

# for i in range(60):
# #    print("Lorentz")
# #    print(lorentzian(1,2,1+i))#,0,LL_linewidth))
# #    print(sp.cauchy.pdf(1,2,1+i))#,0,LL_linewidth))

# #    print("diff")
# #    print(lorentzian(1,2,1+i)/(sp.cauchy.pdf(1,2,1+i)))
#     f=get_f_res()
#     print(get_counts(f))        


@jit(nopython=True)
def measure_counts(N_fotons): #laserduur
    return np.random.poisson(N_fotons)
    #return poisson.rvs(N_fotons,size=num)#int(round)

@jit(nopython=True)
def CRcheck(N_thr_succes=N_thr_succes,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,N_thr_red=20): #experiment=False #using while loop
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    #N=0
    If=get_counts(f_res,dt)
    N=measure_counts(If)
    N_arr=np.zeros(N_thr_red)
    N_arr[0]=N
    N_red+=1
    i=1
    while not N>N_thr_succes:
        if N<N_thr_repump or N_red%N_thr_red==0:#N_red%100==0:
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_arr=np.zeros(N_thr_red)
            i=0
            N_green+=1
        N=measure_counts(If)
        N_arr[i]=N
        i+=1
        N_red+=1
    Delta=f_res-f_red
    return Delta, N_green, N_red, N_arr, f_res



n=5
f_res=get_f_res(sigma)
delta=f_res-f_red
#print("delta in MHz = "+str(delta/1E6))
If=get_counts(f_res,dt)#24
#If=24
#print("If = "+str(If))
x=np.random.poisson(minval,size=n)
print(x)#[2:])
y=np.random.poisson(minval,size=n)#np.ones(n,dtype=int)*minval#
print(y)
x=np.zeros(4)
x[0]=6
x[1]=7
x[2]=8
x[3]=7
y=np.zeros(4)
y[0]=6
y[1]=8
y[2]=6
y[3]=6
stat,p=sp.wilcoxon(x,y,alternative="greater")

print("statistic")
print(stat)
print()
print("p-value")
print(p)

# print("If = "+str(If))
# N=np.zeros(5)#N_thr_red)
# N[0]=measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)
# print(N)

# bin_loc=np.arange(start=-3*LL_linewidth, stop=3*LL_linewidth,step=LL_linewidth/20)
# z=sp.cauchy.rvs(loc=0, scale=LL_linewidth, size=10000, random_state=None)
# print("mean = "+str(np.mean(z)/1E6))
# print("variance = "+str(np.var(z)))
# print("sd = "+str(np.std(z)/1E6))
# print("sd2 = "+str(np.sqrt(np.var(z))/1E6))
# plt.figure()
# plt.hist(z,bins=bin_loc)
# plt.show()


#@jit(nopython=True)
def CRcheck_routine(N_thr_succes=0.01,N_thr_repump=0.5,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,minval=18.3): #experiment=False #using while loop
    N_thr_red=10
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    If=get_counts(f_res,dt)
    N=measure_counts(If)
    N_arr=np.zeros(N_thr_red)
    y=np.ones(N_thr_red)*minval#np.random.poisson(minval,size=N_thr_red)
    N_arr[0]=N
    stat,p=sp.wilcoxon(N_arr[:1],y[:1],zero_method='wilcox',alternative="greater")#sp.ttest_1samp(N_arr[:1],minval)#
    #print(p)
    N_red+=1
    i=1
    while not p<N_thr_succes:
        if p>N_thr_repump or N_red%N_thr_red==0:#N_red%100==0:
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_arr=np.zeros(N_thr_red)
            i=0
            N_green+=1
        N=measure_counts(If)
        N_arr[i]=N  
        i+=1
        N_red+=1

        stat,p=sp.wilcoxon(N_arr[:(i)],y[:(i)],zero_method='wilcox', alternative="greater")#sp.ttest_1samp(N_arr[:(i)],minval)#
    Delta=f_res-f_red
    return Delta, N_green, N_red, np.mean(N_arr[:(i)]), f_res


def CRcheck_routine_t(N_thr_succes=0.01,N_thr_repump=0.5,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,minval=18.3): #experiment=False #using while loop
    N_thr_red=10
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    #N=0
    If=get_counts(f_res,dt)
    N=measure_counts(If)
    N_arr=np.zeros(N_thr_red)
    y=np.ones(N_thr_red)*minval#np.random.poisson(minval,size=N_thr_red)
    N_arr[0]=N
    stat,p=sp.ttest_1samp(N_arr[:1],minval-3)#sp.wilcoxon(N_arr[:1],y[:1],zero_method='wilcox',alternative="greater")#
    p=p/2 #one-sided
    #print(p)
    N_red+=1
    i=1
    while not (p<N_thr_succes and np.mean(N_arr[:(i)])>(minval-3)):
        if p>N_thr_repump or N_red%N_thr_red==0:#N_red%100==0:
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_arr=np.zeros(N_thr_red)
            i=0
            N_green+=1
        N=measure_counts(If)
        N_arr[i]=N  
        i+=1
        N_red+=1
        stat,p=sp.ttest_1samp(N_arr[:i],minval-3)#sp.wilcoxon(N_arr[:(i)],y[:(i)],zero_method='wilcox', alternative="greater")#
        p=p/2
    Delta=f_res-f_red
    return Delta, N_green, N_red, np.mean(N_arr[:(i)]), f_res


#print(CRcheck_routine())

#@jit(nopython=True,parallel=True)
def get_results(function, a, b, c, dt=dt, minval=18.3, amount=125):
    #print() 
    #print("N_thr_green = "+str(a))
    #print("N_thr_repump = "+str(b)) 
    #print("running")
    d=np.zeros(amount*core)
    N_green=np.zeros(amount*core)
    N_red=np.zeros(amount*core)
    N_on_succes=np.zeros(amount*core)
    for j in range(core):
        f_res=get_f_res()
        N_old=0
        for i in range(amount):
            d[i+j*amount], N_green[i+j*amount], N_red[i+j*amount], N_old,f_res_old=function(a,b,c,dt,f_res,N_old,minval)
            N_on_succes[i+j*amount]=N_old
            f_res=get_f_res(c/5,f_res_old)#usual is /5 met /3
            # N, Delta, N_g, N_r=function(a,b,c)
            # d[i]=Delta
            # N_green[i]=N_g
            # N_red[i]=N_r #*(dt_gauss+dt)+dt)
    N_avg=np.mean(N_on_succes)
    # count, bins, ignored = plt.hist(d, bins=number_of_bins ,density=True)
    # guess=np.zeros(3)
    # guess[0]=mu
    # guess[1]=10
    # guess[2]=1.28*np.sqrt(2*var)
    # popt,pcov=curve_fit(generalized_normal,(bins[:-1]+bins[1:])/2,count,p0=guess)
    # mu_fit=popt[0]
    # #print("mu_fit="+str(mu_fit))
    # beta_fit=popt[1]
    # #print("beta_fit="+str(beta_fit))
    # alpha_fit=popt[2]
    # #print("alpha_fit="+str(alpha_fit))   
    mu=np.mean(d)
    var=np.var(d)
    sd=np.sqrt(var)
    #print("Delta = "+str(mu)+" +/- "+str(sd))
    mu_g=np.mean(N_green)
    mu_r=np.mean(N_red)
    meantime=dt_gauss*mu_g+dt*mu_r
    #print("Mean (time untill success) in seconds = "+str(meantime))
    p=(dt_gauss+dt)/meantime
    # print("Chance on succes p = "+str(p) )
    #return alpha_fit, meantime, p, N_avg
    return sd, meantime, p, N_avg

#@jit(nopython=True)
def find_opt(constraint,tSD_arr, tMuT_arr, tN_arr, X, Y,sigma,dt=dt):
        #finding optimal values
    #Constraint 13 MHZ
    c1=tSD_arr<constraint*1E6
    #finding min average time
    c2=np.min(tMuT_arr[c1])
    #print(np.where(tMuT_arr==c2)[1][0])
    place=np.where(tMuT_arr==c2)
    loc=(place[0][0],place[1][0])
    opt_N_thr_succes=X[loc]
    opt_N_thr_repump=Y[loc]
    opt_SD=tSD_arr[loc]
    opt_MuT=tMuT_arr[loc]
    opt_N=tN_arr[loc]
    print()
    print("At sigma = "+str(sigma))
    print("At dt = "+str(dt))
    print("Optimal Location at N_thr_succes = "+str(opt_N_thr_succes)+" and N_thr_repump = "+str(opt_N_thr_repump))
    print("Spectral diffusion = "+str(opt_SD))
    print("Mean time untill succes = "+str(opt_MuT))
    print("Mean number of photons= "+str(opt_N))
    return opt_N_thr_succes, opt_N_thr_repump, opt_SD, opt_MuT, opt_N


#@jit(nopython=True)
def optimal_results(function,g_num,r_num, sigma=sigma, max_N_thr_succes=0.2, min_N_thr_repump=0.5,dt=dt, min_N_thr_succes=0.01, max_N_thr_repump=1.0):
    minval=get_counts(f_red-LL_linewidth,dt)
    #N_thr_succes=start_N_thr_succes
    #N_thr_repump=start_N_thr_repump
    N_thr_succes_arr=np.linspace(min_N_thr_succes,max_N_thr_succes,num=g_num)
    N_thr_repump_arr=np.linspace(min_N_thr_repump,max_N_thr_repump,num=r_num)
    SD_arr= np.zeros((g_num,r_num))
    MuT_arr=np.zeros((g_num,r_num))
    P_arr=np.zeros((g_num,r_num))
    N_arr=np.zeros((g_num,r_num))
    Plane_arr=np.zeros((g_num,r_num))
    X,Y=np.meshgrid(N_thr_succes_arr,N_thr_repump_arr)
    for i in range(g_num):
        for j in range(r_num):
            
            SD_arr[i,j], MuT_arr[i,j] , P_arr[i,j], N_arr[i,j]=get_results(function,N_thr_succes_arr[i],N_thr_repump_arr[j],sigma,dt, minval ) #, P_arr[i,j]
            Plane_arr[i,j]=11E6
            #print("Check")
            # else:
            #     SD_arr[i,j], MuT_arr[i,j] , P_arr[i,j], N_arr[i,j]=np.nan ,np.nan, np.nan, np.nan
            #     Plane_arr[i,j]=np.nan


    
    # #mask=Y<X
    # #X[mask]=np.nan
    # #Y[mask]=np.nan
    # # print(mask)
    # # print("xs shape")
    # # print(Xs.shape)
    # # print(Xs)
    # #print (X.shape)
    # # thr_s_max=max(N_thr_succes_arr)
    # # thr_s_min=min(N_thr_succes_arr)
    # # thr_r_max=max(N_thr_repump_arr)
    # # thr_r_min=min(N_thr_repump_arr)
    # # thrmin=max(thr_s_min,thr_r_min)
    # # thrmax=min(thr_s_max,thr_r_max)
    # # x_line = np.arange(thrmin,thrmax,step=1)
    # # y_line = np.arange(thrmin,thrmax,step=1)
    # # z_0=np.zeros(len(x_line))
    # # z_1=np.ones(len(x_line))
    # # z_try=np.arange(min(SD_arr),max(SD_arr),step=1)
    # #print (SD_arr.shape)
    # #print (Y.shape)
    # fig = plt.figure()
    # ax = fig.add_subplot(1, 3, 1, projection='3d')      
    # plt.title("Deviation of distance upon succes")
    # ax.set_xlabel("N_thr_succes (#)")
    # ax.set_ylabel("N_thr_repump (#)")
    # ax.set_zlabel("Frequency (MHz)")
    tPlane_arr=Plane_arr.T
    tSD_arr=SD_arr.T
    # ax.plot_surface(X, Y, tPlane_arr/1E6 ,color="orange")#label="13 MHZ", )
    # ax.plot_surface(X, Y, tSD_arr/1E6, cmap=cm.coolwarm, linewidth=0, antialiased=False)

    #, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    #X_try,Z_try=np.meshgrid(x_line,z_try)
    #ax.plot_surface(X_try,X_try, Z_try/1E6)
    #ax.plot(x_line, y_line, z_0, label='N_thr_succes<Nthr repump')#line for separation
    #ax.plot(x_line, y_line, z_1, label='N_thr_succes<Nthr repump')#line for separation
    #ax = fig.add_subplot(1, 3, 2, projection='3d')
    # plt.title("Mean (time untill success)")
    # ax.set_xlabel("N_thr_succes (#)")
    # ax.set_ylabel("N_thr_repump (#)")
    # ax.set_zlabel("Time (ms)")
    #plt.plot(Thr_arr,MuT_arr)
    tMuT_arr=MuT_arr.T
    tN_arr=N_arr.T
    #ax.plot_surface(X, Y, tMuT_arr*1E3, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    
    # ax = fig.add_subplot(1, 3, 3, projection='3d')
    # plt.title("Optimal photon counts") #Calculated with the geometric formula and normalization
    # ax.set_xlabel("sigma (MHz)")
    # ax.set_ylabel("dt (ms)")
    # ax.set_zlabel("Photons (#)")
    # ax.plot_surface(X/1E6, Y*1E3, tN_arr, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Average lambda")
    # #plt.legend()
    # plt.show()

    # ax = fig.add_subplot(1, 3, 3, projection='3d')
    # plt.title("Probability to succeed") #Calculated with the geometric formula and normalization
    # ax.set_xlabel("N_thr_succes (#)")
    # ax.set_ylabel("N_thr_repump (#)")
    # ax.set_zlabel("Probability (/)")
    # tP_arr=P_arr.T
    # ax.plot_surface(X, Y, tP_arr, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    # plt.show()

    opt_N_thr_succes, opt_N_thr_repump, opt_SD, opt_MuT, opt_N=find_opt(11,tSD_arr, tMuT_arr, tN_arr, X, Y,sigma,dt)#13/2

    #finding optimal values
    # #Constraint 13 MHZ
    # c1=tSD_arr<13E6
    # #finding min average time
    # c2=min(tMuT_arr[c1])
    # #print(np.where(tMuT_arr==c2)[1][0])
    # place=np.where(tMuT_arr==c2)
    # loc=(place[0][0],place[1][0])
    # opt_N_thr_succes=X[loc]
    # opt_N_thr_repump=Y[loc]
    # opt_SD=tSD_arr[loc]
    # opt_MuT=tMuT_arr[loc]
    # print()
    # print("At sigma = "+str(sigma))
    # print("Optimal Location at N_thr_succes = "+str(opt_N_thr_succes)+" and N_thr_repump = "+str(opt_N_thr_repump))
    # print("Spectral diffusion = "+str(opt_SD))
    # print("Mean time untill succes = "+str(opt_MuT))

    return opt_N_thr_succes, opt_N_thr_repump, opt_SD,  opt_MuT, opt_N
    
#optimal_results(CRcheck_routine,10,4, sigma=2*sigma, max_N_thr_succes=0.84 , min_N_thr_repump=0.70 ,dt=dt, N_thr_red=20)

def variation_2D(function=CRcheck,dt_length=1,dt_start=0.5,dt_step=0.1,sigma_length=100,sigma_start=0,sigma_step=10):
    sigma_arr=1E6*np.arange(sigma_start,sigma_start+sigma_length+sigma_step,step=sigma_step)
    dt_arr=dt*np.arange(dt_start,dt_start+dt_length+dt_step,step=dt_step)
    sigma_size=len(sigma_arr)
    dt_size=len(dt_arr)
    opt_N_thr_succes_arr=np.zeros(sigma_size)
    opt_N_thr_repump_arr=np.zeros(sigma_size)
    opt_SD_arr=np.zeros(sigma_size)
    opt_MuT_arr=np.zeros(sigma_size)
    opt_N_arr=np.zeros(sigma_size)
    opt_N_thr_succes_arr2=np.zeros(dt_size)
    opt_N_thr_repump_arr2=np.zeros(dt_size)
    opt_SD_arr2=np.zeros(dt_size)
    opt_MuT_arr2=np.zeros(dt_size)
    opt_N_arr2=np.zeros(dt_size)      
    #X,Y=np.meshgrid(sigma_arr,dt_arr)      
    #X,Y=np.meshgrid(sigma_arr,dt_arr)
    for i in range(sigma_size):
        print("Check")
        opt_N_thr_succes_arr[i], opt_N_thr_repump_arr[i], opt_SD_arr[i],  opt_MuT_arr[i], opt_N_arr[i]=optimal_results(function,12,8,sigma_arr[i],0.95,0.85,dt,min_N_thr_succes=0.45, max_N_thr_repump=1.0)
    #for j in range(dt_size):
        #minval=int(np.floor(get_counts(f_red-LL_linewidth)))
        #Np=int(min(np.ceil(get_counts(f_red,dt=dt_arr[j])),35))
    #    opt_N_thr_succes_arr2[j], opt_N_thr_repump_arr2[j], opt_SD_arr2[j],  opt_MuT_arr2[j], opt_N_arr2[j]=optimal_results(function,12,10,sigma,0.84,0.50,dt_arr[j])#np.max(Np-40,0)

    plt.figure()
    plt.title("Optimal N_thresholds for varying sigma, dt = "+str(dt*1E3)+"ms")
    plt.plot(sigma_arr/1E6,opt_N_thr_succes_arr,label="N_thr_succes") 
    plt.plot(sigma_arr/1E6, opt_N_thr_repump_arr,label="N_thr_repump") 
    plt.legend()
    plt.xlabel("sigma (MHz)")
    plt.ylabel("N_thr (/)")
    plt.show()

    plt.figure()
    plt.title("Optimal time till succes and fequency with varying sigma, dt = "+str(dt*1E3)+"ms")
    plt.xlabel("sigma (MHz)")
    plt.ylabel("Time (ms)",color='b')
    plt.plot(sigma_arr/1E6,opt_MuT_arr*1E3,color='b',label="Mean (time till succes)")
    plt.tick_params(axis="y", labelcolor="b", pad=8)
    plt.legend(loc='upper left')
    plt.twinx()
    plt.plot(sigma_arr/1E6,opt_SD_arr/1E6,color='r',label="F_resonance")
    plt.tick_params(axis="y", labelcolor="r", pad=8)
    plt.ylabel("Frequency (MHz)", color='r')  
    plt.legend(loc='lower right')
    plt.show()


    plt.figure()
    plt.title("Optimal Photon counts with varying sigma, dt = "+str(dt*1E3)+"ms")
    plt.plot(sigma_arr/1E6,opt_N_arr,label="Average N")  
    plt.legend()
    plt.xlabel("sigma (MHz)")
    plt.ylabel("Photons (-)")
    plt.show()




    # plt.figure()
    # plt.title("Optimal N_thresholds for varying dt, sigma = "+str(dt/1E6)+"MHz")
    # plt.plot(dt_arr*1E3,opt_N_thr_succes_arr2,label="N_thr_succes") 
    # plt.plot(dt_arr*1E3, opt_N_thr_repump_arr2,label="N_thr_repump") 
    # plt.legend()
    # plt.xlabel("dt (ms)")
    # plt.ylabel("N_thr (/)")
    # plt.show()

    # plt.figure()
    # plt.title("Optimal time till succes and fequency with varying dt, sigma = "+str(dt/1E6)+"MHz")
    # plt.xlabel("dt (ms)")
    # plt.ylabel("Time (ms)",color='b')
    # plt.plot(dt_arr*1E3,opt_MuT_arr2*1E3,color='b',label="Mean (time till succes)")
    # plt.tick_params(axis="y", labelcolor="b", pad=8)
    # plt.legend(loc='upper left')
    # plt.twinx()
    # plt.plot(dt_arr*1E3,opt_SD_arr/1E6,color='r',label="F_resonance")
    # plt.tick_params(axis="y", labelcolor="r", pad=8)
    # plt.ylabel("Frequency (MHZ)", color='r')  
    # plt.legend(loc='lower right')
    # plt.show()


    # plt.figure()
    # plt.title("Optimal Photon counts, sigma = "+str(dt/1E6)+"MHz")
    # plt.plot(dt_arr*1E3,opt_N_arr2,label="Average N")  
    # plt.legend()
    # plt.xlabel("dt (ms)")
    # plt.ylabel("Photons (#)")
    # #plt.tight_layout()
    # plt.show()


    
variation_2D(function=CRcheck_routine_t,dt_length=1.5,dt_start=0.1,dt_step=0.1,sigma_length=100,sigma_start=0,sigma_step=10)
#variation_2D(function=CRcheck_new5,dt_length=1,dt_start=0.1,dt_step=0.1,sigma_length=100,sigma_start=0,sigma_step=10)

def variation_of_sigma_results(function=CRcheck,length=1000,start=0,step=10):
    #step=10
    sigma_arr=1E6*np.arange(start,start+length+step,step=step)
    size=len(sigma_arr)
    opt_N_thr_succes_arr=np.zeros(size)
    opt_N_thr_repump_arr=np.zeros(size)
    opt_SD_arr=np.zeros(size)
    opt_MuT_arr=np.zeros(size)
    opt_N_arr=np.zeros(size)
    for i in range(size):
        opt_N_thr_succes_arr[i], opt_N_thr_repump_arr[i], opt_SD_arr[i],  opt_MuT_arr[i], opt_N_arr[i]=optimal_results(function,25,25,sigma_arr[i],10,5)
    
    plt.subplot(1,3,1)
    plt.title("Optimal N_thresholds for varying sigma")
    plt.plot(sigma_arr/1E6,opt_N_thr_succes_arr,label="N_thr_succes") 
    plt.plot(sigma_arr/1E6, opt_N_thr_repump_arr,label="N_thr_repump") 
    plt.legend()
    plt.xlabel("Sigma (MHz)")
    plt.ylabel("N_thr")

    plt.subplot(1,3,2)
    plt.title("Optimal time till succes and fequency with varying sigma")
    plt.xlabel("Sigma (MHz)")
    plt.ylabel("Time (ms)",color='b')
    plt.plot(sigma_arr/1E6,opt_MuT_arr*1E3,color='b',label="Mean (time till succes)")
    plt.tick_params(axis="y", labelcolor="b", pad=8)
    #plt.legend(loc='upper left')
    plt.twinx()
    plt.plot(sigma_arr/1E6,opt_SD_arr/1E6,color='r',label="F_resonance")
    plt.tick_params(axis="y", labelcolor="r", pad=8)
    plt.ylabel("Frequency (MHZ)", color='r')  
    plt.legend(loc='lower right')
    


    plt.subplot(1,3,3)
    plt.title("Optimal photon counts")
    plt.plot(sigma_arr/1E6,opt_N_arr,label="Average lambda")  
    plt.legend()
    plt.xlabel("Sigma (MHz)")
    plt.ylabel("Photons (#)")

    plt.show()

    

#variation_of_sigma_results(function=CRcheck_new3,length=100, start=0,step=5)
#variation_of_sigma_results(function=CRcheck,length=100, start=0,step=10)

def variation_of_dt_results(function=CRcheck,percentage=0.85,length=1,start=0.5,step=0.1):
    #step=10
    dt_arr=dt*np.arange(start,start+length+step,step=step)
    size=len(dt_arr)
    opt_N_thr_succes_arr=np.zeros(size)
    opt_N_thr_repump_arr=np.zeros(size)
    opt_SD_arr=np.zeros(size)
    opt_MuT_arr=np.zeros(size)
    opt_N_arr=np.zeros(size)
    for i in range(size):
        #Ns=set_Nthr(percentage, dt_arr[i],num=100000)
        #Nr=int(round(Ns/3))
        Np=int(min(np.ceil(get_counts(f_red,dt=dt_arr[i])),20))
        opt_N_thr_succes_arr[i], opt_N_thr_repump_arr[i], opt_SD_arr[i],  opt_MuT_arr[i], opt_N_arr[i]=optimal_results(function,Np,Np,sigma,0,0,dt_arr[i])
    
    #plt.figure()
    #plt.tight_layout()
    plt.subplot(1,3,1)
    plt.title("Optimal N_thresholds for varying dt (Ntot)")
    plt.plot(dt_arr*1E3,opt_N_thr_succes_arr,label="N_thr_succes") 
    plt.plot(dt_arr*1E3, opt_N_thr_repump_arr,label="N_thr_repump") 
    plt.legend()
    plt.xlabel("dt (ms)")
    plt.ylabel("N_thr")

    plt.subplot(1,3,2)
    plt.title("Optimal time till succes and fequency with varying dt")
    plt.xlabel("dt (ms)")
    plt.ylabel("Time (ms)",color='b')
    plt.plot(dt_arr*1E3,opt_MuT_arr*1E3,color='b',label="Mean (time till succes)")
    plt.tick_params(axis="y", labelcolor="b", pad=8)
    plt.legend()
    plt.twinx()
    plt.plot(dt_arr*1E3,opt_SD_arr/1E6,color='r',label="F_resonance")
    plt.tick_params(axis="y", labelcolor="r", pad=8)
    plt.ylabel("Frequency (MHZ)", color='r')  
    plt.legend()
    


    plt.subplot(1,3,3)
    plt.title("Optimal photon counts")
    plt.plot(dt_arr*1E3,opt_N_arr,label="Average lambda")  
    plt.legend()
    plt.xlabel("dt (ms)")
    plt.ylabel("Photons (#)")
    #plt.tight_layout()
    plt.show()

#variation_of_dt_results(function=CRcheck,length=1,start=0.1,step=0.05)

































def CRcheck_routine(N_thr_succes=0.01,N_thr_repump=0.5,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,minval=18.3): #experiment=False #using while loop
    N_thr_red=10
    N_green=0
    N_red=0
    If=get_counts(f_res,dt)
    N=measure_counts(If)
    N_arr=np.zeros(N_thr_red)
    y=np.ones(N_thr_red)*minval#np.random.poisson(minval,size=N_thr_red)
    N_arr[0]=N
    stat,p=sp.ttest_1samp(N_arr[:1],minval-3)#sp.wilcoxon(N_arr[:1],y[:1],zero_method='wilcox',alternative="greater")#
    N_red+=1
    i=1
    while not (p<N_thr_succes and np.mean(N_arr[:(i)])>(minval)):
        if p>N_thr_repump or N_red%N_thr_red==0:#N_red%100==0:
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_arr=np.zeros(N_thr_red)
            i=0
            N_green+=1
        N=measure_counts(If)
        N_arr[i]=N  
        i+=1
        N_red+=1

        stat,p=sp.ttest_1samp(N_arr[:(i)],minval-3)#sp.wilcoxon(N_arr[:(i)],y[:(i)],zero_method='wilcox', alternative="greater")#
        p=p/2
    Delta=f_res-f_red
    return Delta, N_green, N_red, np.mean(N_arr[:(i)]), f_res



stat,p=sp.ttest_1samp(N_arr[:i],minval)
p=p/2

y=np.ones(N_thr_red)*minval



stat,p=sp.wilcoxon(N_arr[:i],y[:i],zero_method='wilcox', alternative="greater")