import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sp
import random as rd
from time import time


from numba import jit, njit, prange
import numpy as np


@njit(parallel=True)
def prange_ok_result_outer_slice(x):
    n = x.shape[0]
    y = np.zeros(4)
    z = y[:]
    for i in prange(n):
        z += x[i]
    return y

# prange_ok_result_outer_slice(np.linspace(1,10,num=11))
#print(y)

#Parameters
c=299792458         #m/s    #speed of light
lambda_red=637E-9   #m      #golflengte laser
f_red=c/lambda_red  #1/s    #frequentie laser
sigma=50E6
LL=13E6          #1/s    #variantie laser

#Choose:
N_tot=0.3E14          #-      #Total number of electrons (werken:1E18 met v2 )
N_thr=30            #-      #Treshold number (werken: 1E6 )
dt=50E-6            #s
dt_gauss=400E-6     #s


def get_f_res(j=1):  
    return rd.gauss(f_red,j*sigma)


def test_function(function, output_name, runs=20):
    print(output_name)
    print()
    for i in range(runs):
       print(function())
    print()

#test_function(get_f_res,"f_red")

##print("f_red")
##print()
##for i in range(20):
##    print(get_f_res())
##print()

def lorentzian(x,x_0,gamma=1):
    return 1./(1.+((x-x_0)/gamma)**2)

def get_counts(f_resonance): #nog om aan te passen
    #return N_tot*lorentzian(f_red,f_resonance)5
    return dt*N_tot*sp.cauchy.pdf(f_red,f_resonance,LL)

#print("max value of get counts = "+str(get_counts(f_red)))
def measure_counts(N_fotons): #laserduur
    return sp.poisson.rvs(N_fotons)#int(round(N_fotons)))


# bin_loc=[]
# for i in range(60):
#     bin_loc.append(i)

# #print("Counts per cr")
# num=50000
# counts_arr=np.zeros(num)
# for i in range(num):
#    f=get_f_res()
#    Nf=get_counts(f)
#    counts_arr[i]=(measure_counts(Nf))  

# print( "Max num of counts = "+str(np.max(counts_arr)))
# plt.figure()
# plt.hist(counts_arr, bins=bin_loc)
# plt.vlines(N_thr, 0, 2000)
# plt.show()

@jit(nopython=True,parallel=True)
def speedtest(length):
    arr=np.zeros(length)
    for i in prange(length):
        arr[i]=np.random.poisson(37.8)
    return arr

T=np.zeros(4)
length=100000000
T[0]=time()
x=np.random.poisson(37.8,size=length)
T[1]=time()
y=speedtest(length)
T[2]=time()
z=speedtest(length)
T[3]=time()

#print("mean of x = "+str(np.mean(x)))
#print("mean of y = "+str(np.mean(y)))
#print("mean of z = "+str(np.mean(z)))

for i in range(3):
    print("Time between T"+str(i)+" and T"+str(i+1))
    print(T[i+1]-T[i])