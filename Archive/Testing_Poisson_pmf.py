#Poisson test

import numpy as np
import scipy.stats as sp
import matplotlib.pyplot as plt

#Making the pmf
mu=10
##x=np.arange(sp.poisson.ppf(0.01,mu),sp.poisson.ppf(0.99,mu))
##y=sp.poisson.pmf(x,mu)
##plt.figure()
##plt.plot(x,y,"bo",ms=8,label="poisson pmf")
##plt.vlines(x,0,y,colors="b", lw=5 , alpha=0.5)
##plt.show()
##print(y)

amount=10000
x=np.arange(0,sp.poisson.ppf(0.999,amount+2),step=1)
mu1=np.zeros(amount)
mu2=np.zeros(amount)
cor=np.zeros(amount)
for i in range(0,amount):
    mu1[i]=i+1
    y1=sp.poisson.pmf(x,mu1[i])
    mu2[i]=i+2
    y2=sp.poisson.pmf(x,mu2[i])
    cor[i]=np.correlate(y1,y2)
    #print("Correlation between mu1 = "+str(mu1)+" and mu2 = "+str(mu2))
    #print(cor[i])



for j in range(20):
    n=50*j
    print("Correlation between mu1 = "+str(mu1[n])+" and mu2 = "+str(mu2[n]))
    print(cor[n])

    
plt.figure()
plt.plot(x,y1,"bo",ms=8,label="poisson pmf, mu = "+str(mu1))
plt.plot(x,y2,"ro",ms=8,label="poisson pmf, mu = "+str(mu2))
#plt.vlines(x,0,y,colors="b", lw=5 , alpha=0.5)
plt.legend()
plt.show()
