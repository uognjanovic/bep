import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sp
import random as rd
#from numba import njit, jit, prange
#from time import time
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

#Parameters
c=299792458         #m/s    #speed of light
lambda_red=637E-9   #m      #golflengte laser
f_red=c/lambda_red  #1/s    #frequentie laser
sigma=50E6          #1/s    #variantie laser
LL_linewidth=13E6   #1/s    #Lifetime limited linewidth lorentzian shape

#Choose:
N_tot=0.3E14 #0.3E14          #-      #Total number of electrons (werken:1E18 met v2 )
N_thr_succes=30            #-      #Treshold number (werken: 1E6 )
N_thr_repump=5            #-      #Treshold number (werken: 1E6 )
dt=50E-6            #s
dt_gauss=400E-6     #s


def get_f_res(j=1):  
    return rd.gauss(f_red,j*sigma)


def test_function(function, output_name, runs=20):
    print(output_name)
    print()
    for i in range(runs):
       print(function())
    print()

#test_function(get_f_res,"f_red")

##print("f_red")
##print()
##for i in range(20):
##    print(get_f_res())
##print()

def lorentzian(x,x_0,gamma=1):
    return 1./(1.+((x-x_0)/gamma)**2)

def get_counts(f_resonance): #nog om aan te passen
    #return N_tot*lorentzian(f_red,f_resonance)5
    return dt*N_tot*sp.cauchy.pdf(f_red,f_resonance,LL_linewidth)


#test_function(get_counts,"Counts per cr",get_f_res)
# print("Counts per cr")
# for i in range(20):
#    f=get_f_res()
#    print(get_counts(f))        



def measure_counts(N_fotons): #laserduur
    return sp.poisson.rvs(N_fotons)#int(round(N_fotons)))

# print("Counts per cr")
# for i in range(50000):
#    f=get_f_res()
#    Nf=get_counts(f)
#    print(measure_counts(Nf))   



def CRcheck(N_thr_succes,N_thr_repump): #experiment=False #using while loop
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    N=0
    f_res=get_f_res()
    If=get_counts(f_res)
    N_green+=1
    while not N>N_thr_succes:
        if N<N_thr_repump or N_red%100==0:
            f_res=get_f_res()
            If=get_counts(f_res)
            N_green+=1
        N=measure_counts(If)
        N_red+=1
    Delta=f_res-f_red
    # if experiment:
    #     N2=measure_counts(If)
    #     return N,N2
    return Delta, N_green, N_red


##Plotting and showing results part
bin_loc=[]
for i in range(60):
    bin_loc.append(i)


def get_results(function, a, b, amount=5000):
    print() 
    print("N_thr_green = "+str(a))
    print("N_thr_repump = "+str(b))
    print()
    d=np.zeros(amount)
    N_green=np.zeros(amount)
    N_red=np.zeros(amount)
    for i in range(amount):
        Delta, N_g, N_r=function(a,b)
        d[i]=Delta
        N_green[i]=N_g
        N_red[i]=N_r #*(dt_gauss+dt)+dt)
    mu=np.mean(d)
    var=np.var(d)
    sd=np.sqrt(var)
    print("Delta = "+str(mu)+" +/- "+str(sd))
    mu_g=np.mean(N_green)
    mu_r=np.mean(N_red)
    meantime=dt_gauss*mu_g+dt*mu_r
    print("Mean (time untill success) in seconds = "+str(meantime))
    # p=1./meantime
    # print("Chance on succes p = "+str(p) )
    return sd, meantime#, p

def plots_of_variation(function,g_num,r_num):
    N_thr_succes_arr=np.arange(N_thr_succes,N_thr_succes+g_num,1)
    N_thr_repump_arr=np.arange(N_thr_repump,N_thr_repump+r_num,1)
    SD_arr= np.zeros((g_num,r_num))
    MuT_arr=np.zeros((g_num,r_num))
    #P_arr=np.zeros((g_num,r_num))
    for i in range(g_num):
        for j in range(r_num):
            SD_arr[i,j], MuT_arr[i,j]=get_results(function,N_thr_succes_arr[i],N_thr_repump_arr[j] ) #, P_arr[i,j]
    X,Y=np.meshgrid(N_thr_succes_arr,N_thr_repump_arr)
    print (X.shape)
    
    print (SD_arr.shape)
    print (Y.shape)
    fig = plt.figure()
    ax = fig.add_subplot(1, 2, 1, projection='3d')      
    plt.title("Deviation of distance upon succes")
    ax.set_xlabel("N_thr_succes (#)")
    ax.set_ylabel("N_thr_repump (#)")
    ax.set_zlabel("Frequency (Hz)")
    ax.plot_surface(X, Y, SD_arr.T, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    
    ax = fig.add_subplot(1, 2, 2, projection='3d')
    plt.title("Mean (time untill success)")
    ax.set_xlabel("N_thr_succes (#)")
    ax.set_ylabel("N_thr_repump (#)")
    ax.set_zlabel("Time (s)")
    #plt.plot(Thr_arr,MuT_arr)
    ax.plot_surface(X, Y, MuT_arr.T, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    
    # ax = fig.add_subplot(1, 3, 3, projection='3d')
    # plt.title("Mean (time untill success)")
    # ax.set_xlabel("N_thr_succes (#)")
    # ax.set_ylabel("N_thr_repump (#)")
    # ax.set_zlabel("Probability (/)")
    # ax.plot_surface(X, Y, P_arr.T, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    plt.show()


plots_of_variation(CRcheck,7,10)

