import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sp
from scipy.stats import poisson
import random as rd
from numba import njit, jit, prange
from time import time
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import math
from scipy.optimize import curve_fit

# import cProfile
# import re
# cProfile.run('re.compile("foo|bar")')
core=8

#Parameters
c=299792458         #m/s    #speed of light
lambda_red=637E-9   #m      #golflengte laser
f_red=c/lambda_red  #1/s    #frequentie laser
sigma=50E6          #1/s    #variantie laser
LL_linewidth=13E6   #1/s    #Lifetime limited linewidth lorentzian shape

#Choose:
N_tot=0.3E14        #-      #Total number of electrons (werken:1E18 met v2 )
N_thr_succes=30     #-      #Treshold number (werken: 1E6 )
N_thr_repump=5      #-      #Treshold number (werken: 1E6 )
dt=50E-6            #s
dt_gauss=400E-6     #s

def generalized_normal(x, mu=0, beta=2, alpha=1):
    return beta/2/alpha/math.gamma(1/beta)*np.exp(-((abs(x-mu)/alpha)**beta))


@jit(nopython=True)
def get_f_res(sigma=sigma,f_red=f_red):
    #return np.random.normal(f_red,sigma)
    return rd.gauss(f_red,sigma)

##print("f_red")
##print(str(f_red))
##print()
##for i in range(20):
##    print(get_f_res())
##print()

@jit(nopython=True)
def lorentzian(x,x_0=0.,gamma=1.):
    return 1./(1.+((x-x_0)*(x-x_0)/gamma/gamma))/np.pi/gamma

@jit(nopython=True)
def get_counts(f_resonance,dt=dt): #nog om aan te passen
    #f=sp.cauchy.pdf(f_red,f_resonance,LL_linewidth)
    #return dt*N_tot*sp.cauchy.pdf(f_red,f_resonance,LL_linewidth)
    return dt*N_tot*lorentzian(f_red,f_resonance,LL_linewidth)



# print("Counts per cr")

# for i in range(60):
# #    print("Lorentz")
# #    print(lorentzian(1,2,1+i))#,0,LL_linewidth))
# #    print(sp.cauchy.pdf(1,2,1+i))#,0,LL_linewidth))

# #    print("diff")
# #    print(lorentzian(1,2,1+i)/(sp.cauchy.pdf(1,2,1+i)))
#     f=get_f_res()
#     print(get_counts(f))        


@jit(nopython=True)
def measure_counts(N_fotons): #laserduur
    return np.random.poisson(N_fotons)
    #return poisson.rvs(N_fotons,size=num)#int(round)

def set_Nthr(percentage, dt=dt,num=100000): #percentage of states that succeeds (counted from the highest)
    maxI=get_counts(f_red,dt)
    counts=np.zeros(num)
    for i in range(num):
        counts[i]=measure_counts(maxI)
    #print(counts)
    counts.sort()
    Nt=int(counts[-int(round(percentage*num))])
    print("maximal intensity"+str(maxI))
    print("N_threshold_succes"+str(Nt))
    return Nt


# print("threshold suggestions")
# for i in range(60):
#     print(set_Nthr(0.85))

# print("Counts per cr")
# for i in range(20):
#    f=get_f_res()
#    If=get_counts(f)
#    print(measure_counts(If))   

@jit(nopython=True)
def CRcheck(N_thr_succes=N_thr_succes,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res()): #experiment=False #using while loop
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    #N=0
    If=get_counts(f_res,dt)
    N=measure_counts(If)
    N_red+=1
    while not N>N_thr_succes:
        if N<N_thr_repump or N_red%20==0:#N_red%100==0:
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_green+=1
        N=measure_counts(If)
        N_red+=1
    Delta=f_res-f_red
    return Delta, N_green, N_red, N, f_res


# print("Counts#1 | Counts#2 | Difference")
# for i in range(20):
#     N1,N2=CRcheck(True)
#     print(str(N1)+"    |    "+str(N2)+"    |    "+str(N1-N2))
# print()
#print(CRcheck())


def CRcheck2(iteration=0): #using recursion
    f_res=get_f_res()
    If=get_counts(f_res)
    N=measure_counts(If)
    #print(N)
    #g = input("Enter your name : ") 
    if N>N_thr_succes:
        Delta=f_res-f_red
        #print("Pass, attempt "+str(iteration))
        return Delta,iteration
    else:
        #print("Fail, attempt "+str(iteration))
        return CRcheck2(iteration+1)


bin_loc=np.arange(60,step=1,dtype=int)
amount=20000 
@jit(nopython=True, parallel=True)
def get_results2(function, amount=5000): 
    d=np.zeros(amount)
    N_green=np.zeros(amount)
    N_red=np.zeros(amount)
    #TIME1=time()
    for i in prange(amount):
        Delta, N_g, N_r, N_a,f=function()#1/(j+1))
        d[i]=Delta
        N_green[i]=N_g
        N_red[i]=N_r#*(dt_gauss+dt)+dt)
    #TIME2=time()
    #print("Duration of the loop")
    #print(TIME2-TIME1)
    ##    print()
    ##    print("Resultaten")
    ##    print("Delta = "+str(Delta))
    ##    print("iteration = "+str(iteration))
    ##    print()
    return d, N_green, N_red

def plot_results2(function, j, amount=5000):
    d, N_green, N_red=get_results2(function,amount)
    #plt.subplot(1,3,1)
    plt.subplot(2,4,2*j+1)
    plt.title("Distance on success Ns= "+str(N_thr_succes)+" and Nr= "+str(N_thr_repump)) #surpessed to have a better overview
    plt.ylabel("Occurence (normalized)")
    plt.xlabel("Freq (HZ)")
    number_of_bins=40
    plt.hist(d, bins=number_of_bins ,density=True)
    mu=np.mean(d)
    var=np.var(d)
    x_arr=np.linspace(np.min(d),np.max(d),1000)
    y_arr=1./np.sqrt(2*np.pi*var)*np.exp(-0.5*(x_arr-mu)**2/var)
    plt.plot(x_arr,y_arr,label="Normal Fit")
    
    # #Code for adding a generalized normal fit
    # #sorted_d=d.sort()
    # x_fit=np.linspace(np.min(d),np.max(d),number_of_bins)
    # y_fit=np.zeros(number_of_bins)
    # for k in range(number_of_bins):
    #     for l in range(amount):
    #         if d[l]>x_fit[k] and d[l]<x_fit[k+1]:
    #             y_fit[k]+=1
    # y_fit=y_fit/np.linalg.norm(y_fit)/(x_fit[1]-x_fit[0])
    # print(y_fit)
    # popt,pcov=curve_fit(generalized_normal,x_fit,y_fit)
    # mu_fit=popt[0]
    # print("mu_fit="+str(mu_fit))
    # beta_fit=popt[1]
    # print("mu_fit="+str(mu_fit))
    # alpha_fit=popt[2]
    # plt.plot(x_arr,generalized_normal(x_arr, mu_fit, beta_fit, alpha_fit), label="Generalized normal fit")
    #plt.plot(label="with mu="+str(mu))
    guess=np.zeros(3)
    guess[0]=mu
    guess[1]=10
    guess[2]=1.28*np.sqrt(2*var)
    popt,pcov=curve_fit(generalized_normal,(bins[:-1]+bins[1:])/2,count,p0=guess)#,bounds=(lbound,rbound))
    #popt,pcov=curve_fit(generalized_normal,(bins[:-1]+bins[1:])/2,count)
    
    print(popt)
    
    mu_fit=popt[0]
    print("mu_fit="+str(mu_fit))
    beta_fit=popt[1]
    print("beta_fit="+str(beta_fit))
    alpha_fit=popt[2]
    print("alpha_fit="+str(alpha_fit))   
    plt.plot(x_arr,generalized_normal(x_arr, mu_fit, beta_fit, alpha_fit), label="Generalized normal fit")
    plt.legend()#loc="best")
    #print("Number of zeros is "+str(zero))
    print("Delta = "+str(mu)+" +/- "+str(np.sqrt(var)))
    mu_green=np.mean(N_green)
    mu_red=np.mean(N_red)
    #var_T=np.var(itera)
    print("Mean (time untill success) in seconds = "+str(dt_gauss*mu_green+dt*mu_red))
    #print(sum(d)/len(d))
    #plt.plot(d,"+")
    #plt.subplot(1,3,3)
    plt.subplot(2,4,2*j+2)
    #plt.title("Iterations untill success for "+str(len(d))+" iterations")
    #plt.ylabel("Occurence (normalized)") #surpessed to have a better overview
    #plt.xlabel("Time (s)")
    plt.xlabel("Iteration")
    x_lst1=np.linspace(np.min(N_green),np.max(N_green),20)
    #print(x_lst1)
    p=1./mu_green
    print("Chance on succes p = "+str(p) )
    print()
    y_lst1=p*((1-p)**(x_lst1-1))
    #print(y_lst1)
    # plt.hist(itera,bins=20,density=True)#bin_loc)
    plt.hist(N_green,bins=bin_loc,density=True)#bin_loc)
    plt.plot(x_lst1,y_lst1, linewidth=2, label="Geometric fit")
    #y_lst2=p*((1-p)**bin_loc)
    #plt.plot(bin_loc,y_lst2, label="Geometric fit 2", linestyle="--")
    plt.legend()
    #plt.show()

# for j in range(4):
#     plot_results2(CRcheck,j, 1000000)#  5000) #
# plt.show()

@jit(nopython=True,parallel=True)
def get_results(function, a, b, c, psi=0.2, dt=dt, amount=20000):
    #print() 
    #print("N_thr_green = "+str(a))
    #print("N_thr_repump = "+str(b)) 
    #print("running")
    d=np.zeros(amount*core)
    N_green=np.zeros(amount*core)
    N_red=np.zeros(amount*core)
    N_on_succes=np.zeros(amount*core)
    for j in prange(core):
        f_res=get_f_res()
        for i in range(amount):
            d[i+j*amount], N_green[i+j*amount], N_red[i+j*amount], N_on_succes[i+j*amount],f_res_old=function(a,b,c,dt,f_res)
            f_res=get_f_res(c*psi,f_res_old)#/10
            # N, Delta, N_g, N_r=function(a,b,c)
            # d[i]=Delta
            # N_green[i]=N_g
            # N_red[i]=N_r #*(dt_gauss+dt)+dt)
    N_avg=np.mean(N_on_succes)
    # count, bins, ignored = plt.hist(d, bins=number_of_bins ,density=True)
    # guess=np.zeros(3)
    # guess[0]=mu
    # guess[1]=10
    # guess[2]=1.28*np.sqrt(2*var)
    # popt,pcov=curve_fit(generalized_normal,(bins[:-1]+bins[1:])/2,count,p0=guess)
    # mu_fit=popt[0]
    # #print("mu_fit="+str(mu_fit))
    # beta_fit=popt[1]
    # #print("beta_fit="+str(beta_fit))
    # alpha_fit=popt[2]
    # #print("alpha_fit="+str(alpha_fit))   
    mu=np.mean(d)
    var=np.var(d)
    sd=np.sqrt(var)
    #print("Delta = "+str(mu)+" +/- "+str(sd))
    mu_g=np.mean(N_green)
    mu_r=np.mean(N_red)
    meantime=dt_gauss*mu_g+dt*mu_r
    #print("Mean (time untill success) in seconds = "+str(meantime))
    p=(dt_gauss+dt)/meantime
    # print("Chance on succes p = "+str(p) )
    #return alpha_fit, meantime, p, N_avg
    return sd, meantime, p, N_avg

#@jit(nopython=True)
def find_opt(constraint,tSD_arr, tMuT_arr, tN_arr, X, Y,sigma,dt=dt):
        #finding optimal values
    #Constraint 13 MHZ
    c1=tSD_arr<constraint*1E6
    #finding min average time
    c2=np.min(tMuT_arr[c1])
    #print(np.where(tMuT_arr==c2)[1][0])
    place=np.where(tMuT_arr==c2)
    loc=(place[0][0],place[1][0])
    opt_N_thr_succes=X[loc]
    opt_N_thr_repump=Y[loc]
    opt_SD=tSD_arr[loc]
    opt_MuT=tMuT_arr[loc]
    opt_N=tN_arr[loc]
    print()
    print("At sigma = "+str(sigma))
    print("At dt = "+str(dt))
    print("Optimal Location at N_thr_succes = "+str(opt_N_thr_succes)+" and N_thr_repump = "+str(opt_N_thr_repump))
    print("Spectral diffusion = "+str(opt_SD))
    print("Mean time untill succes = "+str(opt_MuT))
    print("Mean number of photons= "+str(opt_N))
    return opt_N_thr_succes, opt_N_thr_repump, opt_SD, opt_MuT, opt_N


#@jit(nopython=True)
def optimal_results(function,g_num,r_num, sigma=sigma, start_N_thr_succes=0 , start_N_thr_repump=0, psi=0.2, dt=dt):
    N_thr_succes=start_N_thr_succes
    N_thr_repump=start_N_thr_repump
    N_thr_succes_arr=np.arange(N_thr_succes,N_thr_succes+g_num,1)
    N_thr_repump_arr=np.arange(N_thr_repump,N_thr_repump+r_num,1)
    SD_arr= np.zeros((g_num,r_num))
    MuT_arr=np.zeros((g_num,r_num))
    P_arr=np.zeros((g_num,r_num))
    N_arr=np.zeros((g_num,r_num))
    Plane_arr=np.zeros((g_num,r_num))
    X,Y=np.meshgrid(N_thr_succes_arr,N_thr_repump_arr)
    for i in range(g_num):
        for j in range(r_num):
            if N_thr_repump_arr[j]<N_thr_succes_arr[i]:
                SD_arr[i,j], MuT_arr[i,j] , P_arr[i,j], N_arr[i,j]=get_results(function,N_thr_succes_arr[i],N_thr_repump_arr[j],sigma,psi ) #, P_arr[i,j]
                Plane_arr[i,j]=13E6
            else:
                SD_arr[i,j], MuT_arr[i,j] , P_arr[i,j], N_arr[i,j]=np.nan ,np.nan, np.nan, np.nan
                Plane_arr[i,j]=np.nan


    
    # #mask=Y<X
    # #X[mask]=np.nan
    # #Y[mask]=np.nan
    # # print(mask)
    # # print("xs shape")
    # # print(Xs.shape)
    # # print(Xs)
    # #print (X.shape)
    # # thr_s_max=max(N_thr_succes_arr)
    # # thr_s_min=min(N_thr_succes_arr)
    # # thr_r_max=max(N_thr_repump_arr)
    # # thr_r_min=min(N_thr_repump_arr)
    # # thrmin=max(thr_s_min,thr_r_min)
    # # thrmax=min(thr_s_max,thr_r_max)
    # # x_line = np.arange(thrmin,thrmax,step=1)
    # # y_line = np.arange(thrmin,thrmax,step=1)
    # # z_0=np.zeros(len(x_line))
    # # z_1=np.ones(len(x_line))
    # # z_try=np.arange(min(SD_arr),max(SD_arr),step=1)
    # #print (SD_arr.shape)
    # #print (Y.shape)
    # fig = plt.figure()
    # ax = fig.add_subplot(1, 3, 1, projection='3d')      
    # plt.title("Deviation of distance upon succes")
    # ax.set_xlabel("N_thr_succes (#)")
    # ax.set_ylabel("N_thr_repump (#)")
    # ax.set_zlabel("Frequency (MHz)")
    # tPlane_arr=Plane_arr.T
    tSD_arr=SD_arr.T
    # ax.plot_surface(X, Y, tPlane_arr/1E6 ,color="orange")#label="13 MHZ", )
    # ax.plot_surface(X, Y, tSD_arr/1E6, cmap=cm.coolwarm, linewidth=0, antialiased=False)

    # #, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    # #X_try,Z_try=np.meshgrid(x_line,z_try)
    # #ax.plot_surface(X_try,X_try, Z_try/1E6)
    # #ax.plot(x_line, y_line, z_0, label='N_thr_succes<Nthr repump')#line for separation
    # #ax.plot(x_line, y_line, z_1, label='N_thr_succes<Nthr repump')#line for separation
    # ax = fig.add_subplot(1, 3, 2, projection='3d')
    # plt.title("Mean (time untill success)")
    # ax.set_xlabel("N_thr_succes (#)")
    # ax.set_ylabel("N_thr_repump (#)")
    # ax.set_zlabel("Time (ms)")
    # #plt.plot(Thr_arr,MuT_arr)
    tMuT_arr=MuT_arr.T
    tN_arr=N_arr.T
    # ax.plot_surface(X, Y, tMuT_arr*1E3, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    
    # ax = fig.add_subplot(1, 3, 3, projection='3d')
    # plt.title("Probability to succeed") #Calculated with the geometric formula and normalization
    # ax.set_xlabel("N_thr_succes (#)")
    # ax.set_ylabel("N_thr_repump (#)")
    # ax.set_zlabel("Probability (/)")
    # tP_arr=P_arr.T
    # ax.plot_surface(X, Y, tP_arr, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    # plt.show()

    opt_N_thr_succes, opt_N_thr_repump, opt_SD, opt_MuT, opt_N=find_opt(13,tSD_arr, tMuT_arr, tN_arr, X, Y,sigma,dt)

    # #finding optimal values
    # #Constraint 13 MHZ
    # c1=tSD_arr<13E6
    # #finding min average time
    # c2=min(tMuT_arr[c1])
    # #print(np.where(tMuT_arr==c2)[1][0])
    # place=np.where(tMuT_arr==c2)
    # loc=(place[0][0],place[1][0])
    # opt_N_thr_succes=X[loc]
    # opt_N_thr_repump=Y[loc]
    # opt_SD=tSD_arr[loc]
    # opt_MuT=tMuT_arr[loc]
    # print()
    # print("At sigma = "+str(sigma))
    # print("Optimal Location at N_thr_succes = "+str(opt_N_thr_succes)+" and N_thr_repump = "+str(opt_N_thr_repump))
    # print("Spectral diffusion = "+str(opt_SD))
    # print("Mean time untill succes = "+str(opt_MuT))

    return opt_N_thr_succes, opt_N_thr_repump, opt_SD,  opt_MuT, opt_N
    


def variation_of_sigma_results(length=1000,start=0,step=10):
    #step=10
    sigma_arr=1E6*np.arange(start,length+step,step=step)
    size=len(sigma_arr)
    opt_N_thr_succes_arr=np.zeros(size)
    opt_N_thr_repump_arr=np.zeros(size)
    opt_SD_arr=np.zeros(size)
    opt_MuT_arr=np.zeros(size)
    opt_N_arr=np.zeros(size)
    for i in range(size):
        opt_N_thr_succes_arr[i], opt_N_thr_repump_arr[i], opt_SD_arr[i],  opt_MuT_arr[i], opt_N_arr[i]=optimal_results(CRcheck,50,50,sigma_arr[i],0,0)
    
    plt.subplot(1,3,1)
    plt.title("Optimal N_thresholds for varying sigma")
    plt.plot(sigma_arr/1E6,opt_N_thr_succes_arr,label="N_thr_succes") 
    plt.plot(sigma_arr/1E6, opt_N_thr_repump_arr,label="N_thr_repump") 
    plt.legend()
    plt.xlabel("Sigma (MHz)")
    plt.ylabel("N_thr")

    plt.subplot(1,3,2)
    plt.title("Optimal values of rate and fequency with varying sigma")
    plt.xlabel("Sigma (MHz)")
    plt.ylabel("Rate (ms)",color='b')
    plt.plot(sigma_arr/1E6,opt_MuT_arr*1E3,color='b',label="Mean (time till succes)")
    plt.tick_params(axis="y", labelcolor="b", pad=8)
    plt.legend()
    plt.twinx()
    plt.plot(sigma_arr/1E6,opt_SD_arr/1E6,color='r',label="F_resonance")
    plt.tick_params(axis="y", labelcolor="r", pad=8)
    plt.ylabel("Frequency (MHZ)", color='r')  
    plt.legend()
    


    plt.subplot(1,3,3)
    plt.title("Optimal photon counts")
    plt.plot(sigma_arr/1E6,opt_N_arr,label="Average lambda")  
    plt.legend()
    plt.xlabel("Sigma (MHz)")
    plt.ylabel("Photons (#)")

    plt.show()

    

#variation_of_sigma_results(length=1000, start=0,step=50)

def variation_of_dt_results(percentage=0.85,length=1,start=0.5,step=0.1):
    #step=10
    dt_arr=dt*np.arange(start,start+length+step,step=step)
    size=len(dt_arr)
    opt_N_thr_succes_arr=np.zeros(size)
    opt_N_thr_repump_arr=np.zeros(size)
    opt_SD_arr=np.zeros(size)
    opt_MuT_arr=np.zeros(size)
    opt_N_arr=np.zeros(size)
    for i in range(size):
        #Ns=set_Nthr(percentage, dt_arr[i],num=100000)
        #Nr=int(round(Ns/3))
        #print(Ns)
        opt_N_thr_succes_arr[i], opt_N_thr_repump_arr[i], opt_SD_arr[i],  opt_MuT_arr[i], opt_N_arr[i]=optimal_results(CRcheck,32,32,sigma,0,0,dt_arr[i])
    
    #plt.figure()
    #plt.tight_layout()
    plt.subplot(1,3,1)
    plt.title("Optimal N_thresholds for varying dt (Ntot)")
    plt.plot(dt_arr*1E3,opt_N_thr_succes_arr,label="N_thr_succes") 
    plt.plot(dt_arr*1E3, opt_N_thr_repump_arr,label="N_thr_repump") 
    plt.legend()
    plt.xlabel("dt (ms)")
    plt.ylabel("N_thr")

    plt.subplot(1,3,2)
    plt.title("Optimal values of rate and fequency with varying dt")
    plt.xlabel("dt (ms)")
    plt.ylabel("Rate (ms)",color='b')
    plt.plot(dt_arr*1E3,opt_MuT_arr*1E3,color='b',label="Mean (time till succes)")
    plt.tick_params(axis="y", labelcolor="b", pad=8)
    plt.legend()
    plt.twinx()
    plt.plot(dt_arr*1E3,opt_SD_arr/1E6,color='r',label="F_resonance")
    plt.tick_params(axis="y", labelcolor="r", pad=8)
    plt.ylabel("Frequency (MHZ)", color='r')  
    plt.legend()
    


    plt.subplot(1,3,3)
    plt.title("Optimal photon counts")
    plt.plot(dt_arr*1E3,opt_N_arr,label="Average lambda")  
    plt.legend()
    plt.xlabel("dt (ms)")
    plt.ylabel("Photons (#)")
    #plt.tight_layout()
    plt.show()

#variation_of_dt_results(percentage=0.85,length=5,start=1,step=0.2)

#@jit(nopython=True)
def variation_psi(psi_length=1,psi_start=0,psi_step=0.1,sigma_length=100,sigma_start=0,sigma_step=10):
    sigma_arr=1E6*np.arange(sigma_start,sigma_start+sigma_length+sigma_step,step=sigma_step)
    psi_arr=np.arange(psi_start,psi_start+psi_length+psi_step,step=psi_step)
    sigma_size=len(sigma_arr)
    psi_size=len(psi_arr)
    opt_N_thr_succes_arr=np.zeros((sigma_size,psi_size))
    opt_N_thr_repump_arr=np.zeros((sigma_size,psi_size))
    opt_SD_arr=np.zeros((sigma_size,psi_size))
    opt_MuT_arr=np.zeros((sigma_size,psi_size))
    opt_N_arr=np.zeros((sigma_size,psi_size))      
    X,Y=np.meshgrid(sigma_arr,psi_arr)
    for i in range(sigma_size):
        for j in range(psi_size):
            #Np=int(min(np.ceil(get_counts(f_red,dt),50))
            opt_N_thr_succes_arr[i,j], opt_N_thr_repump_arr[i,j], opt_SD_arr[i,j],  opt_MuT_arr[i,j], opt_N_arr[i,j]=optimal_results(CRcheck,30,30,sigma_arr[i],0,0,psi_arr[j])#np.max(Np-40,0)

    # #print (SD_arr.shape)
    # #print (Y.shape)
    fig = plt.figure()
    ax = fig.add_subplot(1, 3, 1, projection='3d')
    plt.title("Optimal N_thresholds for varying sigma")
    #plt.legend()
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("psi (-)")
    ax.set_zlabel("N_thr (-)")

    ax.plot_surface(X/1E6, Y, opt_N_thr_succes_arr.T, cmap=cm.coolwarm, linewidth=0.1, antialiased=False,label="N_thr_succes")#label="13 MHZ", )
    ax.plot_surface(X/1E6, Y, opt_N_thr_repump_arr.T, cmap=cm.bwr, linewidth=0.1, antialiased=False, label="N_thr_repump")
 #Colorings plt.cm.Spectral or plt.cm.CMRmap

    ax = fig.add_subplot(1, 3, 2, projection='3d')
    plt.title("Optimal values of rate and fequency with varying sigma")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("psi (-)")
    ax.set_zlabel("Rate (ms)")#, color='b')
    ax.plot_surface(X/1E6, Y, opt_MuT_arr.T*1E3, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Mean (time till succes)")
    #ax.tick_params(axis="z", labelcolor="b", pad=8)
    #plt.legend()
    # plt.twinx()
    # ax.plot_surface(X/1E6, Y*1E3, opt_SD_arr.T/1E63, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="label="F_resonance"")
    # ax.tick_params(axis="z", labelcolor="r", pad=8)
    # ax.set_zlabel("Frequency (MHZ)", color='r')  
    # plt.legend()


    ax = fig.add_subplot(1, 3, 3, projection='3d')
    plt.title("Optimal photon counts") #Calculated with the geometric formula and normalization
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("psi (-)")
    ax.set_zlabel("Photons (-)")
    ax.plot_surface(X/1E6, Y, opt_N_arr.T, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Average lambda")
    #plt.legend()
    plt.show()



    fig=plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    plt.title("Optimal values of mean time till succes with varying sigma and psi")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("psi (-)")
    ax.set_zlabel("Time (ms)")#, color='b')
    ax.plot_surface(X/1E6, Y, opt_MuT_arr.T*1E3, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Mean (time till succes)")
    plt.show()

    fig=plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    plt.title("Optimal Photon counts with varying sigma and psi") #Calculated with the geometric formula and normalization
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("psi (-)")
    ax.set_zlabel("Photons (-)")
    ax.plot_surface(X/1E6, Y, opt_N_arr.T, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Average N on succes")
    #plt.legend()
    plt.show()

    fig=plt.figure()
    ax = fig.add_subplot(1, 2, 1, projection='3d')
    plt.title("Optimal values of mean time till succes")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("psi (-)")
    ax.set_zlabel("Time (ms)")#, color='b')
    ax.plot_surface(X/1E6, Y, opt_MuT_arr.T*1E3, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Mean (time till succes)")

    ax = fig.add_subplot(1, 2, 2, projection='3d')
    plt.title("Optimal Photon counts with varying sigma and psi") #Calculated with the geometric formula and normalization
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("psi (-)")
    ax.set_zlabel("Photons (-)")
    ax.plot_surface(X/1E6, Y, opt_N_arr.T, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Average N on succes")
    #plt.legend()
    plt.show()

    


    
variation_psi(psi_length=1,psi_start=0,psi_step=0.1,sigma_length=100,sigma_start=0,sigma_step=10)



