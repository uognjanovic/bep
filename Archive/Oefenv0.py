import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sp
import random as rd

def lorentzian(x,x_0,gamma):
    return 1./(1.+((x-x_0)/gamma)**2)


def rng(num):
    lst=[]
    for i in range( num):
        lst.append(rd.gauss(2,1))
    return lst

def plotten(num):
    plt.figure()
    plt.hist(rng(num), bins=100)
    plt.show()

#plotten(100000)
#plotten(1000)
x_list=1
lorentzian(x_list,0,1)
def plotlor(gamma):
    
    x_list=np.arange(-10.05,10.05,0.1)
    plt.plot(x_list,lorentzian(x_list,0,gamma),label="gamma = "+str(gamma))
    #plt.show()

#plotlor(1)
def figlor(num):    
    plt.figure()
    for i in range (1,num):
        
        #plt.subplot(3,3,i)
        plotlor(2**(i-5))
    plt.legend()
    plt.show()


def plotpois(mu):
    num=100
    i_lst=[]
    pois_lst=[]
    for i in range (num):
        i_lst.append(i)
        pois_lst.append(sp.poisson.pmf(i,mu,0))
    
    plt.plot(i_lst,pois_lst,label="mu = "+str(mu))

def figpois(num):
    plt.figure()
    for i in range (1,num):
        
        #plt.subplot(3,3,i)
        plotpois(2**(0.05*(i)))

    #plt.legend()
    plt.show()                      

figpois(1000)
