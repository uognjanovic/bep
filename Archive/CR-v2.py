import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sp
import random as rd
#from time import time

#Parameters
c=299792458         #m/s    #speed of light
lambda_red=637E-9   #m      #golflengte laser
f_red=c/lambda_red  #1/s    #frequentie laser
sigma=50E6          #1/s    #variantie laser
LL_linewidth=13E6   #1/s    #Lifetime limited linewidth lorentzian shape

#Choose:
N_tot=0.3E14          #-      #Total number of electrons (werken:1E18 met v2 )
N_thr=30            #-      #Treshold number (werken: 1E6 )
dt=50E-6            #s
dt_gauss=400E-6     #s

def get_f_res():  
    return rd.gauss(f_red,sigma)

##print("f_red")
##print(str(f_red))
##print()
##for i in range(20):
##    print(get_f_res())
##print()
def lorentzian(x,x_0,gamma=1):
    return 1./(1.+((x-x_0)/gamma)**2)

def get_counts(f_resonance): #nog om aan te passen
    #return N_tot*lorentzian(f_red,f_resonance)5
    return dt*N_tot*sp.cauchy.pdf(f_red,f_resonance,LL_linewidth)

print("Counts per cr")
for i in range(20):
   f=get_f_res()
   print(get_counts(f))        



def measure_counts(N_fotons): #laserduur
    return sp.poisson.rvs(N_fotons)#int(round(N_fotons)))

##print("Counts per cr")
##for i in range(20):
##    f=get_f_res()
##    Nf=get_counts(f)
##    print(measure_counts(Nf))   


def CRcheck(experiment=False): #using while loop
    iteration=-1
    N=0
    while not N>N_thr:
        f_res=get_f_res()
        Nf=get_counts(f_res)
        N=measure_counts(Nf)
        iteration+=1
    Delta=f_res-f_red
    if experiment:
        N2=measure_counts(Nf)
        return N,N2
    return Delta,iteration

print("Counts#1 | Counts#2 | Difference")
for i in range(20):
    N1,N2=CRcheck(True)
    print(str(N1)+"    |    "+str(N2)+"    |    "+str(N1-N2))
print()

def CRcheck2(iteration=0): #using recursion
    f_res=get_f_res()
    Nf=get_counts(f_res)
    N=measure_counts(Nf)
    #print(N)
    #g = input("Enter your name : ") 
    if N>N_thr:
        Delta=f_res-f_red
        #print("Pass, attempt "+str(iteration))
        return Delta,iteration
    else:
        #print("Fail, attempt "+str(iteration))
        return CRcheck2(iteration+1)


##Plotting and showing results part
bin_loc=[]
for i in range(50):
    bin_loc.append(i)

amount=5000    
d=np.zeros(amount)
itera=np.zeros(amount)
zero=0
#TIME1=time()
for i in range(amount):
    Delta,iteration=CRcheck()
    if iteration==0:
        zero+=1
    d[i]=Delta
    itera[i]=iteration #*(dt_gauss+dt)+dt)
#TIME2=time()
#print("Duration of the loop")
#print(TIME2-TIME1)
##    print()
##    print("Resultaten")
##    print("Delta = "+str(Delta))
##    print("iteration = "+str(iteration))
##    print()

plt.subplot(1,3,1)
plt.title("Distance on success")
plt.ylabel("Occurence (normalized)")
plt.xlabel("Freq (1/s)")
plt.hist(d, bins=30 ,density=True)
mu=np.mean(d)
var=np.var(d)
x_arr=np.linspace(np.min(d),np.max(d),1000)
y_arr=1./np.sqrt(2*np.pi*var)*np.exp(-0.5*(x_arr-mu)**2/var)
plt.plot(x_arr,y_arr,label="Fit")
plt.legend()
#print("Number of zeros is "+str(zero))
print("Delta = "+str(mu)+" +/- "+str(np.sqrt(var)))
mu_T=np.mean(itera)
var_T=np.var(itera)
print("Mean (time untill success) in seconds = "+str((mu_T+1)*(dt+dt_gauss)))
#print(sum(d)/len(d))
#plt.plot(d,"+")
plt.subplot(1,3,3)
plt.title("Iterations untill success for "+str(len(d))+" iterations")
plt.ylabel("Occurence (normalizes)")
plt.xlabel("Time (s)")
x_lst1=np.linspace(np.min(itera),np.max(itera),20)
#print(x_lst1)
p=1./mu_T
print("Chance on succes p = "+str(p) )
y_lst1=p*((1-p)**x_lst1)
#print(y_lst1)
# plt.hist(itera,bins=20,density=True)#bin_loc)
plt.hist(itera,bins=bin_loc,density=True)#bin_loc)
plt.plot(x_lst1,y_lst1, linewidth=2, label="Geometric fit")
y_lst2=p*((1-p)**bin_loc)
plt.plot(bin_loc,y_lst2, label="Geometric fit 2", linestyle="--")
plt.legend()
plt.show()
   

d=np.zeros(amount)
itera=np.zeros(amount)
zero=0
#TIME1=time()
for i in range(amount):
    Delta,iteration=CRcheck2()
    # if iteration==0:
    #     zero+=1
    d[i]=Delta
    itera[i]=iteration #*(dt_gauss+dt)+dt)
##    print()
##    print("Resultaten")
##    print("Delta = "+str(Delta))
##    print("iteration = "+str(iteration))
##    print()

plt.subplot(1,3,1)
plt.title("Distance on success")
plt.ylabel("Occurence (normalized)")
plt.xlabel("Freq (1/s)")
plt.hist(d, bins=30 ,density=True)
mu=np.mean(d)
var=np.var(d)
x_lst=np.linspace(np.min(d),np.max(d),1000)
y_lst=1./np.sqrt(2*np.pi*var)*np.exp(-0.5*(x_lst-mu)**2/var)
plt.plot(x_lst,y_lst,label="Fit")
plt.legend()
#print("Number of zeros is "+str(zero))
print("Delta = "+str(mu)+" +/- "+str(np.sqrt(var)))
mu_T=np.mean(itera)
var_T=np.var(itera)
print("Mean (time untill success) in seconds = "+str((mu_T+1)*(dt+dt_gauss)))
#print(sum(d)/len(d))
#plt.plot(d,"+")
plt.subplot(1,3,3)
plt.title("Iterations untill success for "+str(len(d))+" iterations")
plt.ylabel("Occurence (normalizes)")
plt.xlabel("Time (s)")
x_lst1=np.linspace(np.min(itera),np.max(itera),20)
#print(x_lst1)
p=1./mu_T
print("Chance on succes p = "+str(p) )
y_lst1=p*((1-p)**x_lst1)
#print(y_lst1)
# plt.hist(itera,bins=20,density=True)#bin_loc)
plt.hist(itera,bins=bin_loc,density=True)#bin_loc)
plt.plot(x_lst1,y_lst1,linewidth=2 ,label="Geometric fit")
y_lst2=p*((1-p)**bin_loc)
plt.plot(bin_loc,y_lst2, label="Geometric fit 2", linestyle="--")
plt.legend()
plt.show()
   
d2=[]
itera2=[]
zero2=0
for i in range(500):
    Delta2,iteration2=CRcheck2()
    if iteration2==0:
        zero2+=1
    d2.append(Delta2)
    itera2.append(iteration2)
#    print()
#    print("Resultaten 2")
#    print("Delta = "+str(Delta2))
#    print("iteration = "+str(iteration2))
#    print()
print("Number of zeros is "+str(zero2))
print("Delta in MHZ")
print(sum(d2)/len(d2))
plt.subplot(1,2,1)
plt.title("Absolute value of distance on success")
plt.xlabel("i")
plt.ylabel("freq (1/s)")
plt.plot(d2,"+")
plt.subplot(1,3,3)
plt.title("Iterations untill success for "+str(len(d2))+" iterations")
plt.xlabel("Try")
plt.ylabel("Times occurred")
plt.hist(itera2,bins=bin_loc)
plt.show()
    

#plt.figure()
#plt.hist(itera2,bins=bin_loc)
#plt.show()
