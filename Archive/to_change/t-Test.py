import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sp
from scipy.stats import poisson
import random as rd
from numba import njit, jit, prange
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import math
from scipy.optimize import curve_fit


#Parameters
core=8              #-      #number of cores for parallel processing
c=299792458         #m/s    #speed of light
lambda_red=637E-9   #m      #golflengte laser
f_red=c/lambda_red  #1/s    #frequentie laser
sigma=50E6          #1/s    #variantie laser
LL_linewidth=13E6   #1/s    #Lifetime limited linewidth lorentzian shape

#Choose:
N_tot=0.3E14        #-      #Scaling factor
N_thr_succes=30     #-      #Treshold number 
N_thr_repump=5      #-      #Treshold number
dt=50E-6            #s      #Duration red laser
dt_gauss=400E-6     #s      #Duration green laser
psi=0.2             #-      #Memory parameter

def generalized_normal(x, mu=0, beta=2, alpha=1):
    return beta/2/alpha/math.gamma(1/beta)*np.exp(-((abs(x-mu)/alpha)**beta))

@jit(nopython=True)
def get_f_res(sigma=sigma,f_red=f_red):
    return np.random.normal(f_red,sigma)

@jit(nopython=True)
def lorentzian(x,x_0=0.,gamma=1.):
    return 1./(1.+((x-x_0)*(x-x_0)/gamma/gamma))/np.pi/gamma

@jit(nopython=True)
def get_counts(f_resonance,dt=dt): #nog om aan te passen
    return dt*N_tot*lorentzian(f_red,f_resonance,LL_linewidth)

print("max value lamda")
print(get_counts(f_red))
print()
print("minimal value lamda needed to pass")
minval=int(np.floor(get_counts(f_red-LL_linewidth)))
print(get_counts(f_red-LL_linewidth))
print()  


@jit(nopython=True)
def measure_counts(N_fotons): 
    return np.random.poisson(N_fotons)



def CRcheck_t_test(N_thr_succes=0.01,N_thr_repump=0.5,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,minval=18.3):
    N_thr_red=10
    N_green=0 
    N_red=0
    If=get_counts(f_res,dt)
    N=measure_counts(If)
    N_arr=np.zeros(N_thr_red)
    N_arr[0]=N
    stat,p=sp.ttest_1samp(N_arr[:1],minval)
    N_red+=1
    i=1
    while not p<N_thr_succes:
        if p>N_thr_repump or N_red%N_thr_red==0:
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_arr=np.zeros(N_thr_red)
            i=0
            N_green+=1
        N=measure_counts(If)
        N_arr[i]=N  
        i+=1
        N_red+=1
        stat,p=sp.ttest_1samp(N_arr[:(i)],minval)#
    Delta=f_res-f_red
    return Delta, N_green, N_red, np.mean(N_arr[:(i)]), f_res




def get_results(function, a, b, c, dt=dt, minval=18.3, amount=125):
    d=np.zeros(amount*core)
    N_green=np.zeros(amount*core)
    N_red=np.zeros(amount*core)
    N_on_succes=np.zeros(amount*core)
    for j in range(core):
        f_res=get_f_res()
        N_old=0
        for i in range(amount):
            d[i+j*amount], N_green[i+j*amount], N_red[i+j*amount], N_old,f_res_old=function(a,b,c,dt,f_res,N_old,minval)
            N_on_succes[i+j*amount]=N_old
            f_res=get_f_res(c/5,f_res_old)
    N_avg=np.mean(N_on_succes) 
    mu=np.mean(d)
    var=np.var(d)
    sd=np.sqrt(var)
    mu_g=np.mean(N_green)
    mu_r=np.mean(N_red)
    meantime=dt_gauss*mu_g+dt*mu_r
    p=(dt_gauss+dt)/meantime
    return sd, meantime, p, N_avg



def find_opt(constraint,tSD_arr, tMuT_arr, tN_arr, X, Y,sigma,dt=dt):
        #finding optimal values
    #Constraint 13 MHZ
    c1=tSD_arr<constraint*1E6
    #finding min average time
    c2=np.min(tMuT_arr[c1])
    place=np.where(tMuT_arr==c2)
    loc=(place[0][0],place[1][0])
    opt_N_thr_succes=X[loc]
    opt_N_thr_repump=Y[loc]
    opt_SD=tSD_arr[loc]
    opt_MuT=tMuT_arr[loc]
    opt_N=tN_arr[loc]
    print()
    print("At sigma = "+str(sigma))
    print("At dt = "+str(dt))
    print("Optimal Location at N_thr_succes = "+str(opt_N_thr_succes)+" and N_thr_repump = "+str(opt_N_thr_repump))
    print("Spectral diffusion = "+str(opt_SD))
    print("Mean time untill succes = "+str(opt_MuT))
    print("Mean number of photons= "+str(opt_N))
    return opt_N_thr_succes, opt_N_thr_repump, opt_SD, opt_MuT, opt_N




def optimal_results(function,g_num,r_num, sigma=sigma, max_N_thr_succes=0.2, min_N_thr_repump=0.5,dt=dt, min_N_thr_succes=0.01, max_N_thr_repump=1.0):
    minval=get_counts(f_red-LL_linewidth,dt)
    N_thr_succes_arr=np.linspace(min_N_thr_succes,max_N_thr_succes,num=g_num)
    N_thr_repump_arr=np.linspace(min_N_thr_repump,max_N_thr_repump,num=r_num)
    SD_arr= np.zeros((g_num,r_num))
    MuT_arr=np.zeros((g_num,r_num))
    P_arr=np.zeros((g_num,r_num))
    N_arr=np.zeros((g_num,r_num))
    X,Y=np.meshgrid(N_thr_succes_arr,N_thr_repump_arr)
    for i in range(g_num):
        for j in range(r_num):
            SD_arr[i,j], MuT_arr[i,j] , P_arr[i,j], N_arr[i,j]=get_results(function,N_thr_succes_arr[i],N_thr_repump_arr[j],sigma,dt, minval ) #, P_arr[i,j]


    tSD_arr=SD_arr.T
    tMuT_arr=MuT_arr.T
    tN_arr=N_arr.T

    opt_N_thr_succes, opt_N_thr_repump, opt_SD, opt_MuT, opt_N=find_opt(11,tSD_arr, tMuT_arr, tN_arr, X, Y,sigma,dt)

    return opt_N_thr_succes, opt_N_thr_repump, opt_SD,  opt_MuT, opt_N
    
#optimal_results(CRcheck_routine,10,4, sigma=2*sigma, max_N_thr_succes=0.84 , min_N_thr_repump=0.70 ,dt=dt, N_thr_red=20)

def sigma_variation(function=CRcheck_t_test,sigma_length=100,sigma_start=0,sigma_step=10):
    sigma_arr=1E6*np.arange(sigma_start,sigma_start+sigma_length+sigma_step,step=sigma_step)
    sigma_size=len(sigma_arr)
    opt_N_thr_succes_arr=np.zeros(sigma_size)
    opt_N_thr_repump_arr=np.zeros(sigma_size)
    opt_SD_arr=np.zeros(sigma_size)
    opt_MuT_arr=np.zeros(sigma_size)
    opt_N_arr=np.zeros(sigma_size)
    for i in range(sigma_size):
        print("Check")
        opt_N_thr_succes_arr[i], opt_N_thr_repump_arr[i], opt_SD_arr[i],  opt_MuT_arr[i], opt_N_arr[i]=optimal_results(function,12,8,sigma_arr[i],0.95,0.85,dt,min_N_thr_succes=0.45, max_N_thr_repump=1.0)
   
    plt.figure()
    plt.title("Optimal N_thresholds for varying sigma, dt = "+str(dt*1E3)+"ms")
    plt.plot(sigma_arr/1E6,opt_N_thr_succes_arr,label="N_thr_succes") 
    plt.plot(sigma_arr/1E6, opt_N_thr_repump_arr,label="N_thr_repump") 
    plt.legend()
    plt.xlabel("sigma (MHz)")
    plt.ylabel("N_thr (-)")
    plt.show()

    plt.figure()
    plt.title("Optimal time till succes and fequency with varying sigma, dt = "+str(dt*1E3)+"ms")
    plt.xlabel("sigma (MHz)")
    plt.ylabel("Time (ms)",color='b')
    plt.plot(sigma_arr/1E6,opt_MuT_arr*1E3,color='b',label="Mean (time till succes)")
    plt.tick_params(axis="y", labelcolor="b", pad=8)
    plt.legend(loc='upper left')
    plt.twinx()
    plt.plot(sigma_arr/1E6,opt_SD_arr/1E6,color='r',label="F_resonance")
    plt.tick_params(axis="y", labelcolor="r", pad=8)
    plt.ylabel("Frequency (MHz)", color='r')  
    plt.legend(loc='lower right')
    plt.show()


    plt.figure()
    plt.title("Optimal Photon counts with varying sigma, dt = "+str(dt*1E3)+"ms")
    plt.plot(sigma_arr/1E6,opt_N_arr,label="Average N")  
    plt.legend()
    plt.xlabel("sigma (MHz)")
    plt.ylabel("Photons (-)")
    plt.show()



    
sigma_variation(function=CRcheck_t_test,sigma_length=100,sigma_start=0,sigma_step=10)