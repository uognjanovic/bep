import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sp
from scipy.stats import poisson
import random as rd
from numba import njit, jit, prange
from time import time
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import math
from scipy.optimize import curve_fit

# import cProfile
# import re
# cProfile.run('re.compile("foo|bar")')

#Parameters
c=299792458         #m/s    #speed of light
lambda_red=637E-9   #m      #golflengte laser
f_red=c/lambda_red  #1/s    #frequentie laser
sigma=50E6          #1/s    #variantie laser
LL_linewidth=13E6   #1/s    #Lifetime limited linewidth lorentzian shape

#Choose:
N_tot=0.3E14 #0.3E14          #-      #Total number of electrons (werken:1E18 met v2 )
N_thr_succes=30            #-      #Treshold number (werken: 1E6 )
N_thr_repump=5            #-      #Treshold number (werken: 1E6 )
dt=50E-6            #s
dt_gauss=400E-6     #s

def generalized_normal(x, mu=0, beta=2, alpha=1):
    return beta/2/alpha/math.gamma(1/beta)*np.exp(-((abs(x)-mu)**2))


@jit(nopython=True)
def get_f_res(sigma=sigma):
    #return np.random.normal(f_red,sigma)
    return rd.gauss(f_red,sigma)

##print("f_red")
##print(str(f_red))
##print()
##for i in range(20):
##    print(get_f_res())
##print()

@jit(nopython=True)
def lorentzian(x,x_0=0.,gamma=1.):
    return 1./(1.+((x-x_0)*(x-x_0)/gamma/gamma))/np.pi/gamma

@jit(nopython=True)
def get_counts(f_resonance,dt=dt): #nog om aan te passen
    #f=sp.cauchy.pdf(f_red,f_resonance,LL_linewidth)
    #return dt*N_tot*sp.cauchy.pdf(f_red,f_resonance,LL_linewidth)
    return dt*N_tot*lorentzian(f_red,f_resonance,LL_linewidth)



# print("Counts per cr")

# for i in range(60):
# #    print("Lorentz")
# #    print(lorentzian(1,2,1+i))#,0,LL_linewidth))
# #    print(sp.cauchy.pdf(1,2,1+i))#,0,LL_linewidth))

# #    print("diff")
# #    print(lorentzian(1,2,1+i)/(sp.cauchy.pdf(1,2,1+i)))
#     f=get_f_res()
#     print(get_counts(f))        


@jit(nopython=True)
def measure_counts(N_fotons): #laserduur
    return np.random.poisson(N_fotons)
    #return poisson.rvs(N_fotons,size=num)#int(round)

def set_Nthr(percentage, dt=dt,num=100000): #percentage of states that succeeds (counted from the highest)
    maxI=get_counts(f_red,dt)
    counts=np.zeros(num)
    for i in range(num):
        counts[i]=measure_counts(maxI)
    #print(counts)
    counts.sort()
    Nt=int(counts[-int(round(0.85*num))])
    # print("maximal intensity = "+str(maxI))
    # print("N_threshold_succes = "+str(Nt))
    #print(Nt)
    #Nt=np.min(counts)
    #print("minimal Nt = "+str(Nt))
    #Nt=int(Nt*(1-np.exp(-1/Nt)))
    print("minimal Nt = "+str(Nt))
    return Nt#-np.sqrt(Nt)

def set_Nthr_avg(percentage, dt=dt,num=100000): #percentage of states that succeeds (counted from the highest)
    maxI=get_counts(f_red,dt)
    counts=np.zeros(num)
    for i in range(num):
        counts[i]=measure_counts(maxI)
    #print(counts)
    counts.sort()
    # Nt=int(counts[-int(round(percentage*num))])
    # print("maximal intensity = "+str(maxI))
    # print("N_threshold_succes = "+str(Nt))
    #plt.figure()
    #plt.title("average")
    #plt.hist(counts)
    #plt.show()
    Nt = np.round(np.mean(counts)*0.889)
    
    return Nt


# print("threshold suggestions")
# for i in range(60):
#     print(set_Nthr(0.85))

# print("Counts per cr")
# for i in range(20):
#    f=get_f_res()
#    If=get_counts(f)
#    print(measure_counts(If))   

@jit(nopython=True)
def CRcheck(N_thr_succes=N_thr_succes,N_thr_repump=N_thr_repump,sigma=sigma): #experiment=False #using while loop
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    N=0
    while not N>N_thr_succes:
        if N<N_thr_repump or N_red%20==0:#N_red%100==0:
            #T1=time()
            f_res=get_f_res(sigma)
            #T2=time()
            #print("time for f_res"+str(1E6*(T2-T1)))
            #T3=time()
            If=get_counts(f_res)
            #T4=time()
            #print("time for get_counts"+str(1E6*(T4-T3)))
            N_green+=1
        #T5=time()
        N=measure_counts(If)
        #T6=time()
        #print("time for measure_counts"+str((1E6*T6-1E6*T5)))
        N_red+=1
    Delta=f_res-f_red
    # if experiment:
    #     N2=measure_counts(If)
    #     return N,N2
    return Delta, N_green, N_red, N


# print("Counts#1 | Counts#2 | Difference")
# for i in range(20):
#     N1,N2=CRcheck(True)
#     print(str(N1)+"    |    "+str(N2)+"    |    "+str(N1-N2))
# print()
#print(CRcheck())


#Showing difference
maxI=get_counts(f_red,dt)
smaxI=np.sqrt(maxI)
max_arr=np.random.normal(maxI,smaxI,10000000)
bin_loc=np.arange(maxI-2*smaxI,maxI+2*smaxI,10)
for j in range(12):
   plt.subplot(3,4,j+1)
   plt.hist(max_arr,bins=50,label="on resonance")#bin_loc)

   eps=1E-8
   realI=get_counts(get_f_res(),dt)
   srealI=np.sqrt(realI)
   real_arr=np.random.normal(realI,srealI,10000000)
   plt.hist(real_arr,bins=50,label="random")
   plt.legend()
plt.show()


#Showing equivalence od z=x-y and a modified normal
plt.figure()
z_arr=real_arr-max_arr
   #plt.subplot(1,2,1)
plt.hist(z_arr,bins=100, label="Difference")


mdf=np.random.normal(realI-maxI,np.sqrt(smaxI**2+srealI**2),10000000)
plt.hist(mdf,bins=100, label="normal")
plt.vlines(0,0,500000,color='r')
plt.legend()
plt.show()

eps=1E-8
plt.figure()
for i in np.arange(20,step=2):
   dtd=(1+i)*dt
   maxI=get_counts(f_red,dtd)
   smaxI=np.sqrt(maxI)
   realI=get_counts(f_red*(1-eps),dtd)
   srealI=np.sqrt(realI)
   mu_tot=realI-maxI
   si_tot=np.sqrt(smaxI**2+srealI**2)
   mdf=np.random.normal(mu_tot,si_tot,10000000)
   plt.hist(mdf,bins=50, label=("dt ="+str(dtd)))
   plt.vlines(0,0,1000000,color='r')
   success=np.mean(mdf>0)
   print("p on successes = "+str(success))
plt.legend()
plt.show()


print()
print("with modifications")
#modified to have always the same probability on succes:
eps=1E-8
plt.figure()
for i in np.arange(30,step=2):
    dtd=(1+i)*dt
    maxI=get_counts(f_red,(1+i)*dtd)
    smaxI=np.sqrt(maxI)
    maxI=get_counts(f_red,dtd)#added
    realI=get_counts(f_red*(1-eps),(1+i)*dtd)
    srealI=np.sqrt(realI)
    realI=get_counts(f_red*(1-eps),dtd)
    mu_tot=realI-maxI
    si_tot=np.sqrt(smaxI**2+srealI**2)
    mdf=np.random.normal(mu_tot,si_tot,10000000)
    plt.hist(mdf,bins=50, label=("dt ="+str(dtd)))
    plt.vlines(0,0,1000000,color='r')
    success=np.mean(mdf>0)
    success2=np.mean(mdf>10)
    success3=np.mean(mdf>80)
    success4=np.mean(mdf>150)
    print("p on successes = "+str(success))
plt.legend()
plt.show()



