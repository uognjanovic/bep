


n=5
f_res=get_f_res(sigma)
delta=f_res-f_red
#print("delta in MHz = "+str(delta/1E6))
If=get_counts(f_res,dt)#24
#If=24
#print("If = "+str(If))
x=np.random.poisson(minval,size=n)
print(x)#[2:])
y=np.random.poisson(minval,size=n)#np.ones(n,dtype=int)*minval#
print(y)
x=np.zeros(4)
x[0]=6
x[1]=7
x[2]=8
x[3]=7
y=np.zeros(4)
y[0]=6
y[1]=8
y[2]=6
y[3]=6
stat,p=sp.wilcoxon(x,y,alternative="greater")

print("statistic")
print(stat)
print()
print("p-value")
print(p)

# print("If = "+str(If))
# N=np.zeros(5)#N_thr_red)
# N[0]=measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)+measure_counts(If)
# print(N)

# bin_loc=np.arange(start=-3*LL_linewidth, stop=3*LL_linewidth,step=LL_linewidth/20)
# z=sp.cauchy.rvs(loc=0, scale=LL_linewidth, size=10000, random_state=None)
# print("mean = "+str(np.mean(z)/1E6))
# print("variance = "+str(np.var(z)))
# print("sd = "+str(np.std(z)/1E6))
# print("sd2 = "+str(np.sqrt(np.var(z))/1E6))
# plt.figure()
# plt.hist(z,bins=bin_loc)
# plt.show()



    
    # #mask=Y<X
    # #X[mask]=np.nan
    # #Y[mask]=np.nan
    # # print(mask)
    # # print("xs shape")
    # # print(Xs.shape)
    # # print(Xs)
    # #print (X.shape)
    # # thr_s_max=max(N_thr_succes_arr)
    # # thr_s_min=min(N_thr_succes_arr)
    # # thr_r_max=max(N_thr_repump_arr)
    # # thr_r_min=min(N_thr_repump_arr)
    # # thrmin=max(thr_s_min,thr_r_min)
    # # thrmax=min(thr_s_max,thr_r_max)
    # # x_line = np.arange(thrmin,thrmax,step=1)
    # # y_line = np.arange(thrmin,thrmax,step=1)
    # # z_0=np.zeros(len(x_line))
    # # z_1=np.ones(len(x_line))
    # # z_try=np.arange(min(SD_arr),max(SD_arr),step=1)
    # #print (SD_arr.shape)
    # #print (Y.shape)
    # fig = plt.figure()
    # ax = fig.add_subplot(1, 3, 1, projection='3d')      
    # plt.title("Deviation of distance upon succes")
    # ax.set_xlabel("N_thr_succes (#)")
    # ax.set_ylabel("N_thr_repump (#)")
    # ax.set_zlabel("Frequency (MHz)")
    tPlane_arr=Plane_arr.T

        # ax.plot_surface(X, Y, tPlane_arr/1E6 ,color="orange")#label="13 MHZ", )
    # ax.plot_surface(X, Y, tSD_arr/1E6, cmap=cm.coolwarm, linewidth=0, antialiased=False)

    #, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    #X_try,Z_try=np.meshgrid(x_line,z_try)
    #ax.plot_surface(X_try,X_try, Z_try/1E6)
    #ax.plot(x_line, y_line, z_0, label='N_thr_succes<Nthr repump')#line for separation
    #ax.plot(x_line, y_line, z_1, label='N_thr_succes<Nthr repump')#line for separation
    #ax = fig.add_subplot(1, 3, 2, projection='3d')
    # plt.title("Mean (time untill success)")
    # ax.set_xlabel("N_thr_succes (#)")
    # ax.set_ylabel("N_thr_repump (#)")
    # ax.set_zlabel("Time (ms)")
    #plt.plot(Thr_arr,MuT_arr)


    #ax.plot_surface(X, Y, tMuT_arr*1E3, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    
    # ax = fig.add_subplot(1, 3, 3, projection='3d')
    # plt.title("Optimal photon counts") #Calculated with the geometric formula and normalization
    # ax.set_xlabel("sigma (MHz)")
    # ax.set_ylabel("dt (ms)")
    # ax.set_zlabel("Photons (#)")
    # ax.plot_surface(X/1E6, Y*1E3, tN_arr, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Average lambda")
    # #plt.legend()
    # plt.show()

    # ax = fig.add_subplot(1, 3, 3, projection='3d')
    # plt.title("Probability to succeed") #Calculated with the geometric formula and normalization
    # ax.set_xlabel("N_thr_succes (#)")
    # ax.set_ylabel("N_thr_repump (#)")
    # ax.set_zlabel("Probability (/)")
    # tP_arr=P_arr.T
    # ax.plot_surface(X, Y, tP_arr, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    # plt.show()
    #finding optimal values
    # #Constraint 13 MHZ
    # c1=tSD_arr<13E6
    # #finding min average time
    # c2=min(tMuT_arr[c1])
    # #print(np.where(tMuT_arr==c2)[1][0])
    # place=np.where(tMuT_arr==c2)
    # loc=(place[0][0],place[1][0])
    # opt_N_thr_succes=X[loc]
    # opt_N_thr_repump=Y[loc]
    # opt_SD=tSD_arr[loc]
    # opt_MuT=tMuT_arr[loc]
    # print()
    # print("At sigma = "+str(sigma))
    # print("Optimal Location at N_thr_succes = "+str(opt_N_thr_succes)+" and N_thr_repump = "+str(opt_N_thr_repump))
    # print("Spectral diffusion = "+str(opt_SD))
    # print("Mean time untill succes = "+str(opt_MuT))

    opt_N_thr_succes_arr2=np.zeros(dt_size)
    opt_N_thr_repump_arr2=np.zeros(dt_size)
    opt_SD_arr2=np.zeros(dt_size)
    opt_MuT_arr2=np.zeros(dt_size)
    opt_N_arr2=np.zeros(dt_size)   

    #for j in range(dt_size):
        #minval=int(np.floor(get_counts(f_red-LL_linewidth)))
        #Np=int(min(np.ceil(get_counts(f_red,dt=dt_arr[j])),35))
    #    opt_N_thr_succes_arr2[j], opt_N_thr_repump_arr2[j], opt_SD_arr2[j],  opt_MuT_arr2[j], opt_N_arr2[j]=optimal_results(function,12,10,sigma,0.84,0.50,dt_arr[j])#np.max(Np-40,0)





    # plt.figure()
    # plt.title("Optimal N_thresholds for varying dt, sigma = "+str(dt/1E6)+"MHz")
    # plt.plot(dt_arr*1E3,opt_N_thr_succes_arr2,label="N_thr_succes") 
    # plt.plot(dt_arr*1E3, opt_N_thr_repump_arr2,label="N_thr_repump") 
    # plt.legend()
    # plt.xlabel("dt (ms)")
    # plt.ylabel("N_thr (/)")
    # plt.show()

    # plt.figure()
    # plt.title("Optimal time till succes and fequency with varying dt, sigma = "+str(dt/1E6)+"MHz")
    # plt.xlabel("dt (ms)")
    # plt.ylabel("Time (ms)",color='b')
    # plt.plot(dt_arr*1E3,opt_MuT_arr2*1E3,color='b',label="Mean (time till succes)")
    # plt.tick_params(axis="y", labelcolor="b", pad=8)
    # plt.legend(loc='upper left')
    # plt.twinx()
    # plt.plot(dt_arr*1E3,opt_SD_arr/1E6,color='r',label="F_resonance")
    # plt.tick_params(axis="y", labelcolor="r", pad=8)
    # plt.ylabel("Frequency (MHZ)", color='r')  
    # plt.legend(loc='lower right')
    # plt.show()


    # plt.figure()
    # plt.title("Optimal Photon counts, sigma = "+str(dt/1E6)+"MHz")
    # plt.plot(dt_arr*1E3,opt_N_arr2,label="Average N")  
    # plt.legend()
    # plt.xlabel("dt (ms)")
    # plt.ylabel("Photons (#)")
    # #plt.tight_layout()
    # plt.show()


def variation_of_sigma_results(function=CRcheck,length=1000,start=0,step=10):
    #step=10
    sigma_arr=1E6*np.arange(start,start+length+step,step=step)
    size=len(sigma_arr)
    opt_N_thr_succes_arr=np.zeros(size)
    opt_N_thr_repump_arr=np.zeros(size)
    opt_SD_arr=np.zeros(size)
    opt_MuT_arr=np.zeros(size)
    opt_N_arr=np.zeros(size)
    for i in range(size):
        opt_N_thr_succes_arr[i], opt_N_thr_repump_arr[i], opt_SD_arr[i],  opt_MuT_arr[i], opt_N_arr[i]=optimal_results(function,25,25,sigma_arr[i],10,5)
    
    plt.subplot(1,3,1)
    plt.title("Optimal N_thresholds for varying sigma")
    plt.plot(sigma_arr/1E6,opt_N_thr_succes_arr,label="N_thr_succes") 
    plt.plot(sigma_arr/1E6, opt_N_thr_repump_arr,label="N_thr_repump") 
    plt.legend()
    plt.xlabel("Sigma (MHz)")
    plt.ylabel("N_thr")

    plt.subplot(1,3,2)
    plt.title("Optimal time till succes and fequency with varying sigma")
    plt.xlabel("Sigma (MHz)")
    plt.ylabel("Time (ms)",color='b')
    plt.plot(sigma_arr/1E6,opt_MuT_arr*1E3,color='b',label="Mean (time till succes)")
    plt.tick_params(axis="y", labelcolor="b", pad=8)
    #plt.legend(loc='upper left')
    plt.twinx()
    plt.plot(sigma_arr/1E6,opt_SD_arr/1E6,color='r',label="F_resonance")
    plt.tick_params(axis="y", labelcolor="r", pad=8)
    plt.ylabel("Frequency (MHZ)", color='r')  
    plt.legend(loc='lower right')
    


    plt.subplot(1,3,3)
    plt.title("Optimal photon counts")
    plt.plot(sigma_arr/1E6,opt_N_arr,label="Average lambda")  
    plt.legend()
    plt.xlabel("Sigma (MHz)")
    plt.ylabel("Photons (#)")

    plt.show()

    

#variation_of_sigma_results(function=CRcheck_new3,length=100, start=0,step=5)
#variation_of_sigma_results(function=CRcheck,length=100, start=0,step=10)


,dt_length=1,dt_start=0.5,dt_step=0.1


@jit(nopython=True)
def CRcheck(N_thr_succes=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),N_thr_red=20):
    N_green=0 
    N_red=0
    If=get_counts(f_res,dt)
    N=measure_counts(If)
    N_arr=np.zeros(N_thr_red)
    N_arr[0]=N
    N_red+=1
    i=1
    while not N>N_thr_succes:
        if N<N_thr_repump or N_red%N_thr_red==0:
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_arr=np.zeros(N_thr_red)
            i=0
            N_green+=1
        N=measure_counts(If)
        N_arr[i]=N
        i+=1
        N_red+=1
    Delta=f_res-f_red
    return Delta, N_green, N_red, np.mean(N_arr[:i]), f_res



############# Part 2
############################################################################
############################################################################




    
    # #mask=Y<X
    # #X[mask]=np.nan
    # #Y[mask]=np.nan
    # # print(mask)
    # # print("xs shape")
    # # print(Xs.shape)
    # # print(Xs)
    # #print (X.shape)
    # # thr_s_max=max(N_thr_succes_arr)
    # # thr_s_min=min(N_thr_succes_arr)
    # # thr_r_max=max(N_thr_repump_arr)
    # # thr_r_min=min(N_thr_repump_arr)
    # # thrmin=max(thr_s_min,thr_r_min)
    # # thrmax=min(thr_s_max,thr_r_max)
    # # x_line = np.arange(thrmin,thrmax,step=1)
    # # y_line = np.arange(thrmin,thrmax,step=1)
    # # z_0=np.zeros(len(x_line))
    # # z_1=np.ones(len(x_line))
    # # z_try=np.arange(min(SD_arr),max(SD_arr),step=1)
    # #print (SD_arr.shape)
    # #print (Y.shape)
    # fig = plt.figure()
    # ax = fig.add_subplot(1, 3, 1, projection='3d')      
    # plt.title("Deviation of distance upon succes")
    # ax.set_xlabel("N_thr_succes (#)")
    # ax.set_ylabel("N_thr_repump (#)")
    # ax.set_zlabel("Frequency (MHz)")


    # ax.plot_surface(X, Y, tPlane_arr/1E6 ,color="orange")#label="13 MHZ", )
    # ax.plot_surface(X, Y, tSD_arr/1E6, cmap=cm.coolwarm, linewidth=0, antialiased=False)

    #, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    #X_try,Z_try=np.meshgrid(x_line,z_try)
    #ax.plot_surface(X_try,X_try, Z_try/1E6)
    #ax.plot(x_line, y_line, z_0, label='N_thr_succes<Nthr repump')#line for separation
    #ax.plot(x_line, y_line, z_1, label='N_thr_succes<Nthr repump')#line for separation
    #ax = fig.add_subplot(1, 3, 2, projection='3d')
    # plt.title("Mean (time untill success)")
    # ax.set_xlabel("N_thr_succes (#)")
    # ax.set_ylabel("N_thr_repump (#)")
    # ax.set_zlabel("Time (ms)")
    #plt.plot(Thr_arr,MuT_arr)



    #ax.plot_surface(X, Y, tMuT_arr*1E3, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    
    # ax = fig.add_subplot(1, 3, 3, projection='3d')
    # plt.title("Optimal photon counts") #Calculated with the geometric formula and normalization
    # ax.set_xlabel("sigma (MHz)")
    # ax.set_ylabel("dt (ms)")
    # ax.set_zlabel("Photons (#)")
    # ax.plot_surface(X/1E6, Y*1E3, tN_arr, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Average lambda")
    # #plt.legend()
    # plt.show()

    # ax = fig.add_subplot(1, 3, 3, projection='3d')
    # plt.title("Probability to succeed") #Calculated with the geometric formula and normalization
    # ax.set_xlabel("N_thr_succes (#)")
    # ax.set_ylabel("N_thr_repump (#)")
    # ax.set_zlabel("Probability (/)")
    # tP_arr=P_arr.T
    # ax.plot_surface(X, Y, tP_arr, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    # plt.show()

    #finding optimal values
    # #Constraint 13 MHZ
    # c1=tSD_arr<13E6
    # #finding min average time
    # c2=min(tMuT_arr[c1])
    # #print(np.where(tMuT_arr==c2)[1][0])
    # place=np.where(tMuT_arr==c2)
    # loc=(place[0][0],place[1][0])
    # opt_N_thr_succes=X[loc]
    # opt_N_thr_repump=Y[loc]
    # opt_SD=tSD_arr[loc]
    # opt_MuT=tMuT_arr[loc]
    # print()
    # print("At sigma = "+str(sigma))
    # print("Optimal Location at N_thr_succes = "+str(opt_N_thr_succes)+" and N_thr_repump = "+str(opt_N_thr_repump))
    # print("Spectral diffusion = "+str(opt_SD))
    # print("Mean time untill succes = "+str(opt_MuT))


@jit(nopython=True)
def CRcheck(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),N_thr_red=20): 
    N_green=0
    N_red=0
    If=get_counts(f_res,dt)
    N=measure_counts(If)
    N_arr=np.zeros(N_thr_red)
    N_arr[0]=N
    N_red+=1
    i=1
    while not N>N_thr_success:
        if N<N_thr_repump or N_red%N_thr_red==0:
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_arr=np.zeros(N_thr_red)
            i=0
            N_green+=1
        N=measure_counts(If)
        N_arr[i]=N
        i+=1
        N_red+=1
    Delta=f_res-f_red
    return Delta, N_green, N_red, np.mean(N_arr[:i]), f_res


    opt_N_thr_succes_arr2=np.zeros(dt_size)
    opt_N_thr_repump_arr2=np.zeros(dt_size)
    opt_SD_arr2=np.zeros(dt_size)
    opt_MuT_arr2=np.zeros(dt_size)
    opt_N_arr2=np.zeros(dt_size)      
    #X,Y=np.meshgrid(sigma_arr,dt_arr)      
    #X,Y=np.meshgrid(sigma_arr,dt_arr)

        print()
    print("listed values")
    print(Range)
    print()

    #for j in range(dt_size):
        #minval=get_counts(f_red-LL_linewidth,d)
        #Np=int(min(np.ceil(get_counts(f_red,dt=dt_arr[j])),35))
    #    opt_N_thr_succes_arr2[j], opt_N_thr_repump_arr2[j], opt_SD_arr2[j],  opt_MuT_arr2[j], opt_N_arr2[j]=optimal_results(function,12,10,sigma,0.84,0.50,dt_arr[j])#np.max(Np-40,0)


def variation_of_dt_results(function=CRcheck,percentage=0.85,length=1,start=0.5,step=0.1):
    #step=10
    dt_arr=dt*np.arange(start,start+length+step,step=step)
    size=len(dt_arr)
    opt_N_thr_succes_arr=np.zeros(size)
    opt_N_thr_repump_arr=np.zeros(size)
    opt_SD_arr=np.zeros(size)
    opt_MuT_arr=np.zeros(size)
    opt_N_arr=np.zeros(size)
    for i in range(size):
        #Ns=set_Nthr(percentage, dt_arr[i],num=100000)
        #Nr=int(round(Ns/3))
        Np=int(min(np.ceil(get_counts(f_red,dt=dt_arr[i])),20))
        opt_N_thr_succes_arr[i], opt_N_thr_repump_arr[i], opt_SD_arr[i],  opt_MuT_arr[i], opt_N_arr[i]=optimal_results(function,Np,Np,sigma,0,0,dt_arr[i])
    
    #plt.figure()
    #plt.tight_layout()
    plt.subplot(1,3,1)
    plt.title("Optimal N_thresholds for varying dt (Ntot)")
    plt.plot(dt_arr*1E3,opt_N_thr_succes_arr,label="N_thr_succes") 
    plt.plot(dt_arr*1E3, opt_N_thr_repump_arr,label="N_thr_repump") 
    plt.legend()
    plt.xlabel("dt (ms)")
    plt.ylabel("N_thr")

    plt.subplot(1,3,2)
    plt.title("Optimal time till succes and fequency with varying dt")
    plt.xlabel("dt (ms)")
    plt.ylabel("Time (ms)",color='b')
    plt.plot(dt_arr*1E3,opt_MuT_arr*1E3,color='b',label="Mean (time till succes)")
    plt.tick_params(axis="y", labelcolor="b", pad=8)
    plt.legend()
    plt.twinx()
    plt.plot(dt_arr*1E3,opt_SD_arr/1E6,color='r',label="F_resonance")
    plt.tick_params(axis="y", labelcolor="r", pad=8)
    plt.ylabel("Frequency (MHZ)", color='r')  
    plt.legend()
    


    plt.subplot(1,3,3)
    plt.title("Optimal photon counts")
    plt.plot(dt_arr*1E3,opt_N_arr,label="Average lambda")  
    plt.legend()
    plt.xlabel("dt (ms)")
    plt.ylabel("Photons (#)")
    #plt.tight_layout()
    plt.show()

#variation_of_dt_results(function=CRcheck,length=1,start=0.1,step=0.05)



@jit(nopython=True)
def factorial(x):
    fac=1.#(n, dtype=np.int64) 
    for i in range(x):
        fac=fac*(i+1)
    return fac

print(factorial(0))
print(factorial(1))
print(factorial(2))    
print(factorial(3))
print(factorial(10))
print(factorial(26))

@jit(nopython=True)
def hyp_test(x,lam):
    p=0
    for i in range(x):
        p+=np.exp(-lam)/factorial(i)*((float(lam))**i)
    return (1-p)

@jit(nopython=True,parallel=True)
def create_list(minval,rlen=60):
    Range=np.zeros(rlen)
    for j in prange(rlen):
        Range[j]=hyp_test(j,minval)
    return Range

Range=create_list(minval)


#@jit(nopython=True)
def hyp_test2(x,lam):
    p=1-sp.poisson.cdf(x,lam)
    return p

print(hyp_test(0,18))
print(hyp_test(1,18))
print(hyp_test(18,18))
print(hyp_test(14,18))
print(hyp_test(30,18))


@jit(nopython=True)
def CRcheck_routine(N_thr_succes=0.01,N_thr_repump=0.5,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,Range=Range): #experiment=False #using while loop
    N_thr_red=10
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    #N=0
    If=get_counts(f_res,dt)
    N=measure_counts(If)
    N_arr=np.zeros(N_thr_red)
    #y=np.random.poisson(minval,size=N_thr_red)
    N_arr[0]=N
    p=Range[N]#sp.wilcoxon(N_arr[:1],y[:1],zero_method='zsplit',alternative="greater")
    #print(p)
    N_red+=1
    i=1
    while not p<N_thr_succes:# and np.mean(N_arr[:(i)])>minval):
        if p>N_thr_repump or N_red%N_thr_red==0:#N_red%100==0:
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_arr=np.zeros(N_thr_red)
            p=1
            i=0
            N_green+=1
        N=measure_counts(If)
        N_arr[i]=N  
        i+=1
        N_red+=1
        #*p
    Delta=f_res-f_red
    return Delta, N_green, N_red, N , f_res





@jit(nopython=True)
def hyp_test(x,lam):
    p=0
    for i in range(x):
        p+=np.exp(-lam)/factorial(i)*((float(lam))**i)
    return (1-p)

@jit(nopython=True,parallel=True)
def create_list(minval,rlen=60):
    Range=np.zeros(rlen)
    for j in prange(rlen):
        Range[j]=hyp_test(j,minval)
    return Range

Range=create_list(minval)

p=Range[N]


def variation_of_sigma_results(function=CRcheck,length=1000,start=0,step=10):
    #step=10
    sigma_arr=1E6*np.arange(start,start+length+step,step=step)
    size=len(sigma_arr)
    opt_N_thr_succes_arr=np.zeros(size)
    opt_N_thr_repump_arr=np.zeros(size)
    opt_SD_arr=np.zeros(size)
    opt_MuT_arr=np.zeros(size)
    opt_N_arr=np.zeros(size)
    for i in range(size):
        opt_N_thr_succes_arr[i], opt_N_thr_repump_arr[i], opt_SD_arr[i],  opt_MuT_arr[i], opt_N_arr[i]=optimal_results(function,25,25,sigma_arr[i],10,5)
    
    plt.subplot(1,3,1)
    plt.title("Optimal N_thresholds for varying sigma")
    plt.plot(sigma_arr/1E6,opt_N_thr_succes_arr,label="N_thr_succes") 
    plt.plot(sigma_arr/1E6, opt_N_thr_repump_arr,label="N_thr_repump") 
    plt.legend()
    plt.xlabel("Sigma (MHz)")
    plt.ylabel("N_thr")

    plt.subplot(1,3,2)
    plt.title("Optimal time till succes and fequency with varying sigma")
    plt.xlabel("Sigma (MHz)")
    plt.ylabel("Time (ms)",color='b')
    plt.plot(sigma_arr/1E6,opt_MuT_arr*1E3,color='b',label="Mean (time till succes)")
    plt.tick_params(axis="y", labelcolor="b", pad=8)
    #plt.legend(loc='upper left')
    plt.twinx()
    plt.plot(sigma_arr/1E6,opt_SD_arr/1E6,color='r',label="F_resonance")
    plt.tick_params(axis="y", labelcolor="r", pad=8)
    plt.ylabel("Frequency (MHZ)", color='r')  
    plt.legend(loc='lower right')
    


    plt.subplot(1,3,3)
    plt.title("Optimal photon counts")
    plt.plot(sigma_arr/1E6,opt_N_arr,label="Average lambda")  
    plt.legend()
    plt.xlabel("Sigma (MHz)")
    plt.ylabel("Photons (#)")

    plt.show()

    

#variation_of_sigma_results(function=CRcheck_new3,length=100, start=0,step=5)
#variation_of_sigma_results(function=CRcheck,length=100, start=0,step=10)

,dt_length=1.5,dt_start=0.1,dt_step=0.1





    # plt.figure()
    # plt.title("Optimal N_thresholds for varying dt, sigma = "+str(dt/1E6)+"MHz")
    # plt.plot(dt_arr*1E3,opt_N_thr_succes_arr2,label="N_thr_succes") 
    # plt.plot(dt_arr*1E3, opt_N_thr_repump_arr2,label="N_thr_repump") 
    # plt.legend()
    # plt.xlabel("dt (ms)")
    # plt.ylabel("N_thr (/)")
    # plt.show()

    # plt.figure()
    # plt.title("Optimal time till succes and fequency with varying dt, sigma = "+str(dt/1E6)+"MHz")
    # plt.xlabel("dt (ms)")
    # plt.ylabel("Time (ms)",color='b')
    # plt.plot(dt_arr*1E3,opt_MuT_arr2*1E3,color='b',label="Mean (time till succes)")
    # plt.tick_params(axis="y", labelcolor="b", pad=8)
    # plt.legend(loc='upper left')
    # plt.twinx()
    # plt.plot(dt_arr*1E3,opt_SD_arr/1E6,color='r',label="F_resonance")
    # plt.tick_params(axis="y", labelcolor="r", pad=8)
    # plt.ylabel("Frequency (MHZ)", color='r')  
    # plt.legend(loc='lower right')
    # plt.show()


    # plt.figure()
    # plt.title("Optimal Photon counts, sigma = "+str(dt/1E6)+"MHz")
    # plt.plot(dt_arr*1E3,opt_N_arr2,label="Average N")  
    # plt.legend()
    # plt.xlabel("dt (ms)")
    # plt.ylabel("Photons (#)")
    # #plt.tight_layout()
    # plt.show()

    p=(dt_gauss+dt)/meantime

        N_arr[i]=N  
        i+=1

            N_arr=np.zeros(N_thr_red)
            i=0
dt_start

    N_arr=np.zeros(N_thr_red)
    N_arr[0]=N

