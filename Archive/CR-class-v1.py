import math

class quaternion(object):
    """Represents an NV center, with parameters  """
    #constructor
    def __init__(self,ac,xc,yc,zc):
        self.a=ac
        self.x=xc
        self.y=yc
        self.z=zc

    #converts to string to print
    def __str__(self):
        """converts to string to print"""
        return str(self.a)+"+"+str(self.x)+"i+"+str(self.y)+"j+"+str(self.z)+"k"

##    #rounds to 2 digit
##    def __round__(self,n):
##        """rounds to 2 digit"""
##        res=quaternion(0,0,0,0)
##        res.a=round(self.a,n)
##        res.x=round(self.x,n)
##        res.y=round(self.y,n)
##        res.z=round(self.z,n)
##        return res

    #methods
    def get_new_f_res():
        return self.a

    def __add__(self,q):
        res=quaternion(0,0,0,0)
        res.a=self.a+q.a
        res.x=self.x+q.x
        res.y=self.y+q.y
        res.z=self.z+q.z
        return res
    
    def __mul__(self,q):
        res=quaternion(0,0,0,0)
        a=self.a
        b=self.x
        c=self.y
        d=self.z
        e=q.a
        f=q.x
        g=q.y
        h=q.z
        res.a=a*e-b*f-c*g-d*h
        res.x=a*f+b*e-c*h+d*g
        #res.x=a*f-b*e+c*h+d*g
        res.y=a*g+b*h+c*e-d*f
        res.z=a*h-b*g+c*f+d*e
        #res.z=a*h+b*g-c*f+d*e
        return res     

        
    def conjugate(self):
        res=quaternion(0,0,0,0)
        res.a=self.a
        res.x=-self.x
        res.y=-self.y
        res.z=-self.z
        return res
    
        
    def norm(self):
        return float(math.sqrt(self.a**2+self.x**2+self.y**2+self.z**2))

    def reciprocal(self):
        res=self.conjugate()
        res.a=res.a/((self.norm())**2)
        res.x=res.x/((self.norm())**2)
        res.y=res.y/((self.norm())**2)
        res.z=res.z/((self.norm())**2)
        return res

        

def main():
    p=quaternion(2,0,-3,0)
    q=quaternion(0,1,1,-2)
    print ("p = ",p)
    print ("q = ",q)
    print ("p + q = ",p+q)
    print ("p * q = ", q*p)
    print ("conjugate of p is ",p.conjugate())
    print ("norm of p is ",p.norm())
    print ("reciprocal of p is ",p.reciprocal())
    z=p*p.reciprocal()
    z.y=round(z.y,1)
    print ("p x reciprocal(p) = ",z) #ne radi round

if __name__=="__main__":
    main()
    