import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import poisson
#import scipy.stats as sp
import random as rd
from numba import njit, jit, prange
from time import time
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

# import cProfile
# import re
# cProfile.run('re.compile("foo|bar")')

#Parameters
c=299792458         #m/s    #speed of light
lambda_red=637E-9   #m      #golflengte laser
f_red=c/lambda_red  #1/s    #frequentie laser
sigma=50E6          #1/s    #variantie laser
LL_linewidth=13E6   #1/s    #Lifetime limited linewidth lorentzian shape

#Choose:
N_tot=0.3E14 #0.3E14          #-      #Total number of electrons (werken:1E18 met v2 )
N_thr_succes=30            #-      #Treshold number (werken: 1E6 )
N_thr_repump=5            #-      #Treshold number (werken: 1E6 )
dt=50E-6            #s
dt_gauss=400E-6     #s

@jit(nopython=True)
def get_f_res():  
    return rd.gauss(f_red,sigma)

##print("f_red")
##print(str(f_red))
##print()
##for i in range(20):
##    print(get_f_res())
##print()

@jit(nopython=True)
def lorentzian(x,x_0=0.,gamma=1.):
    return 1./(1.+((x-x_0)*(x-x_0)/gamma/gamma))/np.pi/gamma

@jit(nopython=True)
def get_counts(f_resonance): #nog om aan te passen
    #f=sp.cauchy.pdf(f_red,f_resonance,LL_linewidth)
    #return dt*N_tot*sp.cauchy.pdf(f_red,f_resonance,LL_linewidth)
    return dt*N_tot*lorentzian(f_red,f_resonance,LL_linewidth)



# print("Counts per cr")

# for i in range(60):
# #    print("Lorentz")
# #    print(lorentzian(1,2,1+i))#,0,LL_linewidth))
# #    print(sp.cauchy.pdf(1,2,1+i))#,0,LL_linewidth))

# #    print("diff")
# #    print(lorentzian(1,2,1+i)/(sp.cauchy.pdf(1,2,1+i)))
#     f=get_f_res()
#     print(get_counts(f))        


@jit(nopython=True)
def measure_counts(N_fotons,num=1): #laserduur
    return np.random.poisson(N_fotons)
    #return poisson.rvs(N_fotons,size=num)#int(round)

def set_Nthr(percentage,num=10000): #percentage of states that succeeds (counted from the highest)
    maxI=get_counts(f_red)
    counts=measure_counts(maxI,num)
    #print(counts)
    counts.sort()
    return counts[-int(round(percentage*num))]

# print("threshold suggestions")
# for i in range(60):
#     print(set_Nthr(0.80))

# print("Counts per cr")
# for i in range(20):
#    f=get_f_res()
#    If=get_counts(f)
#    print(measure_counts(If))   

@jit(nopython=True)
def CRcheck(N_thr_succes=N_thr_succes,N_thr_repump=N_thr_repump): #experiment=False #using while loop
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    N=0
    while not N>N_thr_succes:
        if N<N_thr_repump or N_red%100==0:
            #T1=time()
            f_res=get_f_res()
            #T2=time()
            #print("time for f_res"+str(1E6*(T2-T1)))
            #T3=time()
            If=get_counts(f_res)
            #T4=time()
            #print("time for get_counts"+str(1E6*(T4-T3)))
            N_green+=1
        #T5=time()
        N=measure_counts(If)
        #T6=time()
        #print("time for measure_counts"+str((1E6*T6-1E6*T5)))
        N_red+=1
    Delta=f_res-f_red
    # if experiment:
    #     N2=measure_counts(If)
    #     return N,N2
    return Delta, N_green, N_red


# print("Counts#1 | Counts#2 | Difference")
# for i in range(20):
#     N1,N2=CRcheck(True)
#     print(str(N1)+"    |    "+str(N2)+"    |    "+str(N1-N2))
# print()
#print(CRcheck())


def CRcheck2(iteration=0): #using recursion
    f_res=get_f_res()
    If=get_counts(f_res)
    N=measure_counts(If)
    #print(N)
    #g = input("Enter your name : ") 
    if N>N_thr_succes:
        Delta=f_res-f_red
        #print("Pass, attempt "+str(iteration))
        return Delta,iteration
    else:
        #print("Fail, attempt "+str(iteration))
        return CRcheck2(iteration+1)



bin_loc=[]
for i in range(60):
    bin_loc.append(i)

amount=20000 
@jit(nopython=True, parallel=True)
def get_results2(function, amount=5000): 
    d=np.zeros(amount)
    N_green=np.zeros(amount)
    N_red=np.zeros(amount)
    #TIME1=time()
    for i in prange(amount):
        Delta, N_g, N_r=function()#1/(j+1))
        d[i]=Delta
        N_green[i]=N_g
        N_red[i]=N_r#*(dt_gauss+dt)+dt)
    #TIME2=time()
    #print("Duration of the loop")
    #print(TIME2-TIME1)
    ##    print()
    ##    print("Resultaten")
    ##    print("Delta = "+str(Delta))
    ##    print("iteration = "+str(iteration))
    ##    print()
    return d, N_green, N_red

def plot_results2(function, j, amount=5000):
    d, N_green, N_red=get_results2(function,amount)
    #plt.subplot(1,3,1)
    plt.subplot(3,4,2*j+1)
    #plt.title("Distance on success") #surpessed to have a better overview
    plt.ylabel("Occurence (normalized)")
    plt.xlabel("Freq (HZ)")
    plt.hist(d, bins=40 ,density=True)
    mu=np.mean(d)
    var=np.var(d)
    x_arr=np.linspace(np.min(d),np.max(d),1000)
    y_arr=1./np.sqrt(2*np.pi*var)*np.exp(-0.5*(x_arr-mu)**2/var)
    plt.plot(x_arr,y_arr,label="Fit")
    plt.legend()
    #print("Number of zeros is "+str(zero))
    print("Delta = "+str(mu)+" +/- "+str(np.sqrt(var)))
    mu_green=np.mean(N_green)
    mu_red=np.mean(N_red)
    #var_T=np.var(itera)
    print("Mean (time untill success) in seconds = "+str(dt_gauss*mu_green+dt*mu_red))
    #print(sum(d)/len(d))
    #plt.plot(d,"+")
    #plt.subplot(1,3,3)
    plt.subplot(3,4,2*j+2)
    #plt.title("Iterations untill success for "+str(len(d))+" iterations")
    #plt.ylabel("Occurence (normalized)") #surpessed to have a better overview
    #plt.xlabel("Time (s)")
    plt.xlabel("Iteration")
    x_lst1=np.linspace(np.min(N_green),np.max(N_green),20)
    #print(x_lst1)
    p=1./mu_green
    print("Chance on succes p = "+str(p) )
    print()
    y_lst1=p*((1-p)**(x_lst1-1))
    #print(y_lst1)
    # plt.hist(itera,bins=20,density=True)#bin_loc)
    plt.hist(N_green,bins=50,density=True)#bin_loc)
    plt.plot(x_lst1,y_lst1, linewidth=2, label="Geometric fit")
    #y_lst2=p*((1-p)**bin_loc)
    #plt.plot(bin_loc,y_lst2, label="Geometric fit 2", linestyle="--")
    plt.legend()
    #plt.show()

# for j in range(6):
#     plot_results2(CRcheck,j,1000000)#50000)   
# plt.show()

@jit(nopython=True,parallel=True)
def get_results(function, a, b, amount=5000):
    #print() 
    #print("N_thr_green = "+str(a))
    #print("N_thr_repump = "+str(b))
    #print()
    d=np.zeros(amount)
    N_green=np.zeros(amount)
    N_red=np.zeros(amount)
    for i in prange(amount):
        Delta, N_g, N_r=function(a,b)
        d[i]=Delta
        N_green[i]=N_g
        N_red[i]=N_r #*(dt_gauss+dt)+dt)
    mu=np.mean(d)
    var=np.var(d)
    sd=np.sqrt(var)
    #print("Delta = "+str(mu)+" +/- "+str(sd))
    mu_g=np.mean(N_green)
    mu_r=np.mean(N_red)
    meantime=dt_gauss*mu_g+dt*mu_r
    #print("Mean (time untill success) in seconds = "+str(meantime))
    p=(dt_gauss+dt)/meantime
    # print("Chance on succes p = "+str(p) )
    return sd, meantime, p

@jit(nopython=True,parallel=True)
def Results_of_variation(function,g_num,r_num,N_thr_succes=0,N_thr_repump=0):
    N_thr_succes_arr=np.arange(N_thr_succes,N_thr_succes+g_num,1)
    N_thr_repump_arr=np.arange(N_thr_repump,N_thr_repump+r_num,1)
    SD_arr= np.zeros((g_num,r_num))
    MuT_arr=np.zeros((g_num,r_num))
    P_arr=np.zeros((g_num,r_num))
    for i in prange(g_num):
        for j in prange(r_num):
            SD_arr[i,j], MuT_arr[i,j] , P_arr[i,j]=get_results(function,N_thr_succes_arr[i],N_thr_repump_arr[j] ) #, P_arr[i,j]
    return N_thr_succes_arr, N_thr_repump_arr, SD_arr, MuT_arr, P_arr
    
def plots_of_variation(function,g_num,r_num):
    N_thr_succes_arr, N_thr_repump_arr, SD_arr, MuT_arr, P_arr=Results_of_variation(function,g_num,r_num)
    X,Y=np.meshgrid(N_thr_succes_arr,N_thr_repump_arr)
    #print (X.shape)
    
    #print (SD_arr.shape)
    #print (Y.shape)
    fig = plt.figure()
    ax = fig.add_subplot(1, 3, 1, projection='3d')      
    plt.title("Deviation of distance upon succes")
    ax.set_xlabel("N_thr_succes (#)")
    ax.set_ylabel("N_thr_repump (#)")
    ax.set_zlabel("Frequency (MHz)")
    ax.plot_surface(X, Y, SD_arr.T/1E6, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    
    ax = fig.add_subplot(1, 3, 2, projection='3d')
    plt.title("Mean (time untill success)")
    ax.set_xlabel("N_thr_succes (#)")
    ax.set_ylabel("N_thr_repump (#)")
    ax.set_zlabel("Time (ms)")
    #plt.plot(Thr_arr,MuT_arr)
    ax.plot_surface(X, Y, MuT_arr.T*1E3, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    
    ax = fig.add_subplot(1, 3, 3, projection='3d')
    plt.title("Probability to succeed") #Calculated with the geometric formula and normalization
    ax.set_xlabel("N_thr_succes (#)")
    ax.set_ylabel("N_thr_repump (#)")
    ax.set_zlabel("Probability (/)")
    ax.plot_surface(X, Y, P_arr.T, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    plt.show()


plots_of_variation(CRcheck,50,40)




# ##Plotting and showing results part
# bin_loc=[]
# for i in range(20):
#     bin_loc.append(i)

# amount=5000    
# d=np.zeros(amount)
# itera=np.zeros(amount)
# zero=0
# #TIME1=time()
# for i in range(amount):
#     Delta,iteration=CRcheck()
#     if iteration==0:
#         zero+=1
#     d[i]=Delta
#     itera[i]=iteration #*(dt_gauss+dt)+dt)
# #TIME2=time()
# #print("Duration of the loop")
# #print(TIME2-TIME1)
# ##    print()
# ##    print("Resultaten")
# ##    print("Delta = "+str(Delta))
# ##    print("iteration = "+str(iteration))
# ##    print()

# plt.subplot(1,3,1)
# plt.title("Distance on success")
# plt.ylabel("Occurence (normalized)")
# plt.xlabel("Freq (1/s)")
# plt.hist(d, bins=30 ,density=True)
# mu=np.mean(d)
# var=np.var(d)
# x_arr=np.linspace(np.min(d),np.max(d),1000)
# y_arr=1./np.sqrt(2*np.pi*var)*np.exp(-0.5*(x_arr-mu)**2/var)
# plt.plot(x_arr,y_arr,label="Fit")
# plt.legend()
# #print("Number of zeros is "+str(zero))
# print("Delta = "+str(mu)+" +/- "+str(np.sqrt(var)))
# mu_T=np.mean(itera)
# var_T=np.var(itera)
# print("Mean (time untill success) in seconds = "+str((mu_T+1)*(dt+dt_gauss)))
# #print(sum(d)/len(d))
# #plt.plot(d,"+")
# plt.subplot(1,3,3)
# plt.title("Iterations untill success for "+str(len(d))+" iterations")
# plt.ylabel("Occurence (normalizes)")
# plt.xlabel("Time (s)")
# x_lst1=np.linspace(np.min(itera),np.max(itera),20)
# #print(x_lst1)
# p=1./mu_T
# print("Chance on succes p = "+str(p) )
# y_lst1=p*((1-p)**x_lst1)
# #print(y_lst1)
# # plt.hist(itera,bins=20,density=True)#bin_loc)
# plt.hist(itera,bins=bin_loc,density=True)#bin_loc)
# plt.plot(x_lst1,y_lst1, linewidth=2, label="Geometric fit")
# y_lst2=p*((1-p)**bin_loc)
# plt.plot(bin_loc,y_lst2, label="Geometric fit 2", linestyle="--")
# plt.legend()
# plt.show()
