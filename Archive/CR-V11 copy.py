import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sp
from scipy.stats import poisson
import random as rd
from numba import njit, jit, prange
from time import time
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
#from matplotlib.ticker import LinearLocator, FormatStrFormatter
import math
from scipy.optimize import curve_fit

# import cProfile
# import re
# cProfile.run('re.compile("foo|bar")')
core=8

#Parameters
c=299792458         #m/s    #speed of light
lambda_red=637E-9   #m      #golflengte laser
f_red=c/lambda_red  #1/s    #frequentie laser
sigma=50E6          #1/s    #variantie laser
LL_linewidth=13E6   #1/s    #Lifetime limited linewidth lorentzian shape

#Choose:
N_tot=0.3E14        #-      #Total number of electrons (werken:1E18 met v2 )
N_thr_success=30    #-      #Treshold number (werken: 1E6 )
N_thr_repump=5      #-      #Treshold number (werken: 1E6 )
dt=50E-6            #s
dt_gauss=400E-6     #s      
psi=0.2

def generalized_normal(x, mu=0, beta=2, alpha=1):
    return beta/2/alpha/math.gamma(1/beta)*np.exp(-((abs(x-mu)/alpha)**beta))


@jit(nopython=True)
def get_f_res(sigma=sigma,f_red=f_red):
    return np.random.normal(f_red,sigma)
    
    
#return rd.gauss(f_red,sigma)

##print("f_red")
##print(str(f_red))
##print()
##for i in range(20):
##    print(get_f_res())
##print()

@jit(nopython=True)
def lorentzian(x,x_0=0.,gamma=1.):
    return 1./(1.+((x-x_0)*(x-x_0)/gamma/gamma))/np.pi/gamma

@jit(nopython=True)
def get_counts(f_resonance,dt=dt):
    return dt*N_tot*lorentzian(f_red,f_resonance,LL_linewidth)

#nog om aan te passen
#f=sp.cauchy.pdf(f_red,f_resonance,LL_linewidth)
#return dt*N_tot*sp.cauchy.pdf(f_red,f_resonance,LL_linewidth)

print("max value lamda")
print(get_counts(f_red))
print()
print("minimal value lamda needed to pass")
minval=np.floor(f_red-LL_linewidth)
print(get_counts(f_red-LL_linewidth))
print(get_counts(f_red+LL_linewidth))
print()
# print("Counts per cr")

# for i in range(60):
# #    print("Lorentz")
# #    print(lorentzian(1,2,1+i))#,0,LL_linewidth))
# #    print(sp.cauchy.pdf(1,2,1+i))#,0,LL_linewidth))

# #    print("diff")
# #    print(lorentzian(1,2,1+i)/(sp.cauchy.pdf(1,2,1+i)))
#     f=get_f_res()
#     print(get_counts(f))        


@jit(nopython=True)
def measure_counts(N_fotons): 
    return np.random.poisson(N_fotons)

#laserduur
#return poisson.rvs(N_fotons,size=num)#int(round)

def set_Nthr(percentage, dt=dt,num=100000): #percentage of states that succeeds (counted from the highest)
    maxI=get_counts(f_red,dt)
    counts=np.zeros(num)
    for i in range(num):
        counts[i]=measure_counts(maxI)
    #print(counts)
    counts.sort()
    Nt=int(counts[-int(round(percentage*num))])
    print("maximal intensity"+str(maxI))
    print("N_threshold_success"+str(Nt))
    return Nt


# print("threshold suggestions")
# for i in range(60):
#     print(set_Nthr(0.85))

# print("Counts per cr")
# for i in range(20):
#    f=get_f_res()
#    If=get_counts(f)
#    print(measure_counts(If))   

@jit(nopython=True)
def CRcheck(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,N_thr_red=20):
    N_green=0 
    N_red=0
    If=get_counts(f_res,dt)
    N=measure_counts(If)
    N_red+=1
    while not N>N_thr_success:
        if N<N_thr_repump or N_red%N_thr_red==0:
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_green+=1
        N=measure_counts(If)
        N_red+=1
    Delta=f_res-f_red
    return Delta, N_green, N_red, N, f_res

@jit(nopython=True)
def CRcheck_prim(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,N_thr_red=20):
    N_green=0 
    N_red=0
    If=get_counts(f_res,dt)
    N=measure_counts(If)
    N_red+=1
    while not N>N_thr_success:
        f_res=get_f_res(sigma)
        If=get_counts(f_res,dt)
        N_green+=1
        N=measure_counts(If)
        N_red+=1
    Delta=f_res-f_red
    return Delta, N_green, N_red, N, f_res



#begin je met groen stralen of is de eerste trekking gratis?
#,Nold=0,N_thr_red=100): #experiment=False #using while loop



@jit(nopython=True)
def zmean(x):
    if not int(x[len(x)-1])==0:
        return np.mean(x)
    for i in range(len(x)):
        if int(x[i])==0:
            if i==0:
                return 0
            else:
                #print( np.mean(x[:i]))
                return np.mean(x[:i])

@jit(nopython=True)
def zvar(x):
    if not int(x[len(x)-1])==0:
        return np.var(x)
    for i in range(len(x)):
        if int(x[i])==0:
            if i==0:
                return 0
            else:
                #print( np.mean(x[:i]))
                return np.var(x[:i])                
# x=np.zeros(10)
# x[0]=1
# x[1]=4
# print(zmean(x))


@jit(nopython=True)
def CRcheck_new(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,N_thr_red=20): #experiment=False #using while loop
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    #N=0
    If=get_counts(f_res,dt/3)
    Nt=(measure_counts(If)+measure_counts(If)+measure_counts(If))
    N_red+=1
    while not Nt>N_thr_success:
        if  Nt<N_thr_repump or N_red%N_thr_red==0:#N_red%100==0:
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt/3)
            N_green+=1
        #N[iz%3]=measure_counts(If)
        #Nt=int(round(np.mean(N)))
        Nt=(measure_counts(If)+measure_counts(If)+measure_counts(If))
        N_red+=1
    Delta=f_res-f_red
    return Delta, N_green, N_red, Nt, f_res#Nt

@jit(nopython=True)
def CRcheck_new2(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,N_thr_red=20): #experiment=False #using while loop
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    #N=0
    If=get_counts(f_res,dt)
    Nt=measure_counts(If)
    N_red+=1
    while not Nt>N_thr_success:
        if  Nt<N_thr_repump or N_red%20==0:
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_green+=1
            Nt=measure_counts(If)
            N_red+=1
        else:
            Nt+=measure_counts(If)
            Nt=Nt/2
            N_red+=1
    Delta=f_res-f_red
    return Delta, N_green, N_red, Nt, f_res

@jit(nopython=True)
def CRcheck_new3(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,ro=20): #experiment=False #using while loop
    N_thr_red=10
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    #N=0
    # eps=3
    # if not Nold > (N_thr_success+eps):
    #     f_res=get_f_res(sigma)
    #     N_green+=1
    If=get_counts(f_res,dt)
    N=np.zeros(N_thr_red)
    N[0]=measure_counts(If)
    N_red+=1
    Nt=zmean(N)
    iz=1
    while not Nt>(N_thr_success+ro/iz**0.8):#/np.sqrt(iz)):#N_thr_success
        if  Nt<(N_thr_repump) or N_red%N_thr_red==0:#-np.sqrt(N_thr_repump/iz)
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_green+=1
            N=np.zeros(N_thr_red)
            iz=0
        
        N[iz]=measure_counts(If)
        iz+=1
        N_red+=1
        Nt=zmean(N)
    Delta=f_res-f_red
    return Delta, N_green, N_red, Nt, f_res

@jit(nopython=True)#,parallel=True)
def CRcheck_mem(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,N_thr_red=15): #experiment=False #using while loop
    N_thr_red=15
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    n=50
    length=25
    If=get_counts(f_res,dt/n*length)
    #N=0
    #for i in range(length):
    #    N+=measure_counts(If)
    totallen=length
    N=measure_counts(If)
    N=n/totallen*N
    N_red+=1
    while not N>N_thr_success:#N_thr_success,min(iz,2)
        if  N<N_thr_repump or N_red%N_thr_red==0:#-np.sqrt(N_thr_repump/iz)
            f_res=get_f_res(sigma)
            #If=get_counts(f_res,dt/n)
            If=get_counts(f_res,dt/n*length)
            totallen=0
            N_green+=1
            N=0
        #for j in range(length):
            #N+=measure_counts(If)
        N+=measure_counts(If)
        totallen+=length
        totallen=min(2*length,totallen)
        N=n/totallen*N
        N_red+=1
    If=get_counts(f_res,dt)    
    Delta=f_res-f_red
    return Delta, N_green, N_red*length/n, If, f_res


@jit(nopython=True)#,parallel=True)
def CRcheck_radical(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,N_thr_red=15): #experiment=False #using while loop
    N_thr_red=15
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    n=50
    length=25
    If=get_counts(f_res,dt/n*length)
    #N=0
    #for i in range(length):
    #    N+=measure_counts(If)
    N=measure_counts(If)
    N=n/length*N
    N_red+=1
    while not N>N_thr_success:#N_thr_success,min(iz,2)
        if  N<N_thr_repump or N_red%N_thr_red==0:#-np.sqrt(N_thr_repump/iz)
            f_res=get_f_res(sigma)
            #If=get_counts(f_res,dt/n)
            If=get_counts(f_res,dt/n*length)
            N_green+=1
        #N=0
        #for j in range(length):
            #N+=measure_counts(If)
        N=measure_counts(If)
        N=n/length*N
        N_red+=1
    #If=get_counts(f_res,dt)    
    Delta=f_res-f_red
    return Delta, N_green, N_red*length/n, N, f_res

@jit(nopython=True)#,parallel=True)
def CRcheck_radical2(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,N_thr_red=15): #experiment=False #using while loop
    N_thr_red=15
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    chi=1./5
    #length=25
    Ifi=get_counts(f_res,dt*chi)
    #N=0
    #for i in range(length):
    #    N+=measure_counts(If)
    Ni=measure_counts(Ifi)
    N_red+=chi
    N_thr_success_init=3# 4 works
    i=0
    while i<3:
        while not Ni>N_thr_repump:
                f_res=get_f_res(sigma)
                N_green+=1
                Ifi=get_counts(f_res,dt*chi)
                Ni=measure_counts(Ifi)
                N_red+=1*chi
        Ni=measure_counts(Ifi)
        N_red+=1*chi
        i+=1
    If=get_counts(f_res,dt)
    N=measure_counts(If)
    while not N>N_thr_success:#N_thr_success,min(iz,2)
        if  N<N_thr_repump or N_red%N_thr_red==0:#-np.sqrt(N_thr_repump/iz)
            
            f_res=get_f_res(sigma)
            #If=get_counts(f_res,dt/n)
            If=get_counts(f_res,dt)
            N_green+=1
        #N=0
        # for j in range(length):
            #N+=measure_counts(If)
        N=measure_counts(If)
        #N=n/length*N
        N_red+=1
    If=get_counts(f_res,dt)    
    Delta=f_res-f_red
    return Delta, N_green, N_red, N, f_res

@jit(nopython=True)#,parallel=True)
def CRcheck_step(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,N_thr_red=15): #experiment=False #using while loop
    N_thr_red=15
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    If=get_counts(f_res,dt/3)
    N=measure_counts(If)
    N_red+=1./3
    #N_thr_success_init=3# 4 works
    while not N>N_thr_success:#,min(iz,2)
        if  N>=N_thr_repump:# or N_red%N_thr_red==0:#-np.sqrt(N_thr_repump/iz)
            If=get_counts(f_res,dt/4)
            N=measure_counts(If)
            N_red+=1./4
            if N>=1:
                If=get_counts(f_res,dt/6)
                N=measure_counts(If)
                N_red+=1./6
                if N>=1:
                    If=get_counts(f_res,dt/6)
                    N=measure_counts(If)
                    N_red+=1./12
                    if N>=1:
                        If=get_counts(f_res,dt)
                        N=measure_counts(If)
                        N_red+=1
                
        # for j in range(length):
            #N+=measure_counts(If)
            #N=measure_counts(If)
        #N=n/length*N
        else:
            f_res=get_f_res(sigma)
            N_green+=1
            If=get_counts(f_res,dt/3)
            N=measure_counts(If)
            N_red+=1./3
        N_red+=1
    If=get_counts(f_res,dt)    
    Delta=f_res-f_red
    return Delta, N_green, N_red, N, f_res



@jit(nopython=True)
def CRcheck_new5(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,ro=10): #experiment=False #using while loop
    N_thr_red=5
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    N_thr_success_arr=np.zeros(N_thr_red)
    N_thr_success_arr[0]=N_thr_success
    N_thr_success_arr[1]=N_thr_success-0.6
    N_thr_success_arr[2:]=N_thr_success-1
    #N_thr_success_arr[3]=N_thr_success-1.3
    #N_thr_success_arr[4:]=N_thr_success-1.5
    #N_thr_success_arr[3:]=9.5#:]=ro
    N_thr_repump_arr=np.zeros(N_thr_red)
    N_thr_repump_arr[0]=N_thr_repump
    N_thr_repump_arr[1]=N_thr_repump+0.5
    N_thr_repump_arr[2:]=N_thr_repump+1
    #N_thr_repump_arr[3]=N_thr_repump+1.3
    #N_thr_repump_arr[4:]=N_thr_repump+1.5
    #N=0
    # eps=3
    # if not Nold > (N_thr_success+eps):
    #     f_res=get_f_res(sigma)
    #     N_green+=1
    If=get_counts(f_res,dt)
    N=np.zeros(5)#N_thr_red)
    N[0]=measure_counts(If)
    N_red+=1
    Nt=zmean(N)
    iz=0
    while not Nt>N_thr_success_arr[iz]:#N_thr_success,min(iz,2)
        iz+=1 
        if  Nt<(N_thr_repump_arr[iz-1]) or N_red%N_thr_red==0:#-np.sqrt(N_thr_repump/iz)
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_green+=1
            N=np.zeros(N_thr_red)
            iz=0
        
        N[iz%3]=measure_counts(If)
        N_red+=1
        Nt=zmean(N)
    Delta=f_res-f_red
    return Delta, N_green, N_red, Nt, f_res

@jit(nopython=True)
def CRcheck_r2(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,ro=10): #experiment=False #using while loop
    N_thr_red=15
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    N_thr_success_arr=np.zeros(N_thr_red)
    N_thr_success_arr[0:]=N_thr_success
    N_thr_success_arr[1]=N_thr_success-0.6
    N_thr_success_arr[2:]=N_thr_success-1
    # N_thr_success_arr[3]=N_thr_success-1.3
    # N_thr_success_arr[4:]=N_thr_success-1.5
    #N_thr_success_arr[3:]=9.5#:]=ro
    N_thr_repump_arr=np.zeros(N_thr_red)
    N_thr_repump_arr[0:]=N_thr_repump
    N_thr_repump_arr[1]=N_thr_repump+0.5
    # N_thr_repump_arr[2]=N_thr_repump+1
    # N_thr_repump_arr[3]=N_thr_repump+1.3
    # N_thr_repump_arr[4:]=N_thr_repump+1.5
    #N=0
    # eps=3
    # if not Nold > (N_thr_success+eps):
    #     f_res=get_f_res(sigma)
    #     N_green+=1
    If=get_counts(f_res,dt)
    N=np.zeros(N_thr_red)
    N[0]=measure_counts(If)
    N_red+=1
    Nt=N[0]+0.5*zvar(N)#zmean(N)+0.5*zvar(N)
    iz=0
    while not Nt>N_thr_success_arr[iz]:#N_thr_success,min(iz,2)
        iz+=1 
        if  Nt<(N_thr_repump_arr[iz-1]) or N_red%N_thr_red==0:#-np.sqrt(N_thr_repump/iz)
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_green+=1
            N=np.zeros(N_thr_red)
            iz=0
        
        N[iz]=measure_counts(If)#%15
        N_red+=1
        Nt=N[iz]+0.5*zvar(N)
    Delta=f_res-f_red
    return Delta, N_green, N_red, zmean(N), f_res


@jit(nopython=True)
def CRcheck_new4(N_thr_success=N_thr_success,N_thr_repump=N_thr_repump,sigma=sigma,dt=dt,f_res=get_f_res(),Nold=0,N_thr_red=20): #experiment=False #using while loop
    #N_thr_red=20
    N_green=0 #begin je met groen stralen of is de eerste trekking gratis?
    N_red=0
    eps=5
    #N=0
    if not Nold > (N_thr_success+eps):
        f_res=get_f_res(sigma)
        N_green+=1
    If=get_counts(f_res,dt)
    Nt=measure_counts(If)
    N_red+=1
    while not Nt>N_thr_success:
        if  Nt<N_thr_repump or N_red%20==0:
            f_res=get_f_res(sigma)
            If=get_counts(f_res,dt)
            N_green+=1
            Nt=measure_counts(If)
            N_red+=1
        else:
            Nt=measure_counts(If)
            #Nt=Nt/2
            N_red+=1
    Delta=f_res-f_red
    return Delta, N_green, N_red, Nt, f_res



# for i in range(30):
#     print(CRcheck_new())

bin_loc=np.arange(60,step=1,dtype=int)
amount=20000 

@jit(nopython=True,parallel=True)
def get_resultsv2(function, a, b, c, dt=dt, N_thr_red=20, amount=200000):
    #print() 
    #print("N_thr_green = "+str(a))
    #print("N_thr_repump = "+str(b)) 
    #print("running")
    d=np.zeros(amount*core)
    N_green=np.zeros(amount*core)
    N_red=np.zeros(amount*core)
    N_on_success=np.zeros(amount*core)
    for j in prange(core):
        f_res=get_f_res()
        N_old=0
        for i in range(amount):
            d[i+j*amount], N_green[i+j*amount], N_red[i+j*amount], N_old,f_res_old=function(a,b,c,dt,f_res,N_old,N_thr_red)
            N_on_success[i+j*amount]=N_old
            #f_res=get_f_res(c,f_red)
            f_res=get_f_res(psi*c,f_res_old)#usual is /5 met /3
            # N, Delta, N_g, N_r=function(a,b,c)
            # d[i]=Delta
            # N_green[i]=N_g
            # N_red[i]=N_r #*(dt_gauss+dt)+dt)
    return d, N_green, N_red, N_on_success

@jit(nopython=True, parallel=True)
def get_results2(function, amount=5000,a=30): 
    d=np.zeros(amount)
    N_green=np.zeros(amount)
    N_red=np.zeros(amount)
    #TIME1=time()
    for i in prange(amount):
        Delta, N_g, N_r, N_a,f=function(N_thr_success=a)#1/(j+1))
        d[i]=Delta
        N_green[i]=N_g
        N_red[i]=N_r#*(dt_gauss+dt)+dt)
    #TIME2=time()
    #print("Duration of the loop")
    #print(TIME2-TIME1)
    ##    print()
    ##    print("Resultaten")
    ##    print("Delta = "+str(Delta))
    ##    print("iteration = "+str(iteration))
    ##    print()
    return d, N_green, N_red

def plot_distribution(function=CRcheck, N_thr_success=N_thr_success, N_thr_repump=N_thr_repump, sigma=sigma,dt=dt, amount=200000):
    #N_thr_success=10

    d, N_green, N_red, N=get_resultsv2(function, N_thr_success, N_thr_repump, sigma, dt=dt, N_thr_red=20, amount=amount)
    #d, N_green, N_red=get_results2(function,amount,N_thr_success)
    #plt.subplot(1,3,1)
    #plt.subplot(2,4,2*j+1)
    plt.figure()
    plt.title("Distance on success N_thr_success= "+str(N_thr_success)+" and N_thr_repump= "+str(N_thr_repump)) #surpessed to have a better overview
    plt.ylabel("Normalized occurence (-)")
    plt.xlabel("Freqeuncy (MHz)")
    number_of_bins=50
    d=d/1E6
    count, bins, ignored = plt.hist(d, bins=number_of_bins ,density=True)
    mu=np.mean(d)
    var=np.var(d)
    x_arr=np.linspace(np.min(d),np.max(d),1000)
    #y_arr=1./np.sqrt(2*np.pi*var)*np.exp(-0.5*(x_arr-mu)**2/var)
    #plt.plot(x_arr,y_arr,label="Normal Fit")
    
    # #Code for adding a generalized normal fit
    # #sorted_d=d.sort()
    # x_fit=np.linspace(np.min(d),np.max(d),number_of_bins)
    # y_fit=np.zeros(number_of_bins)
    # for k in range(number_of_bins):
    #     for l in range(amount):
    #         if d[l]>x_fit[k] and d[l]<x_fit[k+1]:
    #             y_fit[k]+=1
    # y_fit=y_fit/np.linalg.norm(y_fit)/(x_fit[1]-x_fit[0])
    # print(y_fit)
    # popt,pcov=curve_fit(generalized_normal,x_fit,y_fit)
    # mu_fit=popt[0]
    # print("mu_fit="+str(mu_fit))
    # beta_fit=popt[1]
    # print("mu_fit="+str(mu_fit))
    # alpha_fit=popt[2]
    # plt.plot(x_arr,generalized_normal(x_arr, mu_fit, beta_fit, alpha_fit), label="Generalized normal fit")
    #plt.plot(label="with mu="+str(mu))
    guess=np.zeros(3)
    guess[0]=mu
    guess[1]=2#10
    guess[2]=1.28*np.sqrt(2*var)
    popt,pcov=curve_fit(generalized_normal,(bins[:-1]+bins[1:])/2,count,p0=guess)#,bounds=(lbound,rbound))
    #popt,pcov=curve_fit(generalized_normal,(bins[:-1]+bins[1:])/2,count)
    
    print(popt)
    
    mu_fit=popt[0]
    print("mu_fit="+str(mu_fit))
    beta_fit=popt[1]
    print("beta_fit="+str(beta_fit))
    alpha_fit=popt[2]
    print("alpha_fit="+str(alpha_fit))   
    plt.plot(x_arr,generalized_normal(x_arr, mu_fit, beta_fit, alpha_fit),linewidth=2, label=("Generalized normal fit"))
    #plt.text(0.5E7,8E-8,("with mu_fit = "+str(mu_fit)+", beta_fit = "+str(beta_fit)+" and alpha_fit = "+str(alpha_fit)))
    plt.legend()#loc="best")
    #print("Number of zeros is "+str(zero))
    print("Delta = "+str(mu)+" +/- "+str(np.sqrt(var)))
    mu_green=np.mean(N_green)
    mu_red=np.mean(N_red)
    #var_T=np.var(itera)
    print("Mean (time untill success) in seconds = "+str(dt_gauss*mu_green+dt*mu_red))
    #print(sum(d)/len(d))
    #plt.plot(d,"+")
    #plt.subplot(1,3,3)
    #plt.subplot(2,4,2*j+2)
    plt.show()
    # plt.figure()
    # #plt.title("Iterations untill success for "+str(len(d))+" iterations")
    # #plt.ylabel("Occurence (normalized)") #surpessed to have a better overview
    # #plt.xlabel("Time (s)")
    # plt.xlabel("Iteration")
    # x_lst1=np.linspace(np.min(N_green),np.max(N_green),20)
    # #print(x_lst1)
    # p=1./mu_green
    # print("Chance on success p = "+str(p) )
    # print()
    # y_lst1=p*((1-p)**(x_lst1))#-1
    # #print(y_lst1)
    # # plt.hist(itera,bins=20,density=True)#bin_loc)
    # plt.hist(N_green,bins=bin_loc,density=True)#bin_loc)
    # plt.plot(x_lst1,y_lst1, linewidth=2, label="Geometric fit")
    # #y_lst2=p*((1-p)**bin_loc)
    # #plt.plot(bin_loc,y_lst2, label="Geometric fit 2", linestyle="--")
    # plt.legend()
    # plt.show()


plot_distribution(function=CRcheck, N_thr_success=40, N_thr_repump=20, sigma=sigma,dt=dt, amount=200000)


@jit(nopython=True,parallel=True)
def get_results(function, a, b, c, dt=dt, N_thr_red=20, amount=200000):
    #print() 
    #print("N_thr_green = "+str(a))
    #print("N_thr_repump = "+str(b)) 
    #print("running")
    d=np.zeros(amount*core)
    N_green=np.zeros(amount*core)
    N_red=np.zeros(amount*core)
    N_on_success=np.zeros(amount*core)
    for j in prange(core):
        f_res=get_f_res()
        N_old=0
        for i in range(amount):
            d[i+j*amount], N_green[i+j*amount], N_red[i+j*amount], N_old,f_res_old=function(a,b,c,dt,f_res,N_old,N_thr_red)
            f_res=get_f_res(psi*c,f_res_old)
            N_on_success[i+j*amount]=N_old
            #usual is /5 met /3
            # N, Delta, N_g, N_r=function(a,b,c)
            # d[i]=Delta
            # N_green[i]=N_g
            # N_red[i]=N_r #*(dt_gauss+dt)+dt)



    N_avg=np.mean(N_on_success)
    # count, bins, ignored = plt.hist(d, bins=number_of_bins ,density=True)
    # guess=np.zeros(3)
    # guess[0]=mu
    # guess[1]=10
    # guess[2]=1.28*np.sqrt(2*var)
    # popt,pcov=curve_fit(generalized_normal,(bins[:-1]+bins[1:])/2,count,p0=guess)
    # mu_fit=popt[0]
    # #print("mu_fit="+str(mu_fit))
    # beta_fit=popt[1]
    # #print("beta_fit="+str(beta_fit))
    # alpha_fit=popt[2]
    # #print("alpha_fit="+str(alpha_fit))   
    mu=np.mean(d)
    var=np.var(d)
    sd=np.sqrt(var)
    #print("Delta = "+str(mu)+" +/- "+str(sd))
    mu_g=np.mean(N_green)
    mu_r=np.mean(N_red)
    meantime=dt_gauss*mu_g+dt*mu_r
    #print("Mean (time untill success) in seconds = "+str(meantime))
    p=(dt_gauss+dt)/meantime
    # print("Chance on success p = "+str(p) )
    #return alpha_fit, meantime, p, N_avg
    return sd, meantime, p, N_avg

#@jit(nopython=True)
def find_opt(constraint,tSD_arr, tMuT_arr, tN_arr, X, Y,sigma,dt=dt):
        #finding optimal values
    #Constraint 13 MHZ
    c1=tSD_arr<constraint*1E6
    #finding min average time
    c2=np.min(tMuT_arr[c1])
    #print(np.where(tMuT_arr==c2)[1][0])
    place=np.where(tMuT_arr==c2)
    loc=(place[0][0],place[1][0])
    opt_N_thr_success=X[loc]
    opt_N_thr_repump=Y[loc]
    opt_SD=tSD_arr[loc]
    opt_MuT=tMuT_arr[loc]
    opt_N=tN_arr[loc]
    print()
    print("At sigma = "+str(sigma))
    print("At dt = "+str(dt))
    print("Optimal Location at N_thr_success = "+str(opt_N_thr_success)+" and N_thr_repump = "+str(opt_N_thr_repump))
    print("Spectral diffusion = "+str(opt_SD))
    print("Mean time untill success = "+str(opt_MuT))
    print("Mean number of photons= "+str(opt_N))
    return opt_N_thr_success, opt_N_thr_repump, opt_SD, opt_MuT, opt_N


#@jit(nopython=True)
def optimal_results(function,g_num,r_num, sigma=sigma, start_N_thr_success=0 , start_N_thr_repump=0,dt=dt, N_thr_red=20):
    N_thr_success=start_N_thr_success
    N_thr_repump=start_N_thr_repump
    N_thr_success_arr=np.arange(N_thr_success,N_thr_success+g_num,1)
    N_thr_repump_arr=np.arange(N_thr_repump,N_thr_repump+r_num,1)
    SD_arr= np.zeros((g_num,r_num))
    MuT_arr=np.zeros((g_num,r_num))
    P_arr=np.zeros((g_num,r_num))
    N_arr=np.zeros((g_num,r_num))
    Plane_arr=np.zeros((g_num,r_num))
    X,Y=np.meshgrid(N_thr_success_arr,N_thr_repump_arr)
    for i in range(g_num):
        for j in range(r_num):
            if N_thr_repump_arr[j]<=N_thr_success_arr[i]:
                SD_arr[i,j], MuT_arr[i,j] , P_arr[i,j], N_arr[i,j]=get_results(function,N_thr_success_arr[i],N_thr_repump_arr[j],sigma,dt, N_thr_red ) #, P_arr[i,j]
                Plane_arr[i,j]=11E6
            else:
                SD_arr[i,j], MuT_arr[i,j] , P_arr[i,j], N_arr[i,j]=np.nan, np.nan, np.nan, np.nan
                Plane_arr[i,j]=np.nan

    tSD_arr=SD_arr.T
    tMuT_arr=MuT_arr.T
    tN_arr=N_arr.T

    opt_N_thr_success, opt_N_thr_repump, opt_SD, opt_MuT, opt_N=find_opt(11,tSD_arr, tMuT_arr, tN_arr, X, Y,sigma,dt)#13 new:11 
    # #mask=Y<X
    # #X[mask]=np.nan
    # #Y[mask]=np.nan
    # # print(mask)
    # # print("xs shape")
    # # print(Xs.shape)
    # # print(Xs)
    # #print (X.shape)
    # # thr_s_max=max(N_thr_success_arr)
    # # thr_s_min=min(N_thr_success_arr)
    # # thr_r_max=max(N_thr_repump_arr)
    # # thr_r_min=min(N_thr_repump_arr)
    # # thrmin=max(thr_s_min,thr_r_min)
    # # thrmax=min(thr_s_max,thr_r_max)
    # # x_line = np.arange(thrmin,thrmax,step=1)
    # # y_line = np.arange(thrmin,thrmax,step=1)
    # # z_0=np.zeros(len(x_line))
    # # z_1=np.ones(len(x_line))
    # # z_try=np.arange(min(SD_arr),max(SD_arr),step=1)
    # #print (SD_arr.shape)
    # #print (Y.shape)


    # # tPlane_arr=Plane_arr.T
    
    #ax.plot_surface(X, Y, tPlane_arr/1E6 ,color="orange")#label="13 MHZ", )
    #current_cmap = cm.coolwarm
    #current_cmap.set_bad(color='red')
    #print(np.nanmin(tSD_arr))
    #print(np.nanmax(tSD_arr))

    # fig = plt.figure()
    # ax = fig.add_subplot(1, 1, 1, projection='3d')      
    # plt.title("Spectral diffusion on success, optimum at N_thr_success = "+str(opt_N_thr_success)+" and N_thr_repump = "+str(opt_N_thr_repump))
    # ax.set_xlabel("N_thr_success (-)")
    # ax.set_ylabel("N_thr_repump (-)")
    # ax.set_zlabel("Frequency (MHz)")
    # ax.plot_surface(X, Y, tSD_arr/1E6, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False,vmin=np.nanmin(tSD_arr/1E6), vmax=np.nanmax(tSD_arr/1E6))
    # ax.scatter(opt_N_thr_success, opt_N_thr_repump, zs=(opt_SD/1E6), zdir='z', s=20, c="red")#, depthshade=True)#, *args, **kwargs)
    # #ax.set_zlim(0.99*np.nanmin(tSD_arr), 1.01*np.nanmax(tSD_arr))
    # #plt.imshow(X, Y, tSD_arr/1E6)#,cmap=current_cmap)
    # plt.show()

    # # # #, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    # # # #X_try,Z_try=np.meshgrid(x_line,z_try)
    # # # #ax.plot_surface(X_try,X_try, Z_try/1E6)
    # # # #ax.plot(x_line, y_line, z_0, label='N_thr_success<Nthr repump')#line for separation
    # # # #ax.plot(x_line, y_line, z_1, label='N_thr_success<Nthr repump')#line for separation
    # fig=plt.figure()
    # ax = fig.add_subplot(1, 1, 1, projection='3d')
    # plt.title("Mean time till success, optimum at N_thr_success = "+str(opt_N_thr_success)+" and N_thr_repump = "+str(opt_N_thr_repump))
    # ax.set_xlabel("N_thr_success (-)")
    # ax.set_ylabel("N_thr_repump (-)")
    # ax.set_zlabel("Time (ms)")
    # #plt.plot(Thr_arr,MuT_arr)
    # ax.plot_surface(X, Y, tMuT_arr/1E-3, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False,vmin=np.nanmin(tMuT_arr*1E3), vmax=np.nanmax(tMuT_arr*1E3))
    # ax.scatter(opt_N_thr_success, opt_N_thr_repump, zs=(opt_MuT*1E3), zdir='z', s=20, c="red")#cmap=cm.coolwarm, linewidth=0, antialiased=False)
    # plt.show()


    # # # ax = fig.add_subplot(1, 3, 3, projection='3d')
    # # # plt.title("Probability to succeed") #Calculated with the geometric formula and normalization
    # # # ax.set_xlabel("N_thr_success (#)")
    # # # ax.set_ylabel("N_thr_repump (#)")
    # # # ax.set_zlabel("Probability (/)")
    # # # tP_arr=P_arr.T
    # # # ax.plot_surface(X, Y, tP_arr, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    # # # plt.show()

    # # ##COMBINED PLOT
    # fig = plt.figure()
    # ax = fig.add_subplot(1, 2, 1, projection='3d')      
    # plt.title("Spectral diffusion on success")#, optimum at N_thr_success = "+str(opt_N_thr_success)+" and N_thr_repump = "+str(opt_N_thr_repump))
    # ax.set_xlabel("N_thr_success (-)")
    # ax.set_ylabel("N_thr_repump (-)")
    # ax.set_zlabel("Frequency (MHz)")
    # # # tPlane_arr=Plane_arr.T
    
    # # # ax.plot_surface(X, Y, tPlane_arr/1E6 ,color="orange")#label="13 MHZ", )
    # # #current_cmap = cm.coolwarm
    # # #current_cmap.set_bad(color='red')
    # # print(np.nanmin(tSD_arr))
    # # print(np.nanmax(tSD_arr))
    # ax.plot_surface(X, Y, tSD_arr/1E6, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False,vmin=np.nanmin(tSD_arr/1E6), vmax=np.nanmax(tSD_arr/1E6))
    # ax.scatter(opt_N_thr_success, opt_N_thr_repump, zs=(opt_SD/1E6), zdir='z', s=20, c="red")#, depthshade=True)#, *args, **kwargs)

    # ax = fig.add_subplot(1, 2, 2, projection='3d')
    # plt.title("Mean time till succes, optimum at N_thr_success = "+str(opt_N_thr_success)+" and N_thr_repump = "+str(opt_N_thr_repump))
    # ax.set_xlabel("N_thr_success (-)")
    # ax.set_ylabel("N_thr_repump (-)")
    # ax.set_zlabel("Time (ms)")
    # ax.plot_surface(X, Y, tMuT_arr*1E3, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False,vmin=np.nanmin(tMuT_arr*1E3), vmax=np.nanmax(tMuT_arr*1E3))
    # ax.scatter(opt_N_thr_success, opt_N_thr_repump, zs=(opt_MuT*1E3), zdir='z', s=20, c="red")#cmap=cm.coolwarm, linewidth=0, antialiased=False)
    # plt.show()


    # # #finding optimal values
    # # #Constraint 13 MHZ
    # # c1=tSD_arr<13E6
    # # #finding min average time
    # # c2=min(tMuT_arr[c1])
    # # #print(np.where(tMuT_arr==c2)[1][0])
    # # place=np.where(tMuT_arr==c2)
    # # loc=(place[0][0],place[1][0])
    # # opt_N_thr_success=X[loc]
    # # opt_N_thr_repump=Y[loc]
    # # opt_SD=tSD_arr[loc]
    # # opt_MuT=tMuT_arr[loc]
    # # print()
    # # print("At sigma = "+str(sigma))
    # # print("Optimal Location at N_thr_success = "+str(opt_N_thr_success)+" and N_thr_repump = "+str(opt_N_thr_repump))
    # # print("Spectral diffusion = "+str(opt_SD))
    # # print("Mean time untill success = "+str(opt_MuT))

    return opt_N_thr_success, opt_N_thr_repump, opt_SD,  opt_MuT, opt_N

#optimal_results(CRcheck,30,30, sigma=sigma, start_N_thr_success=0 , start_N_thr_repump=0,dt=dt, N_thr_red=20)
    


def variation_of_sigma_results(function=CRcheck,length=500,start=0,step=10, dt=dt):
    #step=10
    sigma_arr=1E6*np.arange(start,start+length+step,step=step)
    size=len(sigma_arr)
    opt_N_thr_success_arr=np.zeros(size)
    opt_N_thr_repump_arr=np.zeros(size)
    opt_SD_arr=np.zeros(size)
    opt_MuT_arr=np.zeros(size)
    opt_N_arr=np.zeros(size)
    for i in range(size):
        opt_N_thr_success_arr[i], opt_N_thr_repump_arr[i], opt_SD_arr[i],  opt_MuT_arr[i], opt_N_arr[i]=optimal_results(function,20,18,sigma_arr[i],0,0,dt) #(20,18)
    plt.figure()
    plt.title("Optimal N_thresholds for varying sigma, dt = "+str(dt*1E3)+"ms")
    plt.plot(sigma_arr/1E6,opt_N_thr_success_arr,label="N_thr_success") 
    plt.plot(sigma_arr/1E6, opt_N_thr_repump_arr,label="N_thr_repump") 
    plt.legend()
    plt.xlabel("sigma (MHz)")
    plt.ylabel("N_thr (-)")
    plt.show()

    plt.figure()
    plt.title("Optimal time till success and fequency with varying sigma, dt = "+str(dt*1E3)+"ms")
    plt.xlabel("sigma (MHz)")
    plt.ylabel("Time (ms)",color='b')
    plt.plot(sigma_arr/1E6,opt_MuT_arr*1E3,color='b',label="Mean (time till success)")
    plt.tick_params(axis="y", labelcolor="b", pad=8)
    plt.legend(loc='upper left')
    plt.twinx()
    plt.plot(sigma_arr/1E6,opt_SD_arr/1E6,color='r',label="F_resonance")
    plt.tick_params(axis="y", labelcolor="r", pad=8)
    plt.ylabel("Frequency (MHZ)", color='r')  
    plt.legend(loc='lower right')
    plt.show()
    


    plt.figure()
    plt.title("Optimal Photon counts with varying sigma, dt = "+str(dt*1E3)+"ms")
    plt.plot(sigma_arr/1E6,opt_N_arr,label="Average N")  
    plt.legend()
    plt.xlabel("sigma (MHz)")
    plt.ylabel("Photons (-)")

    plt.show()

    

#variation_of_sigma_results(function=CRcheck,length=100, start=0,step=10)
#variation_of_sigma_results(function=CRcheck_prim,length=200, start=0,step=10, dt=dt)

def variation_of_dt_results(function=CRcheck,percentage=0.85,length=1,start=0.5,step=0.1,sigma=sigma):
    #step=10
    dt_arr=dt*np.arange(start,start+length+step,step=step)
    size=len(dt_arr)
    opt_N_thr_success_arr=np.zeros(size)
    opt_N_thr_repump_arr=np.zeros(size)
    opt_SD_arr=np.zeros(size)
    opt_MuT_arr=np.zeros(size)
    opt_N_arr=np.zeros(size)
    for i in range(size):
        #Ns=set_Nthr(percentage, dt_arr[i],num=100000)
        #Nr=int(round(Ns/3))
        #Np=int(min(np.ceil(get_counts(f_red,dt=dt_arr[i])),50))
        Np=min(int((np.ceil(get_counts(f_red,dt=dt_arr[i])))),30)
        opt_N_thr_success_arr[i], opt_N_thr_repump_arr[i], opt_SD_arr[i],  opt_MuT_arr[i], opt_N_arr[i]=optimal_results(function,Np,Np,sigma,0,0,dt_arr[i])
    
    #plt.figure()
    #plt.tight_layout()
    plt.figure()
    plt.title("Optimal N_thresholds for varying dt, sigma = "+str(sigma/1E6)+"MHz")
    plt.plot(dt_arr*1E3,opt_N_thr_success_arr,label="N_thr_success") 
    plt.plot(dt_arr*1E3, opt_N_thr_repump_arr,label="N_thr_repump") 
    plt.legend()
    plt.xlabel("dt (ms)")
    plt.ylabel("N_thr (-)")
    plt.show()

    plt.figure
    plt.title("Optimal time till success and fequency with varying dt, sigma = "+str(sigma/1E6)+"MHz")
    plt.xlabel("dt (ms)")
    plt.ylabel("Time (ms)",color='b')
    plt.plot(dt_arr*1E3,opt_MuT_arr*1E3,color='b',label="Mean (time till success)")
    plt.tick_params(axis="y", labelcolor="b", pad=8)
    plt.legend(loc='lower right')
    plt.twinx()
    plt.plot(dt_arr*1E3,opt_SD_arr/1E6,color='r',label="F_resonance")
    plt.tick_params(axis="y", labelcolor="r", pad=8)
    plt.ylabel("Frequency (MHZ)", color='r')  
    plt.legend(loc='upper left')
    plt.show()


    plt.figure()
    plt.title("Optimal Photon counts with varying dt, sigma = "+str(sigma/1E6)+"MHz")
    plt.plot(dt_arr*1E3,opt_N_arr,label="Average N")  
    plt.legend()
    plt.xlabel("dt (ms)")
    plt.ylabel("Photons (-)")
    #plt.tight_layout()
    plt.show()


    plt.figure()
    plt.title("Optimal N_thresholds and Photon counts for varying dt, sigma = "+str(sigma/1E6)+"MHz")
    plt.plot(dt_arr*1E3,opt_N_thr_success_arr,label="N_thr_success") 
    plt.plot(dt_arr*1E3, opt_N_thr_repump_arr,label="N_thr_repump")
    plt.plot(dt_arr*1E3,opt_N_arr,label="Average N on succes")  
    plt.legend()
    plt.xlabel("dt (ms)")
    plt.ylabel("N (-)")
    plt.show()
#variation_of_dt_results(function=CRcheck,length=2,start=0.1,step=0.1,sigma=sigma)#cap=30 #2*sigma
#variation_of_dt_results(function=CRcheck_prim,length=2,start=0.5,step=0.1)

#@jit(nopython=True)
def variation_2D(function=CRcheck,dt_length=1,dt_start=0.5,dt_step=0.1,sigma_length=100,sigma_start=0,sigma_step=10):
    sigma_arr=1E6*np.arange(sigma_start,sigma_start+sigma_length+sigma_step,step=sigma_step)
    dt_arr=dt*np.arange(dt_start,dt_start+dt_length+dt_step,step=dt_step)
    sigma_size=len(sigma_arr)
    dt_size=len(dt_arr)
    opt_N_thr_success_arr=np.zeros((sigma_size,dt_size))
    opt_N_thr_repump_arr=np.zeros((sigma_size,dt_size))
    opt_SD_arr=np.zeros((sigma_size,dt_size))
    opt_MuT_arr=np.zeros((sigma_size,dt_size))
    norm_arr=np.zeros((sigma_size,dt_size))
    opt_N_arr=np.zeros((sigma_size,dt_size))      
    X,Y=np.meshgrid(sigma_arr,dt_arr)
    for i in range(sigma_size):
        for j in range(dt_size):
            norm_arr[i,j]=get_counts(f_red,dt=dt_arr[j])
            Np=int(min(np.ceil(get_counts(f_red,dt=dt_arr[j])),35))
            print(Np)
            opt_N_thr_success_arr[i,j], opt_N_thr_repump_arr[i,j], opt_SD_arr[i,j],  opt_MuT_arr[i,j], opt_N_arr[i,j]=optimal_results(function,Np,1,sigma_arr[i],0,0,dt_arr[j])#np.max(Np-40,0)

    # #print (SD_arr.shape)
    # #print (Y.shape)
    #k=0
    # loc=np.zeros((dt_size,2))
    # place
    # for k in range(dt_size):
    #     place=np.where(opt_MuT_arr[:,k]==min(opt_MuT_arr[:,k]))
    #     loc
        
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    plt.title("Optimal N_threshold_success for varying sigma and dt")
    #plt.legend()
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("dt (ms)")
    ax.set_zlabel("N_thr (-)")

    ax.plot_surface(X/1E6, Y*1E3, opt_N_thr_success_arr.T, cmap=cm.coolwarm, linewidth=0.1, antialiased=False,label="N_thr_success")#label="13 MHZ", )
    
    # ax = fig.add_subplot(1, 2, 2, projection='3d')
    # plt.title("Optimal N_threshold_repump for varying sigma and dt")
    # #plt.legend()
    # ax.set_xlabel("sigma (MHz)")
    # ax.set_ylabel("dt (ms)")
    # ax.set_zlabel("N_thr (-)")
    
    # ax.plot_surface(X/1E6, Y*1E3, opt_N_thr_repump_arr.T, cmap=cm.bwr, linewidth=0.1, antialiased=False, label="N_thr_repump")
    #Colorings plt.cm.Spectral or plt.cm.CMRmap
    plt.show()

    fig = plt.figure()
    ax = fig.add_subplot(1, 2, 1, projection='3d')
    plt.title("Optimal values of time till success")# and fequency with varying sigma")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("dt (ms)")
    ax.set_zlabel("Time (ms)")#, color='b')
    ax.plot_surface(X/1E6, Y*1E3, opt_MuT_arr.T*1E3, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Mean (time till success)")
    #ax.tick_params(axis="z", labelcolor="b", pad=8)
    #plt.legend()
    # plt.twinx()
    # ax.plot_surface(X/1E6, Y*1E3, opt_SD_arr.T/1E63, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="label="F_resonance"")
    # ax.tick_params(axis="z", labelcolor="r", pad=8)
    # ax.set_zlabel("Frequency (MHZ)", color='r')  
    # plt.legend()


    ax = fig.add_subplot(1, 2, 2, projection='3d')
    plt.title("Optimal photon counts") #Calculated with the geometric formula and normalization
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("dt (ms)")
    ax.set_zlabel("Photons (-)")
    ax.plot_surface(X/1E6, Y*1E3, opt_N_arr.T, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Average N on succes")
    #plt.legend()
    plt.show()


    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    plt.title("Optimal values of time till success")# and fequency with varying sigma")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("dt (ms)")
    ax.set_zlabel("Time (ms)")#, color='b')
    ax.plot_surface(X/1E6, Y*1E3, opt_MuT_arr.T*1E3, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Mean (time till success)")
    #ax.tick_params(axis="z", labelcolor="b", pad=8)
    #plt.legend()
    # plt.twinx()
    # ax.plot_surface(X/1E6, Y*1E3, opt_SD_arr.T/1E63, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="label="F_resonance"")
    # ax.tick_params(axis="z", labelcolor="r", pad=8)
    # ax.set_zlabel("Frequency (MHZ)", color='r')  
    # plt.legend()
    plt.show()

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    plt.title("Optimal photon counts") #Calculated with the geometric formula and normalization
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("dt (ms)")
    ax.set_zlabel("Photons (-)")
    ax.plot_surface(X/1E6, Y*1E3, opt_N_arr.T, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Average N on succes")
    #plt.legend()
    plt.show()

    # fig = plt.figure()
    # ax = fig.add_subplot(1, 1, 1, projection='3d')
    # plt.title("Optimal values of time till success")# and fequency with varying sigma")
    # ax.set_xlabel("sigma (MHz)")
    # ax.set_ylabel("dt (ms)")
    # ax.set_zlabel("Time (ms)")#, color='b')
    # ax.imshow(X/1E6, Y*1E3, opt_MuT_arr.T/norm_arr.T)#*1E3)#, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Mean (time till success)")
    # #ax.tick_params(axis="z", labelcolor="b", pad=8)
    # #plt.legend()
    # # plt.twinx()
    # # ax.plot_surface(X/1E6, Y*1E3, opt_SD_arr.T/1E63, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="label="F_resonance"")
    # # ax.tick_params(axis="z", labelcolor="r", pad=8)
    # # ax.set_zlabel("Frequency (MHZ)", color='r')  
    # # plt.legend()
    # plt.show()


    
#variation_2D(function=CRcheck_prim,dt_length=2,dt_start=0.5,dt_step=0.1,sigma_length=100,sigma_start=0,sigma_step=10)
#variation_2D(function=CRcheck_new5,dt_length=1,dt_start=0.1,dt_step=0.1,sigma_length=100,sigma_start=0,sigma_step=10)

def variation_of_N_thr_red(function=CRcheck,length=20,start=0,step=1):
    #step=10
    N_thr_red_arr=np.arange(start,start+length+step,step=step)
    size=len(N_thr_red_arr)
    opt_N_thr_success_arr=np.zeros(size)
    opt_N_thr_repump_arr=np.zeros(size)
    opt_SD_arr=np.zeros(size)
    opt_MuT_arr=np.zeros(size)
    opt_N_arr=np.zeros(size)
    for i in range(size):
        opt_N_thr_success_arr[i], opt_N_thr_repump_arr[i], opt_SD_arr[i],  opt_MuT_arr[i], opt_N_arr[i]=optimal_results(function,1,1,2*sigma,30,5,dt,N_thr_red_arr[i])
    
    plt.subplot(1,3,1)
    plt.title("Optimal N_thresholds for varying sigma")
    plt.plot(N_thr_red_arr,opt_N_thr_success_arr,label="N_thr_success") 
    plt.plot(N_thr_red_arr, opt_N_thr_repump_arr,label="N_thr_repump") 
    plt.legend()
    plt.xlabel("N_thr_red (#)")
    plt.ylabel("N_thr (#)")

    plt.subplot(1,3,2)
    plt.title("Optimal time till success and fequency with varying sigma")
    plt.xlabel("N_thr_red (#)")
    plt.ylabel("Time (ms)",color='b')
    plt.plot(N_thr_red_arr,opt_MuT_arr*1E3,color='b',label="Mean (time till success)")
    plt.tick_params(axis="y", labelcolor="b", pad=8)
    plt.legend()
    plt.twinx()
    plt.plot(N_thr_red_arr, opt_SD_arr/1E6,color='r',label="F_resonance")
    plt.tick_params(axis="y", labelcolor="r", pad=8)
    plt.ylabel("Frequency (MHZ)", color='r')  
    plt.legend()
    


    plt.subplot(1,3,3)
    plt.title("Optimal photon counts")
    plt.plot(N_thr_red_arr,opt_N_arr,label="Average lambda")  
    plt.legend()
    plt.xlabel("N_thr_red (#)")
    plt.ylabel("Photons (#)")

    plt.show()
    
#variation_of_N_thr_red(function=CRcheck,length=20,start=1,step=1)

#@jit(nopython=True)
def variation_N_red_2D(function=CRcheck,sigma_length=1,sigma_start=0.5,sigma_step=0.1,length=20,start=0,step=1):
    sigma_arr=1E6*np.arange(sigma_start,sigma_start+sigma_length+sigma_step,step=sigma_step)
    N_thr_red_arr=np.arange(start,start+length+step,step=step)
    size=len(N_thr_red_arr)
    sigma_size=len(sigma_arr)
    #dt_size=len(dt_arr)
    opt_N_thr_success_arr=np.zeros((sigma_size,size))
    opt_N_thr_repump_arr=np.zeros((sigma_size,size))
    opt_SD_arr=np.zeros((sigma_size,size))
    opt_MuT_arr=np.zeros((sigma_size,size))
    opt_N_arr=np.zeros((sigma_size,size))      
    X,Y=np.meshgrid(sigma_arr,N_thr_red_arr)
    for i in range(sigma_size):
        for j in range(size):
            #Np=int(min(np.ceil(get_counts(f_red,dt=dt,50))
            opt_N_thr_success_arr[i,j], opt_N_thr_repump_arr[i,j], opt_SD_arr[i,j],  opt_MuT_arr[i,j], opt_N_arr[i,j]=optimal_results(function,20,15,sigma_arr[i],0,0,dt, N_thr_red_arr[j])#np.max(Np-40,0)

    # #print (SD_arr.shape)
    # #print (Y.shape)
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    plt.title("Optimal N_thresholds for varying sigma")
    #plt.legend()
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("N_thr_red (#)")
    ax.set_zlabel("N_thr (#)")

    ax.plot_surface(X/1E6, Y, opt_N_thr_success_arr.T, cmap=cm.coolwarm, linewidth=0.1, antialiased=False,label="N_thr_success")#label="13 MHZ", )
    ax.plot_surface(X/1E6, Y, opt_N_thr_repump_arr.T, cmap=cm.bwr, linewidth=0.1, antialiased=False, label="N_thr_repump")
 #Colorings plt.cm.Spectral or plt.cm.CMRmap

    plt.show()

    fig=plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    plt.title("Optimal values of time till success")# and fequency with varying sigma")
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("N_thr_red (#)")
    ax.set_zlabel("Time (ms)")#, color='b')
    ax.plot_surface(X/1E6, Y, opt_MuT_arr.T*1E3, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Mean (time till success)")
    #ax.tick_params(axis="z", labelcolor="b", pad=8)
    #plt.legend()
    # plt.twinx()
    # ax.plot_surface(X/1E6, Y*1E3, opt_SD_arr.T/1E63, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="label="F_resonance"")
    # ax.tick_params(axis="z", labelcolor="r", pad=8)
    # ax.set_zlabel("Frequency (MHZ)", color='r')  
    # plt.legend()
    plt.show()

    fig=plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    plt.title("Optimal photon counts") #Calculated with the geometric formula and normalization
    ax.set_xlabel("sigma (MHz)")
    ax.set_ylabel("N_thr_red (#)")
    ax.set_zlabel("Photons (#)")
    ax.plot_surface(X/1E6, Y, opt_N_arr.T, cmap=cm.coolwarm, linewidth=0, antialiased=False,label="Average lambda")
    #plt.legend()
    plt.show()

#variation_N_red_2D(function=CRcheck_new3,sigma_length=100,sigma_start=0,sigma_step=10,length=20,start=0,step=0.5)
#variation_N_red_2D(function=CRcheck,sigma_length=100,sigma_start=0,sigma_step=10,length=30,start=1,step=1)
#variation_N_red_2D(function=CRcheck_new5,sigma_length=70,sigma_start=0,sigma_step=10,length=20,start=0,step=1)


def comparison_of_sigma_results(function=CRcheck, function2=CRcheck_new3,length=100,start=0,step=10):
    #step=10
    sigma_arr=1E6*np.arange(start,start+length+step,step=step)
    size=len(sigma_arr)
    opt_N_thr_success_arr=np.zeros((2,size))
    opt_N_thr_repump_arr=np.zeros((2,size))
    opt_SD_arr=np.zeros((2,size))
    opt_MuT_arr=np.zeros((2,size))
    opt_N_arr=np.zeros((2,size))
    for i in range(size):
        opt_N_thr_success_arr[0,i], opt_N_thr_repump_arr[0,i], opt_SD_arr[0,i],  opt_MuT_arr[0,i], opt_N_arr[0,i]=optimal_results(function,15,12,sigma_arr[i],5,3,dt)#15
        opt_N_thr_success_arr[1,i], opt_N_thr_repump_arr[1,i], opt_SD_arr[1,i],  opt_MuT_arr[1,i], opt_N_arr[1,i]=optimal_results(function2,16,12,sigma_arr[i],5,3,dt/2)#12
    #plt.subplot(1,3,1)
    plt.figure()
    plt.title("Optimal N_thresholds for varying sigma")
    plt.plot(sigma_arr/1E6,opt_N_thr_success_arr[0],color='b',label="N_thr_success") 
    plt.plot(sigma_arr/1E6, opt_N_thr_repump_arr[0],color='r',label="N_thr_repump")
    plt.plot(sigma_arr/1E6,opt_N_thr_success_arr[1],color='b',linestyle='dashed',label="N_thr_success func2") 
    plt.plot(sigma_arr/1E6, opt_N_thr_repump_arr[1],color='r',linestyle='dashed',label="N_thr_repump func2") 
    plt.legend()
    plt.xlabel("sigma (MHz)")
    plt.ylabel("N_thr (-)")
    plt.show()

    plt.figure()
    #plt.subplot(1,3,2)
    plt.title("Optimal time till success and fequency with varying sigma")
    plt.xlabel("sigma (MHz)")
    plt.ylabel("Time (ms)",color='b')
    plt.plot(sigma_arr/1E6,opt_MuT_arr[0]*1E3,color='b',label="Mean (time till success)")
    plt.plot(sigma_arr/1E6,opt_MuT_arr[1]*1E3,color='b',linestyle='dashed', label="Mean (time till success) func2")
    plt.tick_params(axis="y", labelcolor="b", pad=8)
    plt.legend()
    plt.twinx()
    plt.plot(sigma_arr/1E6,opt_SD_arr[0]/1E6,color='r',label="F_resonance")
    plt.plot(sigma_arr/1E6,opt_SD_arr[1]/1E6,color='r',linestyle='dashed',label="F_resonance func2")
    plt.tick_params(axis="y", labelcolor="r", pad=8)
    plt.ylabel("Frequency (MHz)", color='r')  
    plt.legend()
    plt.show()

    plt.figure()
    #plt.subplot(1,3,3)
    plt.title("Optimal Photon counts with varying sigma")
    plt.plot(sigma_arr/1E6,opt_N_arr[0],color='b',label="Average N")  
    plt.plot(sigma_arr/1E6,opt_N_arr[1],color='b', linestyle='dashed',label="Average N func 2") 
    plt.legend()
    plt.xlabel("sigma (MHz)")
    plt.ylabel("Photons (-)")

    plt.show()

#comparison_of_sigma_results(function=CRcheck, function2=CRcheck_radical,length=200,start=0,step=10)  #dt is halved
# 
#comparison_of_sigma_results(function=CRcheck, function2=CRcheck,length=120,start=0,step=10)   