# BEP

This code is part of my Bachelor's thesis report. It was used to collect the data and produce the figures for the report.
In the root of the repository, the following Python files can be found:

| File name | Description of purpose |
| ------ | ------ |
| CR-Check.py  | for the standard implementation (including the simplified implementation and the N_thr_red determination) |
| CR-psi_range.py | for determining the memory parameter|
| Direct_test.py    | for the Direct test |
| Wilcoxon_and_t-test.py    | for the Wilcoxon and t-test implementation |


This files have also been refactored, this can be found in the Refactored folder. 